hold on
axis equal
X=load("X.txt");
Y=load("Y.txt");
M=21;
N=21;

%X1=reshape(X,1,M*M);
%Y1=reshape(Y,1,N*N);
% for i=1:M
%     plot(X(i,:),Y(i,:));
% end
% for j=1:N
%     plot(X(:,j),Y(:,j));
% end
%scatter(X1,Y1);

for i=1:M
    plot(X(i,:),Y(i,:),'-b')
end

for j=2:N-1
    plot(X(:,j),Y(:,j),'-b')
end

r=1; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')
r=7.5; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')