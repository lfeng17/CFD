
close all
x=load("X.txt");
y=load("Y.txt");
num=588;
u=load(['u',num2str(num),'.txt']);
v=load(['v',num2str(num),'.txt']);
p=load(['p',num2str(num),'.txt']);
figure(1)
contourf(x,y,u(:,1:N+1)',50, 'edgecolor','none');colormap jet
colorbar;
axis([0 1 0 1]); 
title('U'); 
figure(2)
contourf(x,y,p(1:M+1,1:N+1)',50, 'edgecolor','none');colormap jet
colorbar;
axis([0 1 0 1]); 
figure(3)
quiver(x,y,u(:,1:N+1)',v(1:M+1,:)',10);
title("vector");
axis([0 1 0 1]); 
axis equal
figure(4)
contourf(x,y,v(1:M+1,:)',50, 'edgecolor','none');colormap jet
colorbar;
axis([0 1 0 1]); 
title('V'); 
figure(5)
hold on
startx = 0:0.2:1;
starty = 0.5*ones(size(startx));
streamline(x,y,u(:,1:N+1)',v(1:M+1,:)',startx,starty);
startx = 0:0.02:0.2;
starty = 0.1*ones(size(startx));
streamline(x,y,u(:,1:N+1)',v(1:M+1,:)',startx,starty);
startx = 0.9:0.01:1;
starty = 0.95*ones(size(startx));
streamline(x,y,u(:,1:N+1)',v(1:M+1,:)',startx,starty);
axis([0 1 0 1]); 
axis equal