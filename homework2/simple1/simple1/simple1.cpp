﻿// simple.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <fstream>
using namespace std;
#include<Eigen/Dense>
using namespace Eigen;
#include <math.h>
#include <string.h>
#include <stdio.h>
#define M 33
#define N 33
double u0 = 1;
double dt = 0.01;
double re = 2000;
double alphau = 0.3;
double alphap = 1 - alphau;
double dx = 1.0 / float(M);
double dy = 1.0 / float(N);

VectorXd X( M + 1); //x coordinate
VectorXd Y(N + 1); //y coordinate
//virtual mesh points cosidered
MatrixXd u(M + 1, N+2);
MatrixXd uold(M + 1, N + 2);
MatrixXd ustar(M + 1, N + 2);
MatrixXd uprime(M + 1, N + 2);
MatrixXd du(M + 1, N + 2);


MatrixXd v(M + 2, N + 1);
MatrixXd vold(M + 2, N + 1);
MatrixXd vstar(M + 2, N + 1);
MatrixXd vprime(M + 2, N + 1);
MatrixXd dv(M + 2, N + 1);

MatrixXd p(M + 2, N + 2);
MatrixXd pstar(M + 2, N + 2);
MatrixXd pprime(M + 2, N + 2);

char filename[30];

double max(double a, double b) {
	if (a > b)
	{
		return a;
	}
	else
	{
		return b;
	}
}

int main()
{
	for (int i = 0; i <= M; i++)
	{
		X(i) = float(i) / float(M);
	}
	for (int j = 0; j <= N; j++)
	{
		Y(j) = float(j) / float(N);
	}
	ofstream xout("./result/X.txt");
	xout << X;
	xout.close();
	ofstream yout("./result/Y.txt");
	yout << Y;
	yout.close();
	u = MatrixXd::Zero(M + 1, N + 2);
	uold= MatrixXd::Zero(M + 1, N + 2);
	ustar = MatrixXd::Zero(M + 1, N + 2);
	uprime= MatrixXd::Zero(M + 1, N + 2);
	v = MatrixXd::Zero(M + 2, N + 1);
	vold= MatrixXd::Zero(M + 2, N + 1);
	vstar= MatrixXd::Zero(M + 2, N + 1);
	vprime= MatrixXd::Zero(M + 2, N + 1);
	p = MatrixXd::Zero(M + 2, N + 2);
	pstar= MatrixXd::Zero(M + 2, N + 2);
	pprime=MatrixXd::Zero(M + 2, N + 2);
	double err = 1;
	int k = 0;
	while (err>0.0001)
	{
		//if (k == 1) { cout << uold << endl; }
		/*cout << uold << endl;*/
		ustar = uold;
		for (int i = 1; i < M; i++)
		{
			for (int j = 1; j < N+1; j++)
			{
				//w left e right n up s down
				double uce = dy / 2.0 * (uold(i, j) + uold(i + 1, j)); //a^c_e
				double ucw = dy / 2.0 * (uold(i, j) + uold(i - 1, j)); //a^c_w
				double ucn = dx / 2.0 * (vold(i, j) + vold(i + 1, j)); //a^c_n
				double ucs = dx / 2.0 * (vold(i, j - 1) + vold(i + 1, j - 1)); //a^c_s
				double xce = dy / dx / re;//a^d_e a^d_w
				double ydn = dx / dy / re;//a^d_n a^d_s
				double uae = xce + max(0, -uce); //a_e
				double uaw = xce + max(0, ucw); //a_w
				double uan = ydn + max(0, -ucn); //a_n
				double uas = ydn + max(0, ucs); //a_s
				
				double uap =( uae + uaw + uan + uas) / alphau;
				double pressureu = dy * (pstar(i + 1, j) - pstar(i, j));
				/*if (i == 1 && j == 1)
				{
					cout << "uap" << uap << endl;
				}*/
				du(i, j) = dy / uap;
				ustar(i, j) = (uae * ustar(i + 1, j) + uaw * ustar(i - 1, j) + uan * ustar(i, j + 1) + uas * ustar(i, j - 1) - pressureu ) / uap + (1 - alphau) * uold(i, j);
			}
		}
		//cout << "ustar(1,1)" << ustar(1, 1) << endl;
		vstar = vold;
		for (int i = 1; i < M+1; i++)
		{
			for (int j = 1; j < N; j++)
			{
				//w left e right n up s down
				double vce = dy / 2.0 * (uold(i, j) + uold(i, j + 1)); //a^c_e
				double vcw = dy / 2.0 * (uold(i - 1, j) + uold(i - 1, j + 1)); //a^c_w
				double vcn = dx / 2.0 * (vold(i, j) + vold(i, j + 1)); //a^c_n
				double vcs = dx / 2.0 * (vold(i, j) + vold(i, j - 1)); //a^c_s
				double xce = dy / dx / re;//a^d_e a^d_w
				double ydn = dx / dy / re;//a^d_n a^d_s
				double vae = xce + max(0, -vce); //a_e
				double vaw = xce + max(0, vcw); //a_w
				double van = ydn + max(0, -vcn); //a_n
				double vas = ydn + max(0, vcs); //a_s
				double vap = (vae + vaw + van + vas) / alphau;
				double pressurev = dy * (pstar(i, j + 1) - pstar(i, j));
				dv(i, j) = dx / vap;
				vstar(i, j) = (vae * vstar(i + 1, j) + vaw * vstar(i - 1, j) + van * vstar(i, j + 1) + vas * vstar(i, j - 1) - pressurev ) / vap + (1 - alphau) * vold(i, j);
			}
		}
		pprime = MatrixXd::Zero(M + 2, N + 2);
		for (int i = 1; i < M+1; i++)
		{
			for (int j = 1; j < N+1; j++)
			{
				
					double ape = du(i, j) * dy;
					double apw = du(i - 1, j) * dy;
					double apn = dv(i, j) * dx;
					double aps = dv(i, j - 1) * dx;
					double ap = ape + apw + apn + aps;
					double bp = (ustar(i - 1, j) - ustar(i, j)) * dy + (vstar(i, j - 1) - vstar(i, j)) * dx;
	
					pprime(i, j) = (ape * pprime(i + 1, j) + apw * pprime(i - 1, j) + apn * pprime(i, j + 1) + aps * pprime(i, j - 1) + bp) / ap;			
			}
		}
		if (k==0)
		{
			int i = 1;
			int j = 1;
			double ape = 0;
			double apw = 0;
			double apn = 0;
			double aps = 0;
			double ap = 1;
			double bp = 0;
			pprime(i, j) = (ape * pprime(i + 1, j) + apw * pprime(i - 1, j) + apn * pprime(i, j + 1) + aps * pprime(i, j - 1) + bp) / ap;
			/*if (i == 1 && j == 1)
			{
				cout << "pprime" << pprime(1, 1) << endl;
			}*/
		}
		for (int i = 1; i < M; i++)
		{
			for (int j = 1; j < N + 1; j++)
			{
				uprime(i, j) = du(i, j) * (pprime(i, j) - pprime(i + 1, j));
				u(i,j) = ustar(i, j) + uprime(i, j);
				
			}
		}
		//u = ustar + uprime;
		for (int i = 1; i < M + 1; i++)
		{
			for (int j = 1; j < N; j++)
			{
				vprime(i, j) = dv(i, j) * (pprime(i, j) - pprime(i, j + 1));
				v(i, j) = vstar(i, j) + vprime(i, j);
			}
		}
		//v = vstar + vprime;
		for (int i = 1; i < M + 1; i++)
		{
			for (int j = 0; j < N + 1; j++)
			{
				p(i, j) = pstar(i, j) + alphap * pprime(i, j);
			}
		}
		if (k > 1) {
			err = (pprime).norm();
		}
		else
		{
			err = 1;
		}
		
		uold = u;
		vold = v;
		pstar = p;
		

		//cout << uold << endl;
		/*cout << u << endl;*/
		/*cout << vold << endl;
		cout << v << endl;*/
		
		//MatrixXd u(M + 1, N + 2);
		uold(0, 0) = 0;
		uold(M, 0) = 0;
		for (int i = 1; i < M; i++) {
			uold(i,0) = 0;
			uold(i,N + 1) = 2 * u0 - uold(i, N);
		}
		
		//MatrixXd v(M + 2, N + 1);
		vold.row(0) = MatrixXd::Zero(1, N + 1);
		vold.row(M + 1) = MatrixXd::Zero(1, N + 1);
		cout << "iteration: " << k << endl;
		cout << "error " << err << endl;
			sprintf_s(filename, "./result/p%d.txt", k);
			ofstream psiout(filename);
			psiout << p;
			psiout.close();
			sprintf_s(filename, "./result/u%d.txt", k);
			ofstream uout(filename);
			uout << u;
			uout.close();
			sprintf_s(filename, "./result/v%d.txt", k);
			ofstream vout(filename);
			vout << v;
			vout.close();
			k++;
	}
	return 0;
}
