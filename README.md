# CFD

一个不得不做的大作业

主要内容包括：求解椭圆型方程计算网格、涡量-流函数法计算圆柱绕流
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/175448_23607632_5518506.jpeg "untitled.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/165040_b0e6640a_5518506.jpeg "untitled.jpg")


## eigen库包含

直接包含了eigen库，设置的时候在项目属性中


![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/102352_f00bd96e_5518506.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/102413_4c91269e_5518506.png "2.png")

..代表上一级文件夹，根据你建的项目位置不同自己试几次

然后在文件中写：

```c++
#include <Eigen/Dense>
using namespace Eigen;
```

## 网格生成

### 效果示范

![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/112053_6f181859_5518506.jpeg "circle_example1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/192353_707d321b_5518506.jpeg "airfoil_example1.jpg")

### 目前已完成的

三种迭代方法

```c++
void iteration_J(MatrixXd& x, MatrixXd& y)
void iteration_GS(MatrixXd& x, MatrixXd& y)
void iteration_relaxation(MatrixXd& x, MatrixXd& y,double a) //a目测1.5效果最佳
```

将初始化好边界的X、Y导入即可。

### 更新

将之前的程序整合了一下，放在homework1_a/mesh里面

现在的程序包括：

```c++
//初始化网格
void init_circle(MatrixXd& x, MatrixXd& y)//对圆初始化，滑动边界
void init_airfoil(MatrixXd& x, MatrixXd& y)//对NACA0012翼型初始化，滑动边界
void init_circle_sta(MatrixXd& x, MatrixXd& y)//对圆初始化，固定边界
void init_airfoil_sta(MatrixXd& x, MatrixXd& y)//对NACA0012翼型初始化，固定边界

//初始化源项
void init_PQ(MatrixXd& p, MatrixXd& q)//任意给定源项
void init_PQ_orth(MatrixXd& x, MatrixXd& y, MatrixXd& p, MatrixXd& q)//给定源项使得网格线与边界正交

//迭代函数
void iteration_J(MatrixXd& x, MatrixXd& y, MatrixXd& p, MatrixXd& q)//Jacobi迭代，适用滑动边界
void iteration_GS(MatrixXd& x, MatrixXd& y, MatrixXd& p, MatrixXd& q)//Gauss-Seidel迭代，适用滑动边界
void iteration_relaxation(MatrixXd& x, MatrixXd& y,double a, MatrixXd& p, MatrixXd& q)//松弛迭代，适用滑动边界，目测a=1.5为宜
void iteration_relaxation_sta(MatrixXd& x, MatrixXd& y, double a, MatrixXd& p, MatrixXd& q)//松弛迭代，适用固定边界
//经实测，若要使用init_PQ_orth初始化源项，必须使用固定边界，否则迭代会不收敛，原因不详
```

matlab文件为：

plot_circle.m：绘制圆周围的网格

plot_airfoil.m：绘制NACA0012翼型周围的网格

## 圆柱绕流部分
效果详见视频，picture文件夹下
采用涡量-流函数法，在1b/flow_around_cylinder_breakpoint下
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/165040_b0e6640a_5518506.jpeg "untitled.jpg")


```c++
init()//初始化
init_breakpoint()//断点初始化
push()//涡量ftcs迭代
psi_iteration()//流函数迭代
velocity()//速度从流函数求解
boundary()//边界条件
```

## simple方法
![U](https://images.gitee.com/uploads/images/2020/0613/131315_626c7f73_5518506.jpeg "1.jpg")
![V](https://images.gitee.com/uploads/images/2020/0613/131330_c80189c0_5518506.jpeg "4.jpg")
![streamline](https://images.gitee.com/uploads/images/2020/0613/131341_300d9ed2_5518506.jpeg "5.jpg")