#include<iostream>
#include<fstream>
#include<Eigen/Dense>

using namespace std;
using namespace Eigen;

#define _USE_MATH_DEFINES
#include<math.h>


double d1 = 2; //diameter of inner circle
double d2 = 5; //diameter of outter circle
#define M1 5 //number of mesh along x
#define M2 10
#define N  20 //number of mesh along y
double epslon = 0.01;
double dEta = 1;
double dXi = 1;
double delta_n = 0.05;
double sigma = 0.3;
MatrixXd X(2 * (M1 + M2) + 1, N + 1); //x coordinate
MatrixXd Y(2 * (M1 + M2) + 1, N + 1); //y coordinate
MatrixXd P(2 * (M1 + M2) + 1, N + 1);
MatrixXd Q(2 * (M1 + M2) + 1, N + 1);

VectorXd Tan(2 * M2 + 1);

VectorXd theta_rr(2 * (M1 + M2) + 1);

double arcos(double a) {
	double b = acos(a);
	if (b < 0) { return (b + M_PI); }
	else
	{
		return b;
	}
}
double arctan(double a) {
	double b = atan(a);
	if (b < 0) { return (b + M_PI); }
	else
	{
		return b;
	}
}

double angle(int m) {
	double a = (X(m, 1) - X(m, 0));
	double b = (Y(m, 1) - Y(m, 0));
	//if (cos(theta_rr(m)) < 0) {
		return acos((a * cos(theta_rr(m)) + b * sin(theta_rr(m))) / sqrt(pow(a, 2) + pow(b, 2)));
	//}
	/*if (cos(theta_rr(m)) < 0) {
		return -acos((a * cos(theta_rr(m)) + b * sin(theta_rr(m))) / sqrt(pow(a, 2) + pow(b, 2)));
	}*/
}

void init_airfoil(MatrixXd& x, MatrixXd& y, VectorXd& Tan) {

	x = MatrixXd::Zero(2 * (M1 + M2) + 1, N + 1);
	y = MatrixXd::Zero(2 * (M1 + M2) + 1, N + 1);
	for (int i = 0; i < M1; i++) {
		theta_rr(i) = M_PI / 2.0;
	}
	for (int i = M1 + 2 * M2; i < 2 * (M1 + M2) + 1; i++) {
		theta_rr(i) = -M_PI / 2.0;
	}
	for (int i = M1; i <= M1 + 2*M2; i++) {
		//double theta = (cos(float(i) / float(M) * 2.0 * M_PI)+1) * M_PI / 2 * (M / 2 - i > 0 ? 1 : -1);
		double theta = float(i - M1) / float(2 * M2) * 2.0 * M_PI;
		double temp = 0.5 * cos(theta) + 0.5;
		x(i, 0) = temp - 0.5;
		y(i, 0) = 0.594689181 * (0.298222773 * sqrt(temp) - 0.127125232 * temp - 0.357907906 * pow(temp, 2) + 0.291984971 * pow(temp, 3) - 0.105174606 * pow(temp, 4)) * ((M1 + M2) - i > 0 ? 1 : -1);
		Tan(i - M1) = -1.0 / (0.594689181 * (0.298222773 / 2 / sqrt(temp) - 0.127125232 - 0.357907906 * 2 * temp + 0.291984971 * 3 * pow(temp, 2) - 0.105174606 * 4 * pow(temp, 3))) * ((M1 + M2) - i > 0 ? 1 : -1);
		theta_rr(i) = arctan(-1.0 / (0.594689181 * (0.298222773 / 2 / sqrt(temp) - 0.127125232 - 0.357907906 * 2 * temp + 0.291984971 * 3 * pow(temp, 2) - 0.105174606 * 4 * pow(temp, 3))) * ((M1 + M2) - i > 0 ? 1 : -1));

	}
	for (int i = M1 + M2; i <= M1 + 2 * M2; i++) {
		double theta = float(i - M1) / float(2 * M2) * 2.0 * M_PI;
		double temp = 0.5 * cos(theta) + 0.5;
		Tan(i - M1) = 1.0 / (0.594689181 * (0.298222773 / 2 / sqrt(temp) - 0.127125232 - 0.357907906 * 2 * temp + 0.291984971 * 3 * pow(temp, 2) - 0.105174606 * 4 * pow(temp, 3)));
		theta_rr(i) = -theta_rr(2 * (M1 + M2) - i);
	}
	for (int i = 0; i < M1; i++) {
		x(i, 0) = 0.5 + d1 - float(i) / M1 * d1;
		y(i, 0) = 0;
		x(2 * (M1 + M2) - i, 0) = x(i, 0);
		y(2 * (M1 + M2) - i, 0) = 0;
	}

	for (int j = 0; j <= N; j++) {
		x(0, j) = 0.5 + d1;
		y(0, j) = float(j) / N * d2;
		x(2 * (M1 + M2), j) = 0.5 + d1;
		y(2 * (M1 + M2), j) = -float(j) / N * d2;
	}
	for (int i = 0; i < M1; i++) {
		x(i, N) = 0.5 + d1 - float(i) / M1 * d1;
		y(i, N) = d2;
		x(2 * (M1 + M2) - i, N) = x(i, 0);
		y(2 * (M1 + M2) - i, N) = -d2;
	}
	for (int i = M1; i <= M1 + 2 * M2; i++) {

		double theta = float(i - M1) / float(2 * M2) * M_PI;
		x(i, N) = d2 * cos(theta + M_PI / 2.0) + 0.5;
		y(i, N) = d2 * sin(theta + M_PI / 2.0);

	}
	for (int i = 1; i <= 2 * (M1 + M2); i++) {
		for (int j = 1; j < N; j++) {
			x(i, j) = float(j) / N * (x(i, N) - x(i, 0)) + x(i, 0);
			y(i, j) = float(j) / N * (y(i, N) - y(i, 0)) + y(i, 0);
		}
	}



}

void initPQ_orth_1(MatrixXd& x, MatrixXd& y, MatrixXd& P, MatrixXd& Q) {
	P = MatrixXd::Zero(2 * (M1 + M2) + 1, N + 1);
	Q = MatrixXd::Zero(2 * (M1 + M2) + 1, N + 1);

	for (int m = 1; m < 2 * (M1 + M2); m++) {
		double x_Xi = (x(m + 1, 0) - x(m - 1, 0)) / 2.0 / dXi;
		double x_Eta = (x(m, 2) - x(m, 0)) / 2.0 / dEta;
		double y_Xi = (y(m + 1, 0) - y(m - 1, 0)) / 2.0 / dXi;
		double y_Eta = (y(m, 2) - y(m, 0)) / 2.0 / dEta;
		double x_XiXi = (x(m + 1, 0) - 2 * x(m, 0) + x(m - 1, 0)) / pow(dXi, 2);
		double y_XiXi = (y(m + 1, 0) - 2 * y(m, 0) + y(m - 1, 0)) / pow(dXi, 2);
		double x_EtaEta = (x(m, 0) - 2 * x(m, 1) + x(m, 2)) / pow(dEta, 2);
		double y_EtaEta = (y(m, 0) - 2 * y(m, 1) + y(m, 2)) / pow(dEta, 2);
		P(m, 0) = -(x_Xi * x_XiXi + y_Xi * y_XiXi) / sqrt(pow(x_Xi, 2) + pow(y_Xi, 2));
		Q(m, 0) = -(x_Eta * x_EtaEta + y_Eta * y_EtaEta) / sqrt(pow(x_Eta, 2) + pow(y_Eta, 2));
	}
	/*for (int m = 1; m < 2 * (M1 + M2); m++) {
		double x_Xi = (x(m + 1, N) - x(m - 1, N)) / 2.0 / dXi;
		double x_Eta = (x(m, 2) - x(m, N)) / 2.0 / dEta;
		double y_Xi = (y(m + 1, N) - y(m - 1, N)) / 2.0 / dXi;
		double y_Eta = (y(m, 2) - y(m, N)) / 2.0 / dEta;
		double x_XiXi = (x(m + 1, N) - 2 * x(m, N) + x(m - 1, N)) / pow(dXi, 2);
		double y_XiXi = (y(m + 1, N) - 2 * y(m, N) + y(m - 1, N)) / pow(dXi, 2);
		double x_EtaEta = (x(m, N) - 2 * x(m, 1) + x(m, 2)) / pow(dEta, 2);
		double y_EtaEta = (y(m, N) - 2 * y(m, 1) + y(m, 2)) / pow(dEta, 2);
		P(m, N) = -(x_Xi * x_XiXi + y_Xi * y_XiXi) / sqrt(pow(x_Xi, 2) + pow(y_Xi, 2));
		Q(m, N) = -(x_Eta * x_EtaEta + y_Eta * y_EtaEta) / sqrt(pow(x_Eta, 2) + pow(y_Eta, 2));
	}*/
	for (int i = 0; i <= 2 * (M1 + M2); i++) {
		for (int j = 1; j < N; j++) {
			P(i, j) = P(i, 0) + float(j) / float(N) * (P(i, N) - P(i, 0));
			Q(i, j) = Q(i, 0) + float(j) / float(N) * (Q(j, N) - Q(i, 0));
		}
	}

}

void iteration_relaxation(MatrixXd& x, MatrixXd& y, double a) {
	MatrixXd xnew(2 * (M1 + M2) + 1, N + 1);
	MatrixXd ynew(2 * (M1 + M2) + 1, N + 1);
	double alpha = 0;
	double beta = 0;
	double gamma = 0;
	double b_w = 0, b_e = 0, b_s = 0, b_n = 0, b_p = 0;
	double c_px = 0, c_py = 0;
	xnew = x;
	ynew = y;
	double errorx = 10;
	double errory = 10;
	static int count = 0;
	ofstream aout("alpha.txt");
	ofstream pout("p.txt");
	ofstream qout("q.txt");
	ofstream cosout("cos.txt");
	while (count<=10)
	{
		for (int i = 1; i < 2 * (M1 + M2); i++) {
			for (int j = 1; j < N; j++) {
				alpha = pow((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta, 2) + pow((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta, 2);
				beta = ((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta) * ((x(i + 1, j) - x(i - 1, j)) / 2.0 / dXi) + ((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta) * ((y(i + 1, j) - y(i - 1, j)) / 2.0 / dXi);
				gamma = pow((x(i + 1, j) - x(i - 1, j)) / 2.0 / dXi, 2) + pow((y(i + 1, j) - y(i - 1, j)) / 2.0 / dXi, 2);
				b_w = alpha / pow(dXi, 2);
				//b_e = b_w;
				b_s = gamma / pow(dEta, 2);
				//b_n = b_s;
				//b_p = b_w + b_e + b_s + b_n;
				b_p = 2 * b_w + 2 * b_s;
				c_px = -beta * (x(i + 1, j + 1) - x(i + 1, j - 1) - x(i - 1, j + 1) + x(i - 1, j - 1)) / 2.0 / dXi / dEta + alpha * P(i, j) * (x(i + 1, j) - x(i - 1, j)) / 2 / dXi + gamma * Q(i, j) * (x(i, j + 1) - x(i, j - 1)) / 2 / dEta;
				c_py = -beta * (y(i + 1, j + 1) - y(i + 1, j - 1) - y(i - 1, j + 1) + y(i - 1, j - 1)) / 2.0 / dXi / dEta + alpha * P(i, j) * (y(i + 1, j) - y(i - 1, j)) / 2 / dXi + gamma * Q(i, j) * (y(i, j + 1) - y(i, j - 1)) / 2 / dEta;
				xnew(i, j) = (b_w * xnew(i - 1, j) + b_w * x(i + 1, j) + b_s * xnew(i, j - 1) + b_s * x(i, j + 1) + c_px) / b_p;
				ynew(i, j) = (b_w * ynew(i - 1, j) + b_w * y(i + 1, j) + b_s * ynew(i, j - 1) + b_s * y(i, j + 1) + c_py) / b_p;
				aout << "i:" << i << endl << "j:" << j << endl << b_p << endl;
				
			}
		}
		for (int m = 1; m < 2 * (M1 + M2); m++) {
			Q(m, 0) = Q(m, 0) + sigma * tanh(delta_n - sqrt(pow(x(m, 0) - x(m, 1), 2) + pow(y(m, 0) - y(m, 1), 2)));
			//P(m, 0) = P(m, 0) + sigma * tanh(angle(m));
			cosout << "m:" << m << "angle:" << angle(m) << endl;
			//cosout <<"����"<< delta_n - sqrt(pow(x(m, 0) - x(m, 1), 2) + pow(y(m, 0) - y(m, 1), 2)) << endl;
		}
		for (int m = 0; m <= 2 * (M1 + M2); m++) {
			for (int n = 1; n < N; n++) {
				//P(m, n) = P(m, 0) + float(n) / float(N) * (P(m, N) - P(m, 0));
				Q(m, n) = Q(m, 0) + float(n) / float(N) * (Q(n, N) - Q(m, 0));
			}
		}
		//initPQ_orth_1(X, Y, P, Q);
		qout << "Q:" << Q << endl;
		pout << "P:" << P << endl;
		double errorx = (x - xnew).norm();
		double errory = (y - ynew).norm();
		x = a * xnew + (1 - a) * x;
		y = a * ynew + (1 - a) * y;
		ofstream Xout("X3.txt");
		Xout << X;
		Xout.close();
		ofstream Yout("Y3.txt");
		Yout << Y;
		Yout.close();
		cout << "errorx:" << errorx << endl;
		cout << "errory:" << errory << endl;
		count++;
		if ((errorx < epslon) && (errory < epslon )) {
			cout << "a:" << a << endl;
			cout << "count: " << count << endl;
			aout.close();
			pout.close();
			qout.close();

			return;
		}
	}

}


int main() {
	P = MatrixXd::Zero(2 * (M1 + M2) + 1, N + 1);
	Q = MatrixXd::Zero(2 * (M1 + M2) + 1, N + 1);
	init_airfoil(X, Y, Tan);
	
	ofstream X1out("X1.txt");
	X1out << X;
	X1out.close();
	ofstream Y1out("Y1.txt");
	Y1out << Y;
	Y1out.close();
	ofstream tanout("tan.txt");
	tanout << Tan;
	tanout.close();
	ofstream tout("theta.txt");
	tout << theta_rr;
	tout.close();
	//initPQ_orth_1(X, Y, P, Q);
	iteration_relaxation(X, Y, 1.3);
	//iteration_J(X, Y);
	/*int i;
	cin >> i;*/
	return 0;
}
