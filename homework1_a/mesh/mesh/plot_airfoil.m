hold on
axis equal
X=load("X.txt");
Y=load("Y.txt");
M=41;
N=41;

%??
%X1=reshape(X,1,M*M);
%Y1=reshape(Y,1,N*N);
%scatter(X1,Y1);

%?
for i=1:M
    plot(X(i,:),Y(i,:),'-b');
end

for j=2:N-1
    plot(X(:,j),Y(:,j),'-b');
end


theta=0:pi/100:2*pi;
temp = 0.5*cos(theta)+0.5;
x= temp - 0.5;
y= 0.594689181 .* (0.298222773 .* temp.^0.5 - 0.127125232 .* temp - 0.357907906 .* temp.^2 + 0.291984971 .* temp.^3 - 0.105174606 .* temp.^4) .* sign(pi-theta);
x=0.5*cos(theta); 
plot(x,y,'-r')

r=7.5; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')