﻿

#include <iostream>
using namespace std;
#include <fstream>

#include <Eigen/Dense>
using namespace Eigen;

#define _USE_MATH_DEFINES
#include <math.h>

double d1 = 2; //diameter of inner circle
double d2 = 40; //diameter of outter circle
#define M  100 //number of mesh along X
#define N  100 //number of mesh along Y

MatrixXd X(M + 1, N + 1); //X coordinate
MatrixXd Y(M + 1, N + 1); //Y coordinate
MatrixXd X_eta(M + 1, N + 1); //first order
MatrixXd X_xi(M + 1, N + 1); //first order
MatrixXd Y_eta(M + 1, N + 1); //first order
MatrixXd Y_xi(M + 1, N + 1); //first order
MatrixXd X_etaeta(M + 1, N + 1); //second order,eta xi same
MatrixXd X_xixi(M + 1, N + 1); //second order,eta xi same
MatrixXd X_xieta(M + 1, N + 1); //second order,eta xi same
MatrixXd Y_etaeta(M + 1, N + 1); //second order,eta xi same
MatrixXd Y_xixi(M + 1, N + 1); //second order,eta xi same
MatrixXd Y_xieta(M + 1, N + 1); //second order,eta xi same
MatrixXd J(M + 1, N + 1);
MatrixXd Eta_x(M + 1, N + 1); //first order
MatrixXd Xi_x(M + 1, N + 1); //first order
MatrixXd Eta_y(M + 1, N + 1); //first order
MatrixXd Xi_y(M + 1, N + 1); //first order
MatrixXd Eta_xx(M + 1, N + 1); //second order,eta xi same
MatrixXd Xi_xx(M + 1, N + 1); //second order,eta xi same
MatrixXd Eta_xy(M + 1, N + 1); //second order,eta xi same
MatrixXd Eta_yy(M + 1, N + 1); //second order,eta xi same
MatrixXd Xi_yy(M + 1, N + 1); //second order,eta xi same
MatrixXd Xi_xy(M + 1, N + 1); //second order,eta xi same

double d = 1;
double dXi = 1; //delta Xi
double dEta = 1; //delta Eta
double epslon = 0.001; //error for iteration

//initial boundary points
void init(MatrixXd& X, MatrixXd& Y) {
    X = MatrixXd::Random(M + 1, N + 1);
    Y = MatrixXd::Random(M + 1, N + 1);
    for (int i = 0; i <= M; i++) {
        //for (int j = 0; j <= N; j++) {
            //X(i, j) = (float(j) / N * (d2 - d1) / 2 + d1 / 2) * cos(float(i) / float(M) * 2.0 * M_PI);
            //X(i, j) = d2 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
            //Y(i, j) = (float(j) / N * (d2 - d1) / 2 + d1 / 2) * sin(float(i) / float(M) * 2.0 * M_PI);
            //Y(i, N) = d2 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
        //}
        X(i, 0) = d1 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
        X(i, N) = d2 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
        Y(i, 0) = d1 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
        Y(i, N) = d2 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
    }
    for (int j = 1; j <= N - 1; j++) {
        X(0, j) = float(j) / float(N) * (d2 - d1) / 2.0 + d1 / 2;
        X(M, j) = float(j) / float(N) * (d2 - d1) / 2.0 + d1 / 2;
        Y(0, j) = 0;
        Y(M, j) = 0;
    }

    for (int i = 1; i < M; i++) {
        for (int j = 1; j < N; j++) {
            double theta = float(i) / float(M) * 2.0 * M_PI;
            double r = float(j) / float(N) * (d2 - d1) / 2 + d1 / 2;
            X(i, j) = r * cos(theta);
            Y(i, j) = r * sin(theta);
        }
    }

}


void iteration_relaxation(MatrixXd& X, MatrixXd& Y, double a) {
    MatrixXd xnew(M + 1, N + 1);
    MatrixXd ynew(M + 1, N + 1);
    double alpha = 0;
    double beta = 0;
    double gamma = 0;
    double b_w = 0, b_e = 0, b_s = 0, b_n = 0, b_p = 0;
    double c_px = 0, c_py = 0;
    xnew = X;
    ynew = Y;
    double errorx = 10;
    double errory = 10;
    static int count = 0;
    int index = 0;
    ofstream aout("alpha.txt");
    while (true)
    {
        for (int i = 0; i < M; i++) {
            if (i > 0) {
                index = i - 1;
            }
            else
            {
                index = M - 1;
            }
            for (int j = 1; j < N; j++) {
                alpha = pow((X(i, j + 1) - X(i, j - 1)) / 2.0 / dEta, 2) + pow((Y(i, j + 1) - Y(i, j - 1)) / 2.0 / dEta, 2);
                beta = ((X(i, j + 1) - X(i, j - 1)) / 2.0 / dEta) * ((X(i + 1, j) - X(index, j)) / 2.0 / dXi) + ((Y(i, j + 1) - Y(i, j - 1)) / 2.0 / dEta) * ((Y(i + 1, j) - Y(index, j)) / 2.0 / dXi);
                gamma = pow((X(i + 1, j) - X(index, j)) / 2.0 / dXi, 2) + pow((Y(i + 1, j) - Y(index, j)) / 2.0 / dXi, 2);
                b_w = alpha / pow(dXi, 2);
                //b_e = b_w;
                b_s = gamma / pow(dEta, 2);
                //b_n = b_s;
                //b_p = b_w + b_e + b_s + b_n;
                b_p = 2 * b_w + 2 * b_s;
                c_px = -beta * (X(i + 1, j + 1) - X(i + 1, j - 1) - X(index, j + 1) + X(index, j - 1)) / 2.0 / dXi / dEta;
                c_py = -beta * (Y(i + 1, j + 1) - Y(i + 1, j - 1) - Y(index, j + 1) + Y(index, j - 1)) / 2.0 / dXi / dEta;
                xnew(i, j) = (b_w * xnew(index, j) + b_w * X(i + 1, j) + b_s * xnew(i, j - 1) + b_s * X(i, j + 1) + c_px) / b_p;
                ynew(i, j) = (b_w * ynew(index, j) + b_w * Y(i + 1, j) + b_s * ynew(i, j - 1) + b_s * Y(i, j + 1) + c_py) / b_p;
                xnew(M, j) = xnew(0, j);
                ynew(0, j) = 0;
                aout << "i:" << i << endl << "j:" << j << endl << b_p << endl;
            }
        }

        double errorx = (X - xnew).norm();
        double errory = (Y - ynew).norm();
        X = a * xnew + (1 - a) * X;
        Y = a * ynew + (1 - a) * Y;
        aout << "xnew:\n" << xnew << endl;
        aout << "ynew:\n" << ynew << endl;
        aout << "errorx:" << errorx << endl;
        aout << "errory:" << errory << endl;
        aout << "a:" << a << endl;
        aout << "count: " << count << endl;
        cout << "errorx:" << errorx << endl;
        cout << "errory:" << errory << endl;
        count++;
        if ((errorx < epslon) && (errory < epslon * 0.6)) {
            cout << "a:" << a << endl;
            cout << "count: " << count << endl;
            aout.close();
            return;
        }
    }

}

void Jacobi() {
    int index = 0;

    X_xi = MatrixXd::Zero(M + 1, N + 1);
    Y_xi = MatrixXd::Zero(M + 1, N + 1);
    X_eta = MatrixXd::Zero(M + 1, N + 1);
    Y_eta = MatrixXd::Zero(M + 1, N + 1);
    X_xixi = MatrixXd::Zero(M + 1, N + 1);
    X_etaeta = MatrixXd::Zero(M + 1, N + 1);
    Y_xixi = MatrixXd::Zero(M + 1, N + 1);
    Y_etaeta = MatrixXd::Zero(M + 1, N + 1);
    J = MatrixXd::Zero(M + 1, N + 1);
    for (int i = 0; i < M; i++) {
        if (i > 0) {
            index = i - 1;
        }
        else
        {
            index = M - 1;
        }
        for (int j = 1; j < N; j++) {
            X_xi(i, j) = (X(i + 1, j) - X(index, j)) / 2.0 / d;
            Y_xi(i, j) = (Y(i + 1, j) - Y(index, j)) / 2.0 / d;
            X_eta(i, j) = (X(i, j + 1) - X(i, j - 1)) / 2.0 / d;
            Y_eta(i, j) = (X(i, j + 1) - X(i, j - 1)) / 2.0 / d;
            X_xixi(i, j) = (X(i + 1, j) - 2 * X(i, j) + X(index, j)) / d / d;
            Y_xixi(i, j) = (Y(i + 1, j) - 2 * Y(i, j) + Y(index, j)) / d / d;
            X_etaeta(i, j) = (X(i, j + 1) - 2 * X(i, j) + X(i, j - 1)) / d / d;
            Y_etaeta(i, j) = (Y(i, j + 1) - 2 * Y(i, j) + Y(i, j - 1)) / d / d;
            X_xieta(i, j) = (X(i + 1, j + 1) - X(i + 1, j - 1) - X(index, j + 1) + X(index, j - 1)) / 4.0 / d / d;
            Y_xieta(i, j) = (Y(i + 1, j + 1) - Y(i + 1, j - 1) - Y(index, j + 1) + Y(index, j - 1)) / 4.0 / d / d;
            J(i, j) = X_xi(i, j) * Y_eta(i, j) - X_eta(i, j) * Y_xi(i, j);
            //cout << "temp" << J(i, j) << endl;
            
        }
    }
    
}

void deriviate() {
    int index = 0;
    Vector3d A, B, C, D;
    Matrix3d E;
    double temp;
    Xi_x = MatrixXd::Zero(M + 1, N + 1);
    Eta_x = MatrixXd::Zero(M + 1, N + 1);
    Xi_y = MatrixXd::Zero(M + 1, N + 1);
    Eta_y = MatrixXd::Zero(M + 1, N + 1);
    Xi_xx = MatrixXd::Zero(M + 1, N + 1);
    Xi_xy = MatrixXd::Zero(M + 1, N + 1);
    Xi_yy = MatrixXd::Zero(M + 1, N + 1);
    Eta_xx = MatrixXd::Zero(M + 1, N + 1);
    Eta_xy = MatrixXd::Zero(M + 1, N + 1);
    Eta_yy = MatrixXd::Zero(M + 1, N + 1);
    for (int i = 0; i < M; i++) {
        if (i > 0) {
            index = i - 1;
        }
        else
        {
            index = M - 1;
        }
        for (int j = 1; j < N; j++) {
            Xi_x(i, j) = Y_eta(i, j) / J(i, j);
            Eta_x(i, j) = -Y_xi(i, j) / J(i, j);
            Xi_y(i, j) = -X_eta(i, j) / J(i, j);
            Eta_y(i, j) = X_xi(i, j) / J(i, j);
            A << Xi_x(i, j) * X_xixi(i, j) + Xi_y(i, j) * Y_xixi(i, j), Xi_x(i, j)* X_xieta(i, j) + Xi_y(i, j) * Y_xieta(i, j), Xi_x(i, j)* X_etaeta(i, j) + Xi_y(i, j) * Y_etaeta(i, j);
            E << pow(Y_eta(i, j), 2), -2 * Y_eta(i, j) * Y_xi(i, j), pow(Y_xi(i, j), 2),
                -X_eta(i, j) * Y_eta(i, j), X_xi(i, j)* Y_eta(i, j) + X_eta(i, j) * Y_xi(i, j), -X_xi(i, j) * Y_xi(i, j),
                pow(X_eta(i, j), 2), -2 * X_xi(i, j) * X_eta(i, j), pow(X_xi(i, j), 2);
            //temp = pow(X_xi(i, j) * Y_eta(i, j), 2) + pow(X_eta(i, j) * Y_xi(i, j), 2);
            temp = pow(X_xi(i, j) * Y_eta(i, j) - X_eta(i, j) * Y_xi(i, j), 2);
            B = -E * A / temp;
            Xi_xx(i, j) = B(0);
            Xi_xy(i, j) = B(1);
            Xi_yy(i, j) = B(2);
            C << Eta_x(i, j) * X_xixi(i, j) + Eta_y(i, j) * Y_xixi(i, j), Eta_x(i, j)* X_xieta(i, j) + Eta_y(i, j) * Y_xieta(i, j), Eta_x(i, j)* X_etaeta(i, j) + Eta_y(i, j) * Y_etaeta(i, j);
            D = -E * C / temp;
            Eta_xx(i, j) = D(0);
            Eta_xy(i, j) = D(1);
            Eta_yy(i, j) = D(2);
        }
    }
}

void readPortfolio(MatrixXd& matrix, string b)  //this will be a function that should hopefully return a matrix to use in other functions.
{
    ifstream data(b);
    string lineOfData;

    if (data.is_open())
    {
        int i = 0;
        while (data.good())
        {
            char linebuff[4096];
            getline(data, lineOfData);
            strncpy_s(linebuff, lineOfData.c_str(), sizeof(linebuff) - 1);

            //cout << lineOfData << endl; //just to check if the data was read.
            {
                int j = 0;
                double val;
                char* p_r = NULL, * p_val;
                p_val = strtok_s(linebuff, " ,;", &p_r);
                while (NULL != p_val) {
                    //?wrong??  sscanf_s("%lf", p_val, &val);
                    val = atof(p_val);
                    //cout << "  " << val;
                    matrix(i, j) = val;
                    j++;
                    p_val = strtok_s(NULL, " ,;", &p_r);
                }
                //cout << endl;
            }
            i++;
        }
    }
    else cout << "Unable to open file";

}

int main()
{
    //init(X, Y);
    //ofstream X11out("X1.txt");
    //X11out << X;
    //X11out.close();
    //ofstream Y11out("Y1.txt");
    //Y11out << Y;
    //Y11out.close();
    //iteration_relaxation(X, Y, 1.5);
    ////iteration_GS(X, Y);
    //ofstream Xout("X.txt");
    //Xout << X;
    //Xout.close();
    //ofstream Yout("Y.txt");
    //Yout << Y;
    //Yout.close();
    //std::cout << X.rows() << "\n";
    //std::cout << X.cols() << "\n";
    //cout << X << endl << Y << endl;
    //Jacobi();
    //ofstream X1out("X_xi.txt");
    //X1out << X_xi;
    //X1out.close();
    //ofstream X2out("X_eta.txt");
    //X2out << X_eta;
    //X2out.close();
    //ofstream X3out("X_xixi.txt");
    //X3out << X_xixi;
    //X3out.close();
    //ofstream X4out("X_etaeta.txt");
    //X4out << X_etaeta;
    //X4out.close();
    //ofstream X5out("X_xieta.txt");
    //X5out << X_xieta;
    //X5out.close();
    //ofstream Y1out("Y_xi.txt");
    //Y1out << Y_xi;
    //Y1out.close();
    //ofstream Y2out("Y_eta.txt");
    //Y2out << Y_eta;
    //Y2out.close();
    //ofstream Y3out("Y_xixi.txt");
    //Y3out << Y_xixi;
    //Y3out.close();
    //ofstream Y4out("Y_etaeta.txt");
    //Y4out << Y_etaeta;
    //Y4out.close();
    //ofstream Y5out("Y_xieta.txt");
    //Y5out << Y_xieta;
    //Y5out.close();
    readPortfolio(X, "X.txt");
    readPortfolio(Y, "Y.txt");
    
    Jacobi();
    ofstream Jout("J.txt");
    Jout << J;
    Jout.close();

    deriviate();

    ofstream X1out("Xi_x.txt");
    X1out << Xi_x;
    X1out.close();
    ofstream X2out("Eta_x.txt");
    X2out << Eta_x;
    X2out.close();
    ofstream X3out("Xi_xx.txt");
    X3out << Xi_xx;
    X3out.close();
    ofstream X4out("Eta_xx.txt");
    X4out << Eta_xx;
    X4out.close();
    ofstream X5out("Xi_xy.txt");
    X5out << Xi_xy;
    X5out.close();
    ofstream Y1out("Xi_y.txt");
    Y1out << Xi_y;
    Y1out.close();
    ofstream Y2out("Eta_y.txt");
    Y2out << Eta_y;
    Y2out.close();
    ofstream Y3out("Xi_yy.txt");
    Y3out << Xi_yy;
    Y3out.close();
    ofstream Y4out("Eta_yy.txt");
    Y4out << Eta_yy;
    Y4out.close();
    ofstream Y5out("Eta_xy.txt");
    Y5out << Eta_xy;
    Y5out.close();
    return 0;
}