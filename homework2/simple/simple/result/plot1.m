clear all
X=load("X.txt");
Y=load("Y.txt");
num=6;
U=load(['u',num2str(num),'.txt']);
V=load(['v',num2str(num),'.txt']);
P=load(['p',num2str(num),'.txt']);

[M,N]=size(X);
for i=1:M-1
   for j=1:N-1
      meshx(i,j)=(X(i,j)+X(i+1,j))/2; 
      meshy(i,j)=(Y(i,j)+Y(i,j+1))/2;
   end
end
for i=1:M-1
   meshu(i,:)=( U(i,:)+U(i+1,:))/2;
end
for j=1:N-1
   meshv(:,j)=(V(:,j)+V(:,j+1))/2; 
end
figure(1)
hold on
for i=1:M-1
    plot( meshx(i,:), meshy(i,:),'-g');
end

for j=1:N-1
    plot( meshx(:,j), meshy(:,j),'-g');
end
axis equal
figure(2)

quiver(meshx,meshy,meshu,meshv,0.5);
axis equal
figure(3)
surface(meshx,meshy,P);
shading interp
axis equal
% figure(2)
% surface(X,Y,P);
% shading interp
% figure(3)
% contour(X(1:20,1:20),Y(1:20,1:20),P,50);
