hold on
axis equal
X=load("X.txt");
Y=load("Y.txt");
M=21;
N=21;

%画散点图
%X1=reshape(X,1,M*M);
%Y1=reshape(Y,1,N*N);
%scatter(X1,Y1);

%画网格图
for i=1:M
    plot(X(i,:),Y(i,:),'-b');
end

for j=2:N-1
    plot(X(:,j),Y(:,j),'-b');
end


r=1; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')
r=7.5; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')
hold on
% x=linspace(1,7.5,10);
% y=zeros(10,1);
% plot(x,y,'LineWidth',2);