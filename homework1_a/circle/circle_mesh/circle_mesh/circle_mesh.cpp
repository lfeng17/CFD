﻿

#include <iostream>
using namespace std;
#include <fstream>

#include <Eigen/Dense>
using namespace Eigen;

#define _USE_MATH_DEFINES
#include <math.h>

double d1 = 2; //diameter of inner circle
double d2 = 15; //diameter of outter circle
#define M  20 //number of mesh along x
#define N  20 //number of mesh along y

MatrixXd X(M + 1, N + 1); //x coordinate
MatrixXd Y(M + 1, N + 1); //y coordinate

double dXi = 1; //delta Xi
double dEta = 1; //delta Eta
double epslon = 0.001; //error for iteration

//initial boundary points
void init(MatrixXd& x, MatrixXd& y) {
    x = MatrixXd::Random(M + 1, N + 1);
    y = MatrixXd::Random(M + 1, N + 1);
    for (int i = 0; i <= M; i++) {
        //for (int j = 0; j <= N; j++) {
            //x(i, j) = (float(j) / N * (d2 - d1) / 2 + d1 / 2) * cos(float(i) / float(M) * 2.0 * M_PI);
            //x(i, j) = d2 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
            //y(i, j) = (float(j) / N * (d2 - d1) / 2 + d1 / 2) * sin(float(i) / float(M) * 2.0 * M_PI);
            //y(i, N) = d2 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
        //}
        x(i, 0) = d1 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
        x(i, N) = d2 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
        y(i, 0) = d1 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
        y(i, N) = d2 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
    }
    for (int j = 1; j <= N - 1; j++) {
        x(0, j) = float(j) / float(N) * (d2 - d1) / 2.0 + d1 / 2;
        x(M, j) = float(j) / float(N) * (d2 - d1) / 2.0 + d1 / 2;
        y(0, j) = 0;
        y(M, j) = 0;
    }

    for (int i = 1; i < M; i++) {
        for (int j = 1; j < N; j++) {
            double theta = float(i) / float(M) * 2.0 * M_PI;
            double r = float(j) / float(N) * (d2 - d1) / 2 + d1 / 2;
            x(i, j) = r * cos(theta);
            y(i, j) = r * sin(theta);
        }
    }

}

void iteration_J(MatrixXd& x, MatrixXd& y) {
    MatrixXd xnew(M + 1, N + 1);
    MatrixXd ynew(M + 1, N + 1);
    double alpha = 0;
    double beta = 0;
    double gamma = 0;
    double b_w = 0, b_e = 0, b_s = 0, b_n = 0, b_p = 0;
    double c_px = 0, c_py = 0;
    xnew = x;
    ynew = y;
    double errorx = 10;
    double errory = 10;
    static int count = 0;
    ofstream aout("alpha.txt");
    while (true)
    {
        for (int i = 1; i < M; i++) {
            for (int j = 1; j < N; j++) {
                alpha = pow((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta, 2) + pow((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta, 2);
                beta = ((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta) * ((x(i + 1, j) - x(i - 1, j)) / 2.0 / dXi) + ((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta) * ((y(i + 1, j) - y(i - 1, j)) / 2.0 / dXi);
                gamma = pow((x(i + 1, j) - x(i - 1, j)) / 2.0 / dXi, 2) + pow((y(i + 1, j) - y(i - 1, j)) / 2.0 / dXi, 2);
                b_w = alpha / pow(dXi, 2);
                //b_e = b_w;
                b_s = gamma / pow(dEta, 2);
                //b_n = b_s;
                //b_p = b_w + b_e + b_s + b_n;
                b_p = 2 * b_w + 2 * b_s;
                c_px = -beta * (x(i + 1, j + 1) - x(i + 1, j - 1) - x(i - 1, j + 1) + x(i - 1, j - 1)) / 2.0 / dXi / dEta;
                c_py = -beta * (y(i + 1, j + 1) - y(i + 1, j - 1) - y(i - 1, j + 1) + y(i - 1, j - 1)) / 2.0 / dXi / dEta;
                xnew(i, j) = (b_w * x(i - 1, j) + b_w * x(i + 1, j) + b_s * x(i, j - 1) + b_s * x(i, j + 1) + c_px) / b_p;
                ynew(i, j) = (b_w * y(i - 1, j) + b_w * y(i + 1, j) + b_s * y(i, j - 1) + b_s * y(i, j + 1) + c_py) / b_p;
                aout << "i:" << i << endl << "j:" << j << endl << b_p << endl;
            }
        }
        double errorx = (x - xnew).norm();
        double errory = (y - ynew).norm();
        x = xnew;
        y = ynew;
        aout << "xnew:\n" << xnew << endl;
        aout << "ynew:\n" << ynew << endl;
        aout << "errorx:" << errorx << endl;
        aout << "errory:" << errory << endl;
        aout << "count: " << count << endl;
        cout << "errorx:" << errorx << endl;
        cout << "errory:" << errory << endl;
        count++;
        if ((errorx < epslon) && (errory < epslon)) {
            cout << "count: " << count << endl;
            aout.close();
            return;
        }
    }

}

void iteration_GS(MatrixXd& x, MatrixXd& y) {
    MatrixXd xnew(M + 1, N + 1);
    MatrixXd ynew(M + 1, N + 1);
    double alpha = 0;
    double beta = 0;
    double gamma = 0;
    double b_w = 0, b_e = 0, b_s = 0, b_n = 0, b_p = 0;
    double c_px = 0, c_py = 0;
    xnew = x;
    ynew = y;
    double errorx = 10;
    double errory = 10;
    static int count = 0;
    ofstream aout("alpha.txt");
    while (true)
    {
        for (int i = 1; i < M; i++) {
            for (int j = 1; j < N; j++) {
                alpha = pow((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta, 2) + pow((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta, 2);
                beta = ((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta) * ((x(i + 1, j) - x(i - 1, j)) / 2.0 / dXi) + ((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta) * ((y(i + 1, j) - y(i - 1, j)) / 2.0 / dXi);
                gamma = pow((x(i + 1, j) - x(i - 1, j)) / 2.0 / dXi, 2) + pow((y(i + 1, j) - y(i - 1, j)) / 2.0 / dXi, 2);
                b_w = alpha / pow(dXi, 2);
                //b_e = b_w;
                b_s = gamma / pow(dEta, 2);
                //b_n = b_s;
                //b_p = b_w + b_e + b_s + b_n;
                b_p = 2 * b_w + 2 * b_s;
                c_px = -beta * (x(i + 1, j + 1) - x(i + 1, j - 1) - x(i - 1, j + 1) + x(i - 1, j - 1)) / 2.0 / dXi / dEta;
                c_py = -beta * (y(i + 1, j + 1) - y(i + 1, j - 1) - y(i - 1, j + 1) + y(i - 1, j - 1)) / 2.0 / dXi / dEta;
                xnew(i, j) = (b_w * xnew(i - 1, j) + b_w * x(i + 1, j) + b_s * xnew(i, j - 1) + b_s * x(i, j + 1) + c_px) / b_p;
                ynew(i, j) = (b_w * ynew(i - 1, j) + b_w * y(i + 1, j) + b_s * ynew(i, j - 1) + b_s * y(i, j + 1) + c_py) / b_p;
                aout << "i:" << i << endl << "j:" << j << endl << b_p << endl;
            }
        }
        double errorx = (x - xnew).norm();
        double errory = (y - ynew).norm();
        x = xnew;
        y = ynew;
        aout << "xnew:\n" << xnew << endl;
        aout << "ynew:\n" << ynew << endl;
        aout << "errorx:" << errorx << endl;
        aout << "errory:" << errory << endl;
        aout << "count: " << count << endl;
        cout << "errorx:" << errorx << endl;
        cout << "errory:" << errory << endl;
        count++;
        if ((errorx < epslon) && (errory < epslon)) {
            cout << "count: " << count << endl;
            aout.close();
            return;
        }
    }

}

void iteration_relaxation(MatrixXd& x, MatrixXd& y, double a) {
    MatrixXd xnew(M + 1, N + 1);
    MatrixXd ynew(M + 1, N + 1);
    double alpha = 0;
    double beta = 0;
    double gamma = 0;
    double b_w = 0, b_e = 0, b_s = 0, b_n = 0, b_p = 0;
    double c_px = 0, c_py = 0;
    xnew = x;
    ynew = y;
    double errorx = 10;
    double errory = 10;
    static int count = 0;
    ofstream aout("alpha.txt");
    while (true)
    {
        for (int i = 0; i < M; i++) {
            int index = (i > 0 ? i - 1 : M - 1);
            for (int j = 1; j < N; j++) {
                alpha = pow((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta, 2) + pow((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta, 2);
                beta = ((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta) * ((x(i + 1, j) - x(index, j)) / 2.0 / dXi) + ((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta) * ((y(i + 1, j) - y(index, j)) / 2.0 / dXi);
                gamma = pow((x(i + 1, j) - x(index, j)) / 2.0 / dXi, 2) + pow((y(i + 1, j) - y(index, j)) / 2.0 / dXi, 2);
                b_w = alpha / pow(dXi, 2);
                //b_e = b_w;
                b_s = gamma / pow(dEta, 2);
                //b_n = b_s;
                //b_p = b_w + b_e + b_s + b_n;
                b_p = 2 * b_w + 2 * b_s;
                c_px = -beta * (x(i + 1, j + 1) - x(i + 1, j - 1) - x(index, j + 1) + x(index, j - 1)) / 2.0 / dXi / dEta;
                c_py = -beta * (y(i + 1, j + 1) - y(i + 1, j - 1) - y(index, j + 1) + y(index, j - 1)) / 2.0 / dXi / dEta;
                xnew(i, j) = (b_w * xnew(index, j) + b_w * x(i + 1, j) + b_s * xnew(i, j - 1) + b_s * x(i, j + 1) + c_px) / b_p;
                ynew(i, j) = (b_w * ynew(index, j) + b_w * y(i + 1, j) + b_s * ynew(i, j - 1) + b_s * y(i, j + 1) + c_py) / b_p;
                xnew(M, j) = xnew(0, j);
                ynew(0, j) = 0;
                aout << "i:" << i << endl << "j:" << j << endl << b_p << endl;
            }
        }

        double errorx = (x - xnew).norm();
        double errory = (y - ynew).norm();
        x = a * xnew + (1 - a) * x;
        y = a * ynew + (1 - a) * y;
        aout << "xnew:\n" << xnew << endl;
        aout << "ynew:\n" << ynew << endl;
        aout << "errorx:" << errorx << endl;
        aout << "errory:" << errory << endl;
        aout << "a:" << a << endl;
        aout << "count: " << count << endl;
        cout << "errorx:" << errorx << endl;
        cout << "errory:" << errory << endl;
        count++;
        if ((errorx < epslon) && (errory < epslon * 0.6)) {
            cout << "a:" << a << endl;
            cout << "count: " << count << endl;
            aout.close();
            return;
        }
    }

}

int main()
{
    init(X, Y);
    ofstream X1out("X1.txt");
    X1out << X;
    X1out.close();
    ofstream Y1out("Y1.txt");
    Y1out << Y;
    Y1out.close();
    iteration_relaxation(X, Y, 1.5);
    //iteration_GS(X, Y);
    ofstream Xout("X.txt");
    Xout << X;
    Xout.close();
    ofstream Yout("Y.txt");
    Yout << Y;
    Yout.close();
    std::cout << X.rows() << "\n";
    std::cout << X.cols() << "\n";
    cout << X << endl << Y << endl;

    return 0;
}