
#include<iostream>
using namespace std;


#include<fstream>
#include<Eigen/Dense>
#include <vector>
using namespace Eigen;

#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>
#include <stdio.h>





#define random(x) (rand()%x)
double L1 = 25; 
double L2 = 100; 
double H1 = 45;
double H2 = 35;

#define M1 50
#define M2 199
#define N1 90
#define N2 70 
double dt = 0.001;
double CFL = 1.0;
double e = 0.00001;

double gamma = 1.4;


MatrixXd X1(M1 + 1, N1 + 1); 
MatrixXd Y1(M1 + 1, N1 + 1); 
MatrixXd X2(M2+1 , N2 + 1);
MatrixXd Y2(M2+1 , N2 + 1);


double dx1 = L1 /  M1;
double dx2 = L2 /  (M2+1);
double dy1 = H1 /  N1;
double dy2 = H2 /  N2;


MatrixXd r1(M1 + 1, N1 + 1); //rho
MatrixXd u1(M1 + 1, N1 + 1); //u velocity
MatrixXd v1(M1 + 1, N1 + 1); //v velocity
MatrixXd p1(M1 + 1, N1 + 1); //pressure
MatrixXd r2(M2 + 1, N2 + 1); //rho
MatrixXd u2(M2 + 1, N2 + 1); //u velocity
MatrixXd v2(M2 + 1, N2 + 1); //v velocity
MatrixXd p2(M2 + 1, N2 + 1); //pressure


double minmod(double a, double b) {
	if (a * b > 0) return min(abs(a), abs(b)) * a / abs(a);
	else return 0;

}

void readPortfolio(MatrixXd& matrix, string b)  //this will be a function that should hopefully return a matrix to use in other functions.
{
	ifstream data(b);
	string lineOfData;

	if (data.is_open())
	{
		int i = 0;
		while (data.good())
		{
			char linebuff[4096];
			getline(data, lineOfData);
			strncpy_s(linebuff, lineOfData.c_str(), sizeof(linebuff) - 1);

			//cout << lineOfData << endl; //just to check if the data was read.
			{
				int j = 0;
				double val;
				char* p_r = NULL, * p_val;
				p_val = strtok_s(linebuff, " ,;", &p_r);
				while (NULL != p_val) {
					//?wrong??  sscanf_s("%lf", p_val, &val);
					val = atof(p_val);
					//cout << "  " << val;
					matrix(i, j) = val;
					j++;
					p_val = strtok_s(NULL, " ,;", &p_r);
				}
				//cout << endl;
			}
			i++;
		}
	}
	else { std::cout << "Unable to open file"; }

}



void init() {
	r1 = 1.4 * MatrixXd::Ones(M1 + 1, N1 + 1);
	p1 = MatrixXd::Ones(M1 + 1, N1 + 1) ;
	u1 = 3 * MatrixXd::Ones(M1 + 1, N1 + 1);
	v1 = MatrixXd::Zero(M1 + 1, N1 + 1);

	r2 = 1.4 * MatrixXd::Ones(M2 +1, N2 + 1);
	p2 = MatrixXd::Ones(M2 + 1, N2 + 1);
	u2 = 3 * MatrixXd::Ones(M2 + 1, N2 + 1);
	v2 = MatrixXd::Zero(M2 + 1, N2 + 1);

}


void boundary() {


	for (int i = 0; i < M1 + 1; i++)
	{
		v1(i, 0) = 0;
		v1(i, N1) = 0;
		
	}

	for (int i = 0; i < M2 + 1; i++)
	{
		v2(i, 0) = 0;
		v2(i, N2) = 0;

	}

	for (int j = 0; j < N1 + 1; j++) {
		u1(0, j) = 3;
		v1(0, j) = 0;
		r1(0, j) = 1.4;
		p1(0, j) = 1;
		
	}

	for (int j = 0; j < N1 - N2 ; j++) {
		u1(M1, j) = 0;
	
	}
	v1(M1, 0) = 0;

}

// pos , _p : positive ;  neg,_n : negative
void split(MatrixXd* pos1[], MatrixXd* neg1[], MatrixXd* pos2[], MatrixXd* neg2[], int k) {
	for (int i = 0; i <= M1; i++) {
		for (int j = 0; j <= N1; j++) {
			double a = sqrt(gamma * p1(i, j) / r1(i, j));
			double lambda1 = u1(i, j) *(1-k) + v1(i,j) * k;
			double lambda3 = lambda1 - a ;
			double lambda4 = lambda1 + a ;


			double lambda1h[2] = { (lambda1 + sqrt(lambda1 * lambda1 + e * e)) / 2, (lambda1 - sqrt(lambda1 * lambda1 + e * e)) / 2 };
			double lambda3h[2] = { (lambda3 + sqrt(lambda3 * lambda3 + e * e)) / 2, (lambda3 - sqrt(lambda3 * lambda3 + e * e)) / 2 };
			double lambda4h[2] = { (lambda4 + sqrt(lambda4 * lambda4 + e * e)) / 2 , (lambda4 - sqrt(lambda4 * lambda4 + e * e)) / 2 };
		    
			int alpha = 1- k;
			int beta = k;

			int n = 0;
				double w = (3 - gamma) * (lambda3h[n] + lambda4h[n]) * a * a/2/(gamma-1);
				double lambda0 = 2 * (gamma - 1) * lambda1h[n];
				double U1 = u1(i, j) - a * alpha;
				double U2 = u1(i, j) + a * alpha;
				double V1 = v1(i, j) - a * beta;
				double V2 = v1(i, j) + a * beta;
				Vector4d f1(lambda0 + lambda3h[n] + lambda4h[n], lambda0 * u1(i, j) + lambda3h[n] * U1 + lambda4h[n] * U2,lambda0 * v1(i, j) + lambda3h[n] * V1 + lambda4h[n] * V2,lambda0 * (pow(u1(i, j), 2)+pow(v1(i, j),2))/2 + lambda3h[n] * (pow(U1,2) + pow(V1,2)) / 2 + lambda4h[n] * (pow(U2, 2) + pow(V2, 2)) / 2+ w );
				f1= f1 * r1(i, j) / 2 / gamma;
				
				n = 1;
				w = (3 - gamma) * (lambda3h[n] + lambda4h[n]) * a * a / 2 / (gamma - 1);
				lambda0 = 2 * (gamma - 1) * lambda1h[n];
				U1 = u1(i, j) - a * alpha;
				U2 = u1(i, j) + a * alpha;
				V1 = v1(i, j) - a * beta;
				V2 = v1(i, j) + a * beta;
				Vector4d f2(lambda0 + lambda3h[n] + lambda4h[n], lambda0 * u1(i, j) + lambda3h[n] * U1 + lambda4h[n] * U2,lambda0 * v1(i, j) + lambda3h[n] * V1 + lambda4h[n] * V2,lambda0 * (pow(u1(i, j), 2) + pow(v1(i, j), 2)) / 2 + lambda3h[n] * (pow(U1, 2) + pow(V1, 2)) / 2 + lambda4h[n] * (pow(U2, 2) + pow(V2, 2)) / 2 + w);
				f2 = f2 * r1(i, j) / 2 / gamma;
				
	
			
			

			
			for (int m = 0; m < 4; m++) {

				(*pos1[m])(i, j) = f1[m];
				(*neg1[m])(i, j) = f2[m];

			}
			

		}
	}

	for (int i = 0; i <= M2; i++) {
		for (int j = 0; j <= N2; j++) {
			double a = sqrt(gamma * p2(i, j) / r2(i, j));
			double lambda1 = u2(i, j) * (1 - k) + v2(i, j) * k;
			double lambda3 = lambda1 - a;
			double lambda4 = lambda1 + a;


			double lambda1h[2] = { (lambda1 + sqrt(lambda1 * lambda1 + e * e)) / 2, (lambda1 - sqrt(lambda1 * lambda1 + e * e)) / 2 };
			double lambda3h[2] = { (lambda3 + sqrt(lambda3 * lambda3 + e * e)) / 2, (lambda3 - sqrt(lambda3 * lambda3 + e * e)) / 2 };
			double lambda4h[2] = { (lambda4 + sqrt(lambda4 * lambda4 + e * e)) / 2 , (lambda4 - sqrt(lambda4 * lambda4 + e * e)) / 2 };

			int alpha = 1 - k;
			int beta = k;
		
			int n = 0;
				double w = (3 - gamma) * (lambda3h[n] + lambda4h[n]) * a * a / 2 / (gamma - 1);
				double lambda0 = 2 * (gamma - 1) * lambda1h[i];
				double U1 = u2(i, j) - a * alpha;
				double U2 = u2(i, j) + a * alpha;
				double V1 = v2(i, j) - a * beta;
				double V2 = v2(i, j) + a * beta;
				Vector4d f1(lambda0 + lambda3h[n] + lambda4h[n], lambda0 * u2(i, j) + lambda3h[n] * U1 + lambda4h[n] * U2,lambda0 * v2(i, j) + lambda3h[i] * V1 + lambda4h[i] * V2,lambda0 * (pow(u2(i, j), 2) + pow(v2(i, j), 2)) / 2 + lambda3h[n] * (pow(U1, 2) + pow(V1, 2) )/ 2 + lambda4h[n] * (pow(U2, 2) + pow(V2, 2)) / 2 + w);
				f1 = f1 * r2(i, j) / 2 / gamma;
				
			    n = 1;
				w = (3 - gamma) * (lambda3h[n] + lambda4h[n]) * a * a / 2 / (gamma - 1);
				lambda0 = 2 * (gamma - 1) * lambda1h[i];
				U1 = u2(i, j) - a * alpha;
				U2 = u2(i, j) + a * alpha;
				V1 = v2(i, j) - a * beta;
				V2 = v2(i, j) + a * beta;
				Vector4d f2(lambda0 + lambda3h[n] + lambda4h[n], lambda0 * u2(i, j) + lambda3h[n] * U1 + lambda4h[n] * U2,lambda0 * v2(i, j) + lambda3h[i] * V1 + lambda4h[i] * V2,lambda0 * (pow(u2(i, j), 2) + pow(v2(i, j), 2)) / 2 + lambda3h[n] * (pow(U1, 2) + pow(V1, 2) )/ 2 + lambda4h[n] * (pow(U2, 2) + pow(V2, 2)) / 2 + w);
				f2 = f2 * r2(i, j) / 2 / gamma;



			for (int m = 0; m < 4; m++) {
				(*pos2[m])(i, j) = f1[m];
				(*neg2[m])(i, j) = f2[m];
			}

		}
	}
}

void space(MatrixXd* LQ1[], MatrixXd* LQ2[]) {
	MatrixXd* F_p1[4], * F_n1[4], * G_p1[4], * G_n1[4];
	int M = M1, N = N1;
	MatrixXd F1_p1(M + 1, N + 1);     F_p1[0] = &F1_p1;
	MatrixXd F2_p1(M + 1, N + 1);     F_p1[1] = &F2_p1;
	MatrixXd F3_p1(M + 1, N + 1);     F_p1[2] = &F3_p1;
	MatrixXd F4_p1(M + 1, N + 1);     F_p1[3] = &F4_p1;
	MatrixXd G1_p1(M + 1, N + 1);     G_p1[0] = &G1_p1;
	MatrixXd G2_p1(M + 1, N + 1);     G_p1[1] = &G2_p1;
	MatrixXd G3_p1(M + 1, N + 1);     G_p1[2] = &G3_p1;
	MatrixXd G4_p1(M + 1, N + 1);     G_p1[3] = &G4_p1;
	MatrixXd F1_n1(M + 1, N + 1);     F_n1[0] = &F1_n1;
	MatrixXd F2_n1(M + 1, N + 1);     F_n1[1] = &F2_n1;
	MatrixXd F3_n1(M + 1, N + 1);     F_n1[2] = &F3_n1;
	MatrixXd F4_n1(M + 1, N + 1);     F_n1[3] = &F4_n1;
	MatrixXd G1_n1(M + 1, N + 1);     G_n1[0] = &G1_n1;
	MatrixXd G2_n1(M + 1, N + 1);     G_n1[1] = &G2_n1;
	MatrixXd G3_n1(M + 1, N + 1);     G_n1[2] = &G3_n1;
	MatrixXd G4_n1(M + 1, N + 1);     G_n1[3] = &G4_n1;

	MatrixXd* F_p2[4], * F_n2[4], * G_p2[4], * G_n2[4];
	M = M2, N = N2;
	MatrixXd F1_p2(M + 1, N + 1);     F_p2[0] = &F1_p2;
	MatrixXd F2_p2(M + 1, N + 1);     F_p2[1] = &F2_p2;
	MatrixXd F3_p2(M + 1, N + 1);     F_p2[2] = &F3_p2;
	MatrixXd F4_p2(M + 1, N + 1);     F_p2[3] = &F4_p2;
	MatrixXd G1_p2(M + 1, N + 1);     G_p2[0] = &G1_p2;
	MatrixXd G2_p2(M + 1, N + 1);     G_p2[1] = &G2_p2;
	MatrixXd G3_p2(M + 1, N + 1);     G_p2[2] = &G3_p2;
	MatrixXd G4_p2(M + 1, N + 1);     G_p2[3] = &G4_p2;
	MatrixXd F1_n2(M + 1, N + 1);     F_n2[0] = &F1_n2;
	MatrixXd F2_n2(M + 1, N + 1);     F_n2[1] = &F2_n2;
	MatrixXd F3_n2(M + 1, N + 1);     F_n2[2] = &F3_n2;
	MatrixXd F4_n2(M + 1, N + 1);     F_n2[3] = &F4_n2;
	MatrixXd G1_n2(M + 1, N + 1);     G_n2[0] = &G1_n2;
	MatrixXd G2_n2(M + 1, N + 1);     G_n2[1] = &G2_n2;
	MatrixXd G3_n2(M + 1, N + 1);     G_n2[2] = &G3_n2;
	MatrixXd G4_n2(M + 1, N + 1);     G_n2[3] = &G4_n2;


	split(F_p1, F_n1, F_p2, F_n2, 0);
	split(G_p1, G_n1, G_p2, G_n2, 1);


 //左内部（不包含壁面和第一圈）
	for (int i = 2; i < M1-1 ; i++) {
		for (int j = 2; j < N1-1; j++) {
			for (int k = 0; k < 4; k++) {

				double fpp = (*F_p1[k])(i, j) + minmod((*F_p1[k])(i + 1, j) - (*F_p1[k])(i, j), (*F_p1[k])(i, j) - (*F_p1[k])(i - 1, j)) * 0.5;
				double fnp = (*F_n1[k])(i + 1, j) + minmod((*F_n1[k])(i + 2, j) - (*F_n1[k])(i + 1, j), (*F_n1[k])(i + 1, j) - (*F_n1[k])(i, j)) * 0.5;
				double fpn = (*F_p1[k])(i - 1, j) + minmod((*F_p1[k])(i, j) - (*F_p1[k])(i - 1, j), (*F_p1[k])(i - 1, j) - (*F_p1[k])(i - 2, j)) * 0.5;
				double fnn = (*F_n1[k])(i, j) + minmod((*F_n1[k])(i + 1, j) - (*F_n1[k])(i, j), (*F_n1[k])(i, j) - (*F_n1[k])(i - 1, j)) * 0.5;

				double gpp = (*G_p1[k])(i, j) + minmod((*G_p1[k])(i, j + 1) - (*G_p1[k])(i, j), (*G_p1[k])(i, j) - (*G_p1[k])(i, j - 1)) * 0.5;
				double gnp = (*G_n1[k])(i, j + 1) + minmod((*G_n1[k])(i, j + 2) - (*G_n1[k])(i, j + 1), (*G_n1[k])(i, j + 1) - (*G_n1[k])(i, j)) * 0.5;
				double gpn = (*G_p1[k])(i, j - 1) + minmod((*G_p1[k])(i, j) - (*G_p1[k])(i, j - 1), (*G_p1[k])(i, j - 1) - (*G_p1[k])(i, j - 2)) * 0.5;
				double gnn = (*G_n1[k])(i, j) + minmod((*G_n1[k])(i, j + 1) - (*G_n1[k])(i, j), (*G_n1[k])(i, j) - (*G_n1[k])(i, j - 1)) * 0.5;
				
				(*LQ1[k])(i, j) = ((fpp + fnp - fpn - fnn)/dx1 + (gpp + gnp - gpn - gnn)/dy1) * -1;
			}
		}
	}

	//右内部（不包含壁面和第一圈）
	for (int i = 2; i < M2 - 1; i++) {
		for (int j = 2; j < N2 - 1; j++) {
			for (int k = 0; k < 4; k++) {
				double fpp = (*F_p2[k])(i, j) + minmod((*F_p2[k])(i + 1, j) - (*F_p2[k])(i, j), (*F_p2[k])(i, j) - (*F_p2[k])(i - 1, j)) * 0.5;
				double fnp = (*F_n2[k])(i + 1, j) + minmod((*F_n2[k])(i + 2, j) - (*F_n2[k])(i + 1, j), (*F_n2[k])(i + 1, j) - (*F_n2[k])(i, j)) * 0.5;
				double fpn = (*F_p2[k])(i - 1, j) + minmod((*F_p2[k])(i, j) - (*F_p2[k])(i - 1, j), (*F_p2[k])(i - 1, j) - (*F_p2[k])(i - 2, j)) * 0.5;
				double fnn = (*F_n2[k])(i, j) + minmod((*F_n2[k])(i + 1, j) - (*F_n2[k])(i, j), (*F_n2[k])(i, j) - (*F_n2[k])(i - 1, j)) * 0.5;

				double gpp = (*G_p2[k])(i, j) + minmod((*G_p2[k])(i, j + 1) - (*G_p2[k])(i, j), (*G_p2[k])(i, j) - (*G_p2[k])(i, j - 1)) * 0.5;
				double gnp = (*G_n2[k])(i, j + 1) + minmod((*G_n2[k])(i, j + 2) - (*G_n2[k])(i, j + 1), (*G_n2[k])(i, j + 1) - (*G_n2[k])(i, j)) * 0.5;
				double gpn = (*G_p2[k])(i, j - 1) + minmod((*G_p2[k])(i, j) - (*G_p2[k])(i, j - 1), (*G_p2[k])(i, j - 1) - (*G_p2[k])(i, j - 2)) * 0.5;
				double gnn = (*G_n2[k])(i, j) + minmod((*G_n2[k])(i, j + 1) - (*G_n2[k])(i, j), (*G_n2[k])(i, j) - (*G_n2[k])(i, j - 1)) * 0.5;
				
				(*LQ2[k])(i, j) = ((fpp + fnp - fpn - fnn) / dx2 + (gpp + gnp - gpn - gnn) / dy2) * -1;
			}
		}
	}


	for (int i = 1; i < M1; i++) {
		for (int k = 0; k < 4; k++) {
			(*LQ1[k])(i, 0) = (((*F_p1[k])(i, 0) + (*F_n1[k])(i + 1, 0) - (*F_p1[k])(i - 1, 0) - (*F_n1[k])(i, 0))/dx1 +
				2 * ((*G_p1[k])(i, 0) + (*G_n1[k])(i, 1) - (*G_p1[k])(i, 0) - (*G_n1[k])(i, 0))/dy1) * -1;

			(*LQ1[k])(i, N1) = (((*F_p1[k])(i, N1) + (*F_n1[k])(i + 1, N1) - (*F_p1[k])(i - 1, N1) - (*F_n1[k])(i, N1))/dx1 +
				2 * ((*G_p1[k])(i, N1) + (*G_n1[k])(i, N) - (*G_p1[k])(i, N1) - (*G_n1[k])(i, N1))/dy1) * -1;

		}
	}

	for (int i = 1; i < M2; i++) {
		for (int k = 0; k < 4; k++) {
			(*LQ2[k])(i, 0) = (((*F_p2[k])(i, 0) + (*F_n2[k])(i + 1, 0) - (*F_p2[k])(i - 1, 0) - (*F_n2[k])(i, 0))/dx2 +
				2 * ((*G_p2[k])(i, 0) + (*G_n2[k])(i, 1) - (*G_p2[k])(i, 0) - (*G_n2[k])(i, 0))/dy2) * -1;
            
			(*LQ2[k])(i, N2) = (((*F_p2[k])(i, N2) + (*F_n2[k])(i + 1, N2) - (*F_p2[k])(i - 1, N2) - (*F_n2[k])(i, N2))/dx2 +
				2 * ((*G_p2[k])(i, N2) + (*G_n2[k])(i, N) - (*G_p2[k])(i, N2) - (*G_n2[k])(i, N2))/dy2) * -1;
		}
	}

	N = N1;
	for (int i = 1; i < M1; i++) {
		for (int k = 0; k < 4; k++) {
			(*LQ1[k])(i, N - 1) = (((*F_p1[k])(i, N - 1) + (*F_n1[k])(i + 1, N - 1) - (*F_p1[k])(i - 1, N - 1) - (*F_n1[k])(i, N - 1) )/dx1+
				((*G_p1[k])(i, N - 1) + (*G_n1[k])(i, N) - (*G_p1[k])(i, N - 2) - (*G_n1[k])(i, N - 1))/dy1) * -1;

			(*LQ1[k])(i, 1) = (((*F_p1[k])(i, 1) + (*F_n1[k])(i + 1, 1) - (*F_p1[k])(i - 1, 1) - (*F_n1[k])(i, 1))/dx1 +
				((*G_p1[k])(i, 1) + (*G_n1[k])(i, 2) - (*G_p1[k])(i, 0) - (*G_n1[k])(i, 1))/dy1) * -1;


		}
	}

	N = N2;
	for (int i = 1; i < M2; i++) {
		for (int k = 0; k < 4; k++) {
			(*LQ2[k])(i, N - 1) = (((*F_p2[k])(i, N - 1) + (*F_n2[k])(i + 1, N - 1) - (*F_p2[k])(i - 1, N - 1) - (*F_n2[k])(i, N - 1))/dx2 +
				((*G_p2[k])(i, N - 1) + (*G_n2[k])(i, N) - (*G_p2[k])(i, N - 2) - (*G_n2[k])(i, N - 1))/dy2) * -1;

			(*LQ2[k])(i, 1) = (((*F_p2[k])(i, 1) + (*F_n2[k])(i + 1, 1) - (*F_p2[k])(i - 1, 1) - (*F_n2[k])(i, 1))/dx2 +
				((*G_p2[k])(i, 1) + (*G_n2[k])(i, 2) - (*G_p2[k])(i, 0) - (*G_n2[k])(i, 1)) /dy2)* -1;


		}
	}
	M = M1;
	for (int j = 2; j < N1-1; j++) {
		for (int k = 0; k < 4; k++) {
			(*LQ1[k])(1, j) = (((*F_p1[k])(1, j) + (*F_n1[k])(1 + 1, j) - (*F_p1[k])(1 - 1, j) - (*F_n1[k])(1, j))/dx1 +
				((*G_p1[k])(1, j) + (*G_n1[k])(1, j + 1) - (*G_p1[k])(1, j - 1) - (*G_n1[k])(1, j))/dy1) * -1;

			if (j < N1 - N2) {
				(*LQ1[k])(M - 1, j) = (((*F_p1[k])(M - 1, j) + (*F_n1[k])(M - 1 + 1, j) - (*F_p1[k])(M - 1 - 1, j) - (*F_n1[k])(M - 1, j))/dx1 +
				((*G_p1[k])(M - 1, j) + (*G_n1[k])(M - 1, j + 1) - (*G_p1[k])(M - 1, j - 1) - (*G_n1[k])(M - 1, j))/dy1) * -1;

			}
			else {
				int i = M - 1;
				double fpp = (*F_p1[k])(i, j) + minmod((*F_p1[k])(i + 1, j) - (*F_p1[k])(i, j), (*F_p1[k])(i, j) - (*F_p1[k])(i - 1, j)) * 0.5;
				double fnp = (*F_n1[k])(i + 1, j) + minmod((*F_n2[k])(0, j-N1+N2) - (*F_n1[k])(i + 1, j), (*F_n1[k])(i + 1, j) - (*F_n1[k])(i, j)) * 0.5;
				double fpn = (*F_p1[k])(i - 1, j) + minmod((*F_p1[k])(i, j) - (*F_p1[k])(i - 1, j), (*F_p1[k])(i - 1, j) - (*F_p1[k])(i - 2, j)) * 0.5;
				double fnn = (*F_n1[k])(i, j) + minmod((*F_n1[k])(i + 1, j) - (*F_n1[k])(i, j), (*F_n1[k])(i, j) - (*F_n1[k])(i - 1, j)) * 0.5;

				double gpp = (*G_p1[k])(i, j) + minmod((*G_p1[k])(i, j + 1) - (*G_p1[k])(i, j), (*G_p1[k])(i, j) - (*G_p1[k])(i, j - 1)) * 0.5;
				double gnp = (*G_n1[k])(i, j + 1) + minmod((*G_n1[k])(i, j + 2) - (*G_n1[k])(i, j + 1), (*G_n1[k])(i, j + 1) - (*G_n1[k])(i, j)) * 0.5;
				double gpn = (*G_p1[k])(i, j - 1) + minmod((*G_p1[k])(i, j) - (*G_p1[k])(i, j - 1), (*G_p1[k])(i, j - 1) - (*G_p1[k])(i, j - 2)) * 0.5;
				double gnn = (*G_n1[k])(i, j) + minmod((*G_n1[k])(i, j + 1) - (*G_n1[k])(i, j), (*G_n1[k])(i, j) - (*G_n1[k])(i, j - 1)) * 0.5;

				(*LQ1[k])(i, j) = ((fpp + fnp - fpn - fnn) / dx1 + (gpp + gnp - gpn - gnn) / dy1) * -1;
			}

		}
	}

	M = M2;
	for (int j = 2; j < N2-1; j++) {
		for (int k = 0; k < 4; k++) {
			(*LQ2[k])(N2-1, j) = (((*F_p2[k])(N2 - 1, j) + (*F_n2[k])(N2 - 1 + 1, j) - (*F_p2[k])(N2 - 1 - 1, j) - (*F_n2[k])(N2 - 1, j))/dx2 +
				((*G_p2[k])(N2 - 1, j) + (*G_n2[k])(N2 - 1, j + 1) - (*G_p2[k])(N2 - 1, j - 1) - (*G_n2[k])(N2 - 1, j))/dy2) * -1;

			
			int i =  1;
			double fpp = (*F_p2[k])(i, j) + minmod((*F_p2[k])(i + 1, j) - (*F_p2[k])(i, j), (*F_p2[k])(i, j) - (*F_p2[k])(i - 1, j)) * 0.5;
			double fnp = (*F_n2[k])(i + 1, j) + minmod((*F_n2[k])(i + 2, j) - (*F_n2[k])(i + 1, j), (*F_n2[k])(i + 1, j) - (*F_n2[k])(i, j)) * 0.5;
			double fpn = (*F_p2[k])(i - 1, j) + minmod((*F_p2[k])(i, j) - (*F_p2[k])(i - 1, j), (*F_p2[k])(i - 1, j) - (*F_p1[k])(M1, j+N1-N2)) * 0.5;
			double fnn = (*F_n2[k])(i, j) + minmod((*F_n2[k])(i + 1, j) - (*F_n2[k])(i, j), (*F_n2[k])(i, j) - (*F_n2[k])(i - 1, j)) * 0.5;

			double gpp = (*G_p2[k])(i, j) + minmod((*G_p2[k])(i, j + 1) - (*G_p2[k])(i, j), (*G_p2[k])(i, j) - (*G_p2[k])(i, j - 1)) * 0.5;
			double gnp = (*G_n2[k])(i, j + 1) + minmod((*G_n2[k])(i, j + 2) - (*G_n2[k])(i, j + 1), (*G_n2[k])(i, j + 1) - (*G_n2[k])(i, j)) * 0.5;
			double gpn = (*G_p2[k])(i, j - 1) + minmod((*G_p2[k])(i, j) - (*G_p2[k])(i, j - 1), (*G_p2[k])(i, j - 1) - (*G_p2[k])(i, j - 2)) * 0.5;
			double gnn = (*G_n2[k])(i, j) + minmod((*G_n2[k])(i, j + 1) - (*G_n2[k])(i, j), (*G_n2[k])(i, j) - (*G_n2[k])(i, j - 1)) * 0.5;

			(*LQ2[k])(i, j) = ((fpp + fnp - fpn - fnn) / dx2 + (gpp + gnp - gpn - gnn) / dy2) * -1;


		}
	}
	M = M1;
	for (int j = 2; j < N1-1; j++) {
		for (int k = 0; k < 4; k++) {
		

			if (j < N1 - N2) {
				(*LQ1[k])(M, j) = (((*F_p1[k])(M, j) + (*F_n1[k])(M, j) - (*F_p1[k])(M - 1, j) - (*F_n1[k])(M, j))*2/dx1 +
					((*G_p1[k])(1, j) + (*G_n1[k])(1, j + 1) - (*G_p1[k])(1, j - 1) - (*G_n1[k])(1, j))/dy1) * -1;

			}
			else {
				int i = M ;
				double fpp = (*F_p1[k])(i, j) + minmod((*F_p2[k])(0, j - N1 + N2) - (*F_p1[k])(i, j), (*F_p1[k])(i, j) - (*F_p1[k])(i - 1, j)) * 0.5;
				double fnp = (*F_n2[k])(0, j-N1+N2) + minmod((*F_n2[k])(1, j - N1 + N2) - (*F_n2[k])(0, j-N1+N2), (*F_n2[k])(0, j-N1+N2) - (*F_n1[k])(i, j)) * 0.5;
				double fpn = (*F_p1[k])(i - 1, j) + minmod((*F_p1[k])(i, j) - (*F_p1[k])(i - 1, j), (*F_p1[k])(i - 1, j) - (*F_p1[k])(i - 2, j)) * 0.5;
				double fnn = (*F_n1[k])(i, j) + minmod((*F_n2[k])(0, j-N1+N2) - (*F_n1[k])(i, j), (*F_n1[k])(i, j) - (*F_n1[k])(i - 1, j)) * 0.5;

				double gpp = (*G_p1[k])(i, j) + minmod((*G_p1[k])(i, j + 1) - (*G_p1[k])(i, j), (*G_p1[k])(i, j) - (*G_p1[k])(i, j - 1)) * 0.5;
				double gnp = (*G_n1[k])(i, j + 1) + minmod((*G_n1[k])(i, j + 2) - (*G_n1[k])(i, j + 1), (*G_n1[k])(i, j + 1) - (*G_n1[k])(i, j)) * 0.5;
				double gpn = (*G_p1[k])(i, j - 1) + minmod((*G_p1[k])(i, j) - (*G_p1[k])(i, j - 1), (*G_p1[k])(i, j - 1) - (*G_p1[k])(i, j - 2)) * 0.5;
				double gnn = (*G_n1[k])(i, j) + minmod((*G_n1[k])(i, j + 1) - (*G_n1[k])(i, j), (*G_n1[k])(i, j) - (*G_n1[k])(i, j - 1)) * 0.5;

				(*LQ1[k])(i, j) = ((fpp + fnp - fpn - fnn) / dx1 + (gpp + gnp - gpn - gnn) / dy1) * -1;
			}

		}
	}

	M = M2;
	for (int j = 2; j < N2-1; j++) {
		for (int k = 0; k < 4; k++) {

			int i = 0;
			double fpp = (*F_p2[k])(i, j) + minmod((*F_p2[k])(i + 1, j) - (*F_p2[k])(i, j), (*F_p2[k])(i, j) - (*F_p1[k])(M1, j + N1 - N2)) * 0.5;
			double fnp = (*F_n2[k])(i + 1, j) + minmod((*F_n2[k])(i + 2, j) - (*F_n2[k])(i + 1, j), (*F_n2[k])(i + 1, j) - (*F_n2[k])(i, j)) * 0.5;
			double fpn = (*F_p1[k])(M1, j+N1-N2) + minmod((*F_p2[k])(i, j) - (*F_p1[k])(M1, j+N1-N2), (*F_p1[k])(M1, j+N1-N2) - (*F_p1[k])(M1-1, j + N1 - N2)) * 0.5;
			double fnn = (*F_n2[k])(i, j) + minmod((*F_n2[k])(i + 1, j) - (*F_n2[k])(i, j), (*F_n2[k])(i, j) - (*F_n1[k])(M1, j+N1-N2)) * 0.5;

			double gpp = (*G_p2[k])(i, j) + minmod((*G_p2[k])(i, j + 1) - (*G_p2[k])(i, j), (*G_p2[k])(i, j) - (*G_p2[k])(i, j - 1)) * 0.5;
			double gnp = (*G_n2[k])(i, j + 1) + minmod((*G_n2[k])(i, j + 2) - (*G_n2[k])(i, j + 1), (*G_n2[k])(i, j + 1) - (*G_n2[k])(i, j)) * 0.5;
			double gpn = (*G_p2[k])(i, j - 1) + minmod((*G_p2[k])(i, j) - (*G_p2[k])(i, j - 1), (*G_p2[k])(i, j - 1) - (*G_p2[k])(i, j - 2)) * 0.5;
			double gnn = (*G_n2[k])(i, j) + minmod((*G_n2[k])(i, j + 1) - (*G_n2[k])(i, j), (*G_n2[k])(i, j) - (*G_n2[k])(i, j - 1)) * 0.5;

			(*LQ2[k])(i, j) = ((fpp + fnp - fpn - fnn) / dx2 + (gpp + gnp - gpn - gnn) / dy2) * -1;


		}
	}


}


void time() {
	MatrixXd Q1 =  r1.array();
	MatrixXd Q2 = r1.array() * u1.array();
	MatrixXd Q3 = r1.array() * v1.array();
	MatrixXd Q4 = (p1.array() / (gamma - 1) + 0.5 * r1.array() * (u1.array() * u1.array() + v1.array() * v1.array()));
	MatrixXd* LQ[4];

	MatrixXd LQ1 = MatrixXd::Zero(M1 + 1, N1 + 1); LQ[0] = &LQ1;
	MatrixXd LQ2 = MatrixXd::Zero(M1 + 1, N1 + 1); LQ[1] = &LQ2;
	MatrixXd LQ3 = MatrixXd::Zero(M1 + 1, N1 + 1); LQ[2] = &LQ3;
	MatrixXd LQ4 = MatrixXd::Zero(M1 + 1, N1 + 1); LQ[3] = &LQ4;

	MatrixXd Q1r = r2.array();
	MatrixXd Q2r = r2.array() * u2.array();
	MatrixXd Q3r = r2.array() * v2.array();
	MatrixXd Q4r = (p2.array() / (gamma - 1) + 0.5 * r2.array() * (u2.array() * u2.array() + v2.array() * v2.array()));
	
	MatrixXd* LQr[4];

	MatrixXd LQ1r = MatrixXd::Zero(M2 + 1, N2 + 1); LQr[0] = &LQ1r;
	MatrixXd LQ2r = MatrixXd::Zero(M2 + 1, N2 + 1); LQr[1] = &LQ2r;
	MatrixXd LQ3r = MatrixXd::Zero(M2 + 1, N2 + 1); LQr[2] = &LQ3r;
	MatrixXd LQ4r = MatrixXd::Zero(M2 + 1, N2 + 1); LQr[3] = &LQ4r;


	space(LQ,LQr);
	MatrixXd temp1 = Q1 + (*LQ[0]) * dt;
	MatrixXd temp2 = Q2 + (*LQ[1]) * dt;
	MatrixXd temp3 = Q3 + (*LQ[2]) * dt;
	MatrixXd temp4 = Q4 + (*LQ[3]) * dt;
	r1 = temp1.array();
	u1 = temp2.array() / temp1.array();
	v1 = temp3.array() / temp1.array();
	p1 = (temp4.array() - 0.5 * r1.array() * (u1.array() * u1.array() + v1.array() * v1.array())) * (gamma - 1);


	MatrixXd temp1r = Q1r + (*LQr[0]) * dt;
	MatrixXd temp2r = Q2r + (*LQr[1]) * dt;
	MatrixXd temp3r = Q3r + (*LQr[2]) * dt;
	MatrixXd temp4r = Q4r + (*LQr[3]) * dt;
	r2 = temp1r.array();
	u2 = temp2r.array() / temp1r.array();
	v2 = temp3r.array() / temp1r.array();
	p2 = (temp4r.array() - 0.5 * r2.array() * (u2.array() * u2.array() + v2.array() * v2.array())) * (gamma - 1);
	//ofstream uout("./result/u.txt");
	//uout << u;
	//uout.close();
	//ofstream vout("./result/v.txt");
	//vout << v;
	//vout.close();
	//ofstream pout("./result/p.txt");
	//pout << p;
	//pout.close();
	//ofstream rout("./result/r.txt");
	//rout << r;
	//rout.close();

	boundary();
	space(LQ,LQr);
	temp1 = 0.75 * Q1 + (temp1 + (*LQ[0]) * dt) * 0.25;
	temp2 = 0.75 * Q2 + (temp2 + (*LQ[1]) * dt) * 0.25;
	temp3 = 0.75 * Q3 + (temp3 + (*LQ[2]) * dt) * 0.25;
	temp4 = 0.75 * Q4 + (temp4 + (*LQ[3]) * dt) * 0.25;
	r1 = temp1.array();
	u1 = temp2.array() / temp1.array();
	v1 = temp3.array() / temp1.array();
	p1 = (temp4.array() - 0.5 * r1.array() * (u1.array() * u1.array() + v1.array() * v1.array())) * (gamma - 1);

	temp1r = 0.75 * Q1r + (temp1r + (*LQr[0]) * dt) * 0.25;
	temp2r = 0.75 * Q2r + (temp2r + (*LQr[1]) * dt) * 0.25;
	temp3r = 0.75 * Q3r + (temp3r + (*LQr[2]) * dt) * 0.25;
	temp4r = 0.75 * Q4r + (temp4r + (*LQr[3]) * dt) * 0.25;
	r2 = temp1r.array();
	u2 = temp2r.array() / temp1r.array();
	v2 = temp3r.array() / temp1r.array();
	p2 = (temp4r.array() - 0.5 * r2.array() * (u2.array() * u2.array() + v2.array() * v2.array())) * (gamma - 1);
	//uout << u;
	//uout.close();

	//vout << v;
	//vout.close();

	//pout << p;
	//pout.close();

	//rout << r;
	//rout.close();

	boundary();
	space(LQ,LQr);
	temp1 = 1.0 / 3.0 * Q1 + (temp1 + (*LQ[0]) * dt) * 2 / 3.0;
	temp2 = 1.0 / 3.0 * Q2 + (temp2 + (*LQ[1]) * dt) * 2 / 3.0;
	temp3 = 1.0 / 3.0 * Q3 + (temp3 + (*LQ[2]) * dt) * 2 / 3.0;
	temp4 = 1.0 / 3.0 * Q4 + (temp4 + (*LQ[3]) * dt) * 2 / 3.0;
	r1 = temp1.array();
	u1 = temp2.array() / temp1.array();
	v1 = temp3.array() / temp1.array();
	p1 = (temp4.array() - 0.5 * r1.array() * (u1.array() * u1.array() + v1.array() * v1.array())) * (gamma - 1);

	temp1r = 1.0 / 3.0 * Q1r + (temp1r + (*LQr[0]) * dt) * 2 / 3.0;
	temp2r = 1.0 / 3.0 * Q2r + (temp2r + (*LQr[1]) * dt) * 2 / 3.0;
	temp3r = 1.0 / 3.0 * Q3r + (temp3r + (*LQr[2]) * dt) * 2 / 3.0;
	temp4r = 1.0 / 3.0 * Q4r + (temp4r + (*LQr[3]) * dt) * 2 / 3.0;
	r2 = temp1r.array();
	u2 = temp2r.array() / temp1r.array();
	v2 = temp3r.array() / temp1r.array();
	p2 = (temp4r.array() - 0.5 * r2.array() * (u2.array() * u2.array() + v2.array() * v2.array())) * (gamma - 1);


	//uout << u;
	//uout.close();

	//vout << v;
	//vout.close();
	//
	//pout << p;
	//pout.close();

	//rout << r;
	//rout.close();

}





void init_breakpoint(int index1) {
	char filename[30];
	sprintf_s(filename, "./result/u1_%d.txt", index1);
	readPortfolio(u1, filename);
	sprintf_s(filename, "./result/v1_%d.txt", index1);
	readPortfolio(v1, filename);
	sprintf_s(filename, "./result/p1_%d.txt", index1);
	readPortfolio(p1, filename);
	sprintf_s(filename, "./result/r1_%d.txt", index1);
	readPortfolio(r1, filename);
	sprintf_s(filename, "./result/u2_%d.txt", index1);
	readPortfolio(u2, filename);
	sprintf_s(filename, "./result/v2_%d.txt", index1);
	readPortfolio(v2, filename);
	sprintf_s(filename, "./result/p2_%d.txt", index1);
	readPortfolio(p2, filename);
	sprintf_s(filename, "./result/r2_%d.txt", index1);
	readPortfolio(r2, filename);
}

int main()
{


	init();
	//boundary();
	//init_breakpoint(360);
	boundary();
	
	char filename[30];


	
	int k = 1;
	//for (int k = 1; k <= 25; k++)
	while (k <= 10) {

		cout << "k" << k << endl;
		
		boundary();
		time();
	


		/*	sprintf_s(filename, "T%d.txt", k);
			ofstream Tout(filename);
			Tout << T;
			Tout.close();
			sprintf_s(filename, "L_xi%d.txt", k);
			ofstream xxout(filename);
			xxout << L_xi;
			xxout.close();
			sprintf_s(filename, "L_eta%d.txt", k);
			ofstream eeout(filename);
			eeout << L_eta;
			eeout.close();*/

			/*if (error_u < err&&error_v < err&&error_r < err&&error_p < err) {
			sprintf_s(filename, "p%d.txt", k);
			ofstream psiout(filename);
			psiout << p;
			psiout.close();
			sprintf_s(filename, "u%d.txt", k);
			ofstream uout(filename);
			uout << u;
			uout.close();
			sprintf_s(filename, "v%d.txt", k);
			ofstream vout(filename);
			vout << v;
			vout.close();
			sprintf_s(filename, "r%d.txt", k);
			ofstream rout(filename);
			rout << r;
			rout.close();

			aa = 1;
			}*/

		if (k % 1 == 0) {
			sprintf_s(filename, "./result/p1_%d.txt", k);
			ofstream psiout(filename);
			psiout << p1;
			psiout.close();
			sprintf_s(filename, "./result/u1_%d.txt", k);
			ofstream uout(filename);
			uout << u1;
			uout.close();
			sprintf_s(filename, "./result/v1_%d.txt", k);
			ofstream vout(filename);
			vout << v1;
			vout.close();
			sprintf_s(filename, "./result/r1_%d.txt", k);
			ofstream rout(filename);
			rout << r1;
			rout.close();
			sprintf_s(filename, "./result/p2_%d.txt", k);
			ofstream psiout2(filename);
			psiout2 << p2;
			psiout2.close();
			sprintf_s(filename, "./result/u2_%d.txt", k);
			ofstream uout2(filename);
			uout2 << u2;
			uout2.close();
			sprintf_s(filename, "./result/v2_%d.txt", k);
			ofstream vout2(filename);
			vout2 << v2;
			vout2.close();
			sprintf_s(filename, "./result/r2_%d.txt", k);
			ofstream rout2(filename);
			rout2 << r2;
			rout2.close();
		}

		k = k + 1;
	}

	std::cout << "Hello World!\n";
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件

/*ofstream x1out("x_xi.txt");
x1out << X_xi;
x1out.close();
ofstream x2out("x_eta.txt");
x2out << X_eta;
x2out.close();
ofstream x3out("x_xixi.txt");
x3out << X_xixi;
x3out.close();
ofstream x4out("x_etaeta.txt");
x4out << X_etaeta;
x4out.close();
ofstream x5out("x_xieta.txt");
x5out << X_xieta;
x5out.close();
ofstream y1out("y_xi.txt");
y1out << X_xi;
y1out.close();
ofstream y2out("y_eta.txt");
y2out << X_eta;
y2out.close();
ofstream y3out("y_xixi.txt");
y3out << X_xixi;
y3out.close();
ofstream y4out("y_etaeta.txt");
y4out << X_etaeta;
y4out.close();
ofstream y5out("y_xieta.txt");
y5out << X_xieta;
y5out.close();*/
/*ofstream X1out("Xi_x.txt");
X1out << Xi_x;
X1out.close();
ofstream X2out("Eta_x.txt");
X2out << Eta_x;
X2out.close();
ofstream X3out("Xi_xx.txt");
X3out << Xi_xx;
X3out.close();
ofstream X4out("Eta_xx.txt");
X4out << Eta_xx;
X4out.close();
ofstream X5out("Xi_xy.txt");
X5out << Xi_xy;
X5out.close();
ofstream Y1out("Xi_y.txt");
Y1out << Xi_y;
Y1out.close();
ofstream Y2out("Eta_y.txt");
Y2out << Eta_y;
Y2out.close();
ofstream Y3out("Xi_yy.txt");
Y3out << Xi_yy;
Y3out.close();
ofstream Y4out("Eta_yy.txt");
Y4out << Eta_yy;
Y4out.close();
ofstream Y5out("Eta_xy.txt");
Y5out << Eta_xy;
Y5out.close();*/
//init();
//int index = 0;