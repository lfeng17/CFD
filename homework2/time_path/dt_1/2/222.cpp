﻿#include<fstream>
#include<iostream>
#include<Eigen/Dense>
#include<cmath>
#include <vector>
#include <string.h>
#include <stdio.h>

using namespace std;
using namespace Eigen;

#define M_PI 3.1415926535897932384626

#define random(x) (rand()%x)
double d1 = 2; //diameter of inner circle
double d2 = 40; //diameter of outter circle
#define M  120 //number of mesh along x
#define M1 50
#define M2 10
#define M3 30
#define N  40 //number of mesh along y
double dt = 0.001;
double CFL = 1;
double err;
double Ma = 0.8;
double gamma = 1.4;

MatrixXd X(M + 1, N + 1); //x coordinate
MatrixXd Y(M + 1, N + 1); //y coordinate
MatrixXd X_eta(M + 1, N + 1); //first order
MatrixXd X_xi(M + 1, N + 1); //first order
MatrixXd Y_eta(M + 1, N + 1); //first order
MatrixXd Y_xi(M + 1, N + 1); //first order
MatrixXd J(M + 1, N + 1);
MatrixXd Eta_x(M + 1, N + 1); //first order
MatrixXd Xi_x(M + 1, N + 1); //first order
MatrixXd Eta_y(M + 1, N + 1); //first order
MatrixXd Xi_y(M + 1, N + 1); //first order
MatrixXd L_xi(M + 1, N + 1);
MatrixXd L_eta(M + 1, N + 1);
MatrixXd T(M + 1, N + 1);
MatrixXd* Kapa_x[2];
MatrixXd* Kapa_y[2];

double d = 1.0;

MatrixXd r(M + 1, N + 1); //rho
MatrixXd u(M + 1, N + 1); //u velocity
MatrixXd v(M + 1, N + 1); //v velocity
MatrixXd p(M + 1, N + 1); //pressure
						  //MatrixXd e(M + 1, N + 1); //e=rho*E
						  //MatrixXd U(M + 1, N + 1); // U=Xi_x*u+Xi_y*v
						  //MatrixXd V(M + 1, N + 1); // V=Eta_x*u+Eta_y*v


void readPortfolio(MatrixXd& matrix, string b)  //this will be a function that should hopefully return a matrix to use in other functions.
{
	ifstream data(b);
	string lineOfData;

	if (data.is_open())
	{
		int i = 0;
		while (data.good())
		{
			char linebuff[4096];
			getline(data, lineOfData);
			strncpy_s(linebuff, lineOfData.c_str(), sizeof(linebuff) - 1);

			//cout << lineOfData << endl; //just to check if the data was read.
			{
				int j = 0;
				double val;
				char* p_r = NULL, *p_val;
				p_val = strtok_s(linebuff, " ,;", &p_r);
				while (NULL != p_val) {
					//?wrong??  sscanf_s("%lf", p_val, &val);
					val = atof(p_val);
					//cout << "  " << val;
					matrix(i, j) = val;
					j++;
					p_val = strtok_s(NULL, " ,;", &p_r);
				}
				//cout << endl;
			}
			i++;
		}
	}
	else { std::cout << "Unable to open file"; }

}

void Jacobi() {

	int index = 0;
	X_xi = MatrixXd::Zero(M + 1, N + 1);
	Y_xi = MatrixXd::Zero(M + 1, N + 1);
	X_eta = MatrixXd::Zero(M + 1, N + 1);
	Y_eta = MatrixXd::Zero(M + 1, N + 1);
	J = MatrixXd::Zero(M + 1, N + 1);
	for (int i = 1; i < M; i++) {
		index = i - 1;
		for (int j = 1; j < N; j++) {
			X_xi(i, j) = (X(i + 1, j) - X(index, j)) / 2.0 / d;
			Y_xi(i, j) = (Y(i + 1, j) - Y(index, j)) / 2.0 / d;
			X_eta(i, j) = (X(i, j + 1) - X(i, j - 1)) / 2.0 / d;
			Y_eta(i, j) = (Y(i, j + 1) - Y(i, j - 1)) / 2.0 / d;

		}
	}//inner


	for (int i = 1; i < M; i++) {
		index = i - 1;
		int j = N;
		X_xi(i, j) = (X(i + 1, j) - X(index, j)) / 2.0 / d;
		Y_xi(i, j) = (Y(i + 1, j) - Y(index, j)) / 2.0 / d;
		X_eta(i, j) = (X(i, j) - X(i, j - 1)) / d;
		Y_eta(i, j) = (Y(i, j) - Y(i, j - 1)) / d;
	} // up without corner

	for (int i = 0; i < M + 1; i += M) {
		for (int j = 1; j <= N; j++) {
			Y_xi(i, j) = 0;
			X_eta(i, j) = 0;
			Y_eta(i, j) = (Y(i, j) - Y(i, j - 1)) / d;
		}
	}

	for (int j = 1; j <= N; j++) {
		X_xi(0, j) = (X(1, j) - X(0, j)) / d;
		X_xi(M, j) = (X(M, j) - X(M - 1, j)) / d;
	}

	X_xi(0, 0) = (X(1, 0) - X(0, 0)) / d;
	X_xi(M, 0) = (X(M, 0) - X(M - 1, 0)) / d;

	for (int i = 0; i < M + 1; i += M) {
		Y_xi(i, 0) = 0;
		X_eta(i, 0) = 0;
		Y_eta(i, 0) = (Y(i, 1) - Y(i, 0)) / d;
	}

	//side incluing corner
	
	for (int i = 1; i < M; i++) {
		index = i - 1;
		int j = 0;
		X_xi(i, 0) = (X(i + 1, 0) - X(i - 1, 0)) / 2.0 / d;
		Y_xi(i, 0) = (Y(i + 1, 0) - Y(i - 1, 0)) / 2.0 / d;
		X_eta(i, 0) = (X(i, 1) - X(i, 0)) / d;
		Y_eta(i, 0) = (Y(i, 1) - Y(i, 0)) / d;
	}//down without corner

	/*for (int i = 1; i <= M3; i++) {
		X_eta(i, 0) = (X(i, 1) - X(M - i, 1)) / 2.0 / d;
		Y_eta(i, 0) = (Y(i, 1) - Y(M - i, 1)) / 2.0 / d;
	}*/


	for (int i = 0; i <= M; i++) {
		for (int j = 0; j <= N; j++) {
			double temp = X_xi(i, j) * Y_eta(i, j) - X_eta(i, j) * Y_xi(i, j);
			//cout << "temp" << temp << endl;
			J(i, j) = temp;
		}
	}

}

void deriviate() {


	Xi_x = MatrixXd::Zero(M + 1, N + 1);
	Eta_x = MatrixXd::Zero(M + 1, N + 1);
	Xi_y = MatrixXd::Zero(M + 1, N + 1);
	Eta_y = MatrixXd::Zero(M + 1, N + 1);

	for (int i = 0; i <= M; i++) {
		for (int j = 0; j <= N; j++) {
			Xi_x(i, j) = Y_eta(i, j) / J(i, j);
			Eta_x(i, j) = -Y_xi(i, j) / J(i, j);
			Xi_y(i, j) = -X_eta(i, j) / J(i, j);
			Eta_y(i, j) = X_xi(i, j) / J(i, j);

		}
	}
}

void init() {
	r = MatrixXd::Ones(M + 1, N + 1);
	p = MatrixXd::Ones(M + 1, N + 1);
	u = sqrt(gamma) * Ma * cos(1.25 / 180 * M_PI) * MatrixXd::Ones(M + 1, N + 1);
	v = sqrt(gamma) * Ma * sin(1.25 / 180 * M_PI) * MatrixXd::Ones(M + 1, N + 1);
	Kapa_x[0] = &Xi_x;
	Kapa_x[1] = &Eta_x;
	Kapa_y[0] = &Xi_y;
	Kapa_y[1] = &Eta_y;

	L_xi = MatrixXd::Zero(M + 1, N + 1);
	L_eta = MatrixXd::Zero(M + 1, N + 1);

}


void boundary() {


	for (int i = 0; i < M + 1; i++)
	{
		int j = N;
		u(i, j) = sqrt(gamma) * Ma * cos(1.25 / 180 * M_PI);
		v(i, j) = sqrt(gamma) * Ma * sin(1.25 / 180 * M_PI);
		r(i, j) = 1;
		p(i, j) = 1;
	}

	for (int j = 0; j < N; j++) {
		u(0, j) = sqrt(gamma) * Ma * cos(1.25 / 180 * M_PI);
		v(0, j) = sqrt(gamma) * Ma * sin(1.25 / 180 * M_PI);
		r(0, j) = 1;
		p(0, j) = 1;
		u(M, j) = sqrt(gamma) * Ma * cos(1.25 / 180 * M_PI);
		v(M, j) = sqrt(gamma) * Ma * sin(1.25 / 180 * M_PI);
		r(M, j) = 1;
		p(M, j) = 1;
	}



	for (int i = M3 + 1; i < M - M3; i++)
	{
		if (abs(Eta_y(i, 0)) > 0.1) v(i, 0) = -Eta_x(i, 0) * u(i, 0) / Eta_y(i, 0);
		else { u(i, 0) = 0; }
	}
	p(M3, 0) = p(M3, 0) + pow(v(M3, 0), 2) / 2 * r(M3, 0) * (gamma - 1) / gamma;
	p(M - M3, 0) = p(M3, 0);
	v(M3, 0) = 0;
	v(M - M3, 0) = 0;


	for (int i = 1; i <= M3; i++) {
		u(i, 0) = 0.5*(u(i, 1) + u(M - i, 1));
		v(i, 0) = 0.5*(v(i, 1) + v(M - i, 1));
		r(i, 0) = 0.5*(r(i, 1) + r(M - i, 1));
		p(i, 0) = 0.5*(p(i, 1) + p(M - i, 1));

		u(M - i, 0) = u(i, 0);
		v(M - i, 0) = v(i, 0);
		r(M - i, 0) = r(i, 0);
		p(M - i, 0) = p(i, 0);
	}

}

// pos , _p : positive ;  neg,_n : negative
void split(MatrixXd * pos[], MatrixXd* neg[], int k) {
	for (int i = 0; i <= M; i++) {
		for (int j = 0; j <= N; j++) {
			double a = sqrt(gamma * p(i, j) / r(i, j));
			double dk = sqrt(pow((*Kapa_x[k])(i, j), 2) + pow((*Kapa_y[k])(i, j), 2));
			double lambda1 = (*Kapa_x[k])(i, j) * u(i, j) + (*Kapa_y[k])(i, j) * v(i, j);
			double lambda3 = lambda1 + a * dk;
			double lambda4 = lambda1 - a * dk;
			
			if (k == 0) {
				L_xi(i, j) = fabs(lambda1) + a * dk;
			}
			if (k == 1) {
				L_eta(i, j) = fabs(lambda1) + a * dk;
			}

			double lambda1_p = (lambda1 + abs(lambda1)) / 2;
			double lambda3_p = (lambda3 + abs(lambda3)) / 2;
			double lambda4_p = (lambda4 + abs(lambda4)) / 2;
			double lambda1_n = (lambda1 - abs(lambda1)) / 2;
			double lambda3_n = (lambda3 - abs(lambda3)) / 2;
			double lambda4_n = (lambda4 - abs(lambda4)) / 2;
			double kh_x = (*Kapa_x[k])(i, j) / dk;
			double kh_y = (*Kapa_y[k])(i, j) / dk;
			double th = kh_x * u(i, j) + kh_y * v(i, j);
			double e = p(i, j) / (gamma - 1) + 0.5 * r(i, j) * (pow(u(i, j), 2) + pow(v(i, j), 2));
			Vector4d K1(r(i, j), r(i, j) * u(i, j), r(i, j) * v(i, j), e - p(i, j) / (gamma - 1));
			Vector4d K3(K1[0], K1[1] + r(i, j) * a * kh_x, K1[2] + r(i, j) * a * kh_y, e + p(i, j) + r(i, j) * a * th);
			Vector4d K4(K1[0], K1[1] - r(i, j) * a * kh_x, K1[2] - r(i, j) * a * kh_y, e + p(i, j) - r(i, j) * a * th);
			K1 = K1 * J(i, j) * (gamma - 1) / gamma;
			K3 = K3 * J(i, j) / 2 / gamma;
			K4 = K4 * J(i, j) / 2 / gamma;
			Vector4d K_p = lambda1_p * K1 + lambda3_p * K3 + lambda4_p * K4;
			Vector4d K_n = lambda1_n * K1 + lambda3_n * K3 + lambda4_n * K4;
			for (int m = 0; m < 4; m++) {
				(*pos[m])(i, j) = K_p[m];
				(*neg[m])(i, j) = K_n[m];
			}

		}
	}
}

void space(MatrixXd* LQ[]) {
	MatrixXd* F_p[4], *F_n[4], *G_p[4], *G_n[4];
	MatrixXd F1_p(M + 1, N + 1);     F_p[0] = &F1_p;
	MatrixXd F2_p(M + 1, N + 1);     F_p[1] = &F2_p;
	MatrixXd F3_p(M + 1, N + 1);     F_p[2] = &F3_p;
	MatrixXd F4_p(M + 1, N + 1);     F_p[3] = &F4_p;
	MatrixXd G1_p(M + 1, N + 1);     G_p[0] = &G1_p;
	MatrixXd G2_p(M + 1, N + 1);     G_p[1] = &G2_p;
	MatrixXd G3_p(M + 1, N + 1);     G_p[2] = &G3_p;
	MatrixXd G4_p(M + 1, N + 1);     G_p[3] = &G4_p;
	MatrixXd F1_n(M + 1, N + 1);     F_n[0] = &F1_n;
	MatrixXd F2_n(M + 1, N + 1);     F_n[1] = &F2_n;
	MatrixXd F3_n(M + 1, N + 1);     F_n[2] = &F3_n;
	MatrixXd F4_n(M + 1, N + 1);     F_n[3] = &F4_n;
	MatrixXd G1_n(M + 1, N + 1);     G_n[0] = &G1_n;
	MatrixXd G2_n(M + 1, N + 1);     G_n[1] = &G2_n;
	MatrixXd G3_n(M + 1, N + 1);     G_n[2] = &G3_n;
	MatrixXd G4_n(M + 1, N + 1);     G_n[3] = &G4_n;
	split(F_p, F_n, 0);
	split(G_p, G_n, 1);



	for (int i = 1; i < M; i++) {
		for (int j = 1; j < N; j++) {
			for (int k = 0; k < 4; k++) {
				(*LQ[k])(i, j) = ((*F_p[k])(i, j) + (*F_n[k])(i + 1, j) - (*F_p[k])(i - 1, j) - (*F_n[k])(i, j) +
					(*G_p[k])(i, j) + (*G_n[k])(i, j + 1) - (*G_p[k])(i, j - 1) - (*G_n[k])(i, j))* -1;
			}
		}
	}
	for (int i = M3 + 1; i < M - M3; i++) {
		for (int k = 0; k < 4; k++) {
			(*LQ[k])(i, 0) = ((*F_p[k])(i, 0) + (*F_n[k])(i + 1, 0) - (*F_p[k])(i - 1, 0) - (*F_n[k])(i, 0) +
				2 * ((*G_p[k])(i, 0) + (*G_n[k])(i, 1) - (*G_p[k])(i, 0) - (*G_n[k])(i, 0))) * -1;

		}
	}

	/* for (int i = 1; i < M ; i++) {
	for (int k = 0; k < 4; k++) {
	(*LQ[k])(i, N) = ((*F_p[k])(i,N) + (*F_n[k])(i + 1, N) - (*F_p[k])(i - 1, N) - (*F_n[k])(i, N)+
	2 * ((*G_p[k])(i, N) + (*G_n[k])(i, N) - (*G_p[k])(i, N-1) - (*G_n[k])(i, N))) * -1;
	}
	}

	for (int j = 1; j < N; j++) {
	for (int k = 0; k < 4; k++) {
	(*LQ[k])(0, j) = ((*G_p[k])(0, j) + (*G_n[k])(0, j+1) - (*G_p[k])(0, j-1) - (*G_n[k])(0, j)+
	2*((*F_p[k])(0, j) + (*F_n[k])( 1, j) - (*F_p[k])(0, j) - (*F_n[k])(0, j)))* -1;
	(*LQ[k])(M, j) = ((*G_p[k])(M, j) + (*G_n[k])(M, j + 1) - (*G_p[k])(M, j - 1) - (*G_n[k])(M, j)+
	2 * ((*F_p[k])(M, j) + (*F_n[k])(M, j) - (*F_p[k])(M-1, j) - (*F_n[k])(M, j))) * -1;
	}
	}*/

	/*for (int i = 1; i <M3; i++) {
		for (int k = 0; k < 4; k++) {
			(*LQ[k])(i, 0) = -((*F_p[k])(i, 0) + (*F_n[k])(i + 1, 0) - (*F_p[k])(i - 1, 0) - (*F_n[k])(i, 0) +
				(*G_p[k])(i, 0) + (*G_n[k])(i, 1) - (*G_p[k])(M - i, 1) - (*G_n[k])(i, 0));
			(*LQ[k])(M - i, 0) = (*LQ[k])(i, 0);
		}
	}

	for (int k = 0; k < 4; k++) {
		(*LQ[k])(M3, 0) = -((*G_p[k])(M3, 0) + (*G_n[k])(M3, 1) - (*G_p[k])(M - M3, 1) - (*G_n[k])(M3, 0));

		(*LQ[k])(M - M3, 0) = (*LQ[k])(M3, 0);
	}*/

	/*for (int k = 0; k < 4; k++) {
	(*LQ[k])(0, 0) = ((*G_p[k])(0, 0) + (*G_n[k])(0, 1) - (*G_p[k])(M, 1) - (*G_n[k])(0, 0)) * -1;
	(*LQ[k])(0, N) = 0;
	(*LQ[k])(M, N) = 0;
	}*/
}


void time() {
	MatrixXd Q1 = J.array() * r.array();
	MatrixXd Q2 = J.array() * r.array() * u.array();
	MatrixXd Q3 = J.array() * r.array() * v.array();
	MatrixXd Q4 = J.array() * (p.array() / (gamma - 1) + 0.5*r.array()*(u.array()* u.array() + v.array()* v.array()));
	MatrixXd* LQ[4];
	MatrixXd LQ1 = MatrixXd::Zero(M + 1, N + 1); LQ[0] = &LQ1;
	MatrixXd LQ2 = MatrixXd::Zero(M + 1, N + 1); LQ[1] = &LQ2;
	MatrixXd LQ3 = MatrixXd::Zero(M + 1, N + 1); LQ[2] = &LQ3;
	MatrixXd LQ4 = MatrixXd::Zero(M + 1, N + 1); LQ[3] = &LQ4;

	space(LQ);
	
	for (int i = 0; i <= M; i++) {
		for (int j = 0; j <= N; j++) {
			T(i, j) = CFL / (L_xi(i, j) + L_eta(i, j));
		}
	}
	dt = T.minCoeff();
	cout << "dt = " << dt << endl;

	MatrixXd temp1 = Q1 + (*LQ[0]) * dt;
	MatrixXd temp2 = Q2 + (*LQ[1]) * dt;
	MatrixXd temp3 = Q3 + (*LQ[2]) * dt;
	MatrixXd temp4 = Q4 + (*LQ[3]) * dt;
	r = temp1.array() / J.array();
	u = temp2.array() / temp1.array();
	v = temp3.array() / temp1.array();
	p = (temp4.array() / J.array() - 0.5 * r.array() * (u.array() * u.array() + v.array() * v.array())) * (gamma - 1);


	boundary();
	space(LQ);
	temp1 = 0.75 * Q1 + (temp1 + (*LQ[0]) * dt) * 0.25;
	temp2 = 0.75 * Q2 + (temp2 + (*LQ[1]) * dt) * 0.25;
	temp3 = 0.75 * Q3 + (temp3 + (*LQ[2]) * dt)* 0.25;
	temp4 = 0.75 * Q4 + (temp4 + (*LQ[3]) * dt)* 0.25;
	r = temp1.array() / J.array();
	u = temp2.array() / temp1.array();
	v = temp3.array() / temp1.array();
	p = (temp4.array() / J.array() - 0.5 * r.array() * (u.array() * u.array() + v.array() * v.array())) * (gamma - 1);


	boundary();
	space(LQ);
	temp1 = 1.0 / 3.0 * Q1 + (temp1 + (*LQ[0]) * dt) * 2 / 3.0;
	temp2 = 1.0 / 3.0 * Q2 + (temp2 + (*LQ[1]) * dt) * 2 / 3.0;
	temp3 = 1.0 / 3.0 * Q3 + (temp3 + (*LQ[2]) * dt) * 2 / 3.0;
	temp4 = 1.0 / 3.0 * Q4 + (temp4 + (*LQ[3]) * dt) * 2 / 3.0;
	r = temp1.array() / J.array();
	u = temp2.array() / temp1.array();
	v = temp3.array() / temp1.array();
	p = (temp4.array() / J.array() - 0.5 * r.array() * (u.array() * u.array() + v.array() * v.array())) * (gamma - 1);
	boundary();

	/*   ofstream uout("./result/u.txt");
	uout << u;
	uout.close();
	ofstream vout("./result/v.txt");
	vout << v;
	vout.close();
	ofstream pout("./result/p.txt");
	pout << p;
	pout.close();
	ofstream rout("./result/r.txt");
	rout << r;
	rout.close();*/

}

void time_part() {
	MatrixXd Q1 = J.array() * r.array();
	MatrixXd Q2 = J.array() * r.array() * u.array();
	MatrixXd Q3 = J.array() * r.array() * v.array();
	MatrixXd Q4 = J.array() * (p.array() / (gamma - 1) + 0.5*r.array()*(u.array()* u.array() + v.array()* v.array()));
	MatrixXd* LQ[4];
	MatrixXd LQ1 = MatrixXd::Zero(M + 1, N + 1); LQ[0] = &LQ1;
	MatrixXd LQ2 = MatrixXd::Zero(M + 1, N + 1); LQ[1] = &LQ2;
	MatrixXd LQ3 = MatrixXd::Zero(M + 1, N + 1); LQ[2] = &LQ3;
	MatrixXd LQ4 = MatrixXd::Zero(M + 1, N + 1); LQ[3] = &LQ4;

	space(LQ);

	for (int i = 0; i <= M; i++) {
		for (int j = 0; j <= N; j++) {
			T(i, j) = CFL / (L_xi(i, j) + L_eta(i, j));
		}
	}
	dt = T.minCoeff();
	for (int i = 0; i <= M; i++) {
		for (int j = 0; j <= N; j++) {
			if (T(i, j) > 10.0 * dt)T(i, j) = 10.0 * dt;
		}
	}
	//cout << "dt = " << dt << endl;

	MatrixXd temp01 = (*LQ[0]).array() * T.array();
	MatrixXd temp02 = (*LQ[1]).array() * T.array();
	MatrixXd temp03 = (*LQ[2]).array() * T.array();
	MatrixXd temp04 = (*LQ[3]).array() * T.array();
	MatrixXd temp1 = Q1 + temp01;
	MatrixXd temp2 = Q2 + temp02;
	MatrixXd temp3 = Q3 + temp03;
	MatrixXd temp4 = Q4 + temp04;
	r = temp1.array() / J.array();
	u = temp2.array() / temp1.array();
	v = temp3.array() / temp1.array();
	p = (temp4.array() / J.array() - 0.5 * r.array() * (u.array() * u.array() + v.array() * v.array())) * (gamma - 1);
	//ofstream uout("./result/u.txt");
	//uout << u;
	//uout.close();
	//ofstream vout("./result/v.txt");
	//vout << v;
	//vout.close();
	//ofstream pout("./result/p.txt");
	//pout << p;
	//pout.close();
	//ofstream rout("./result/r.txt");
	//rout << r;
	//rout.close();

	boundary();
	space(LQ);
	temp01 = (*LQ[0]).array() * T.array();
	temp02 = (*LQ[1]).array() * T.array();
	temp03 = (*LQ[2]).array() * T.array();
	temp04 = (*LQ[3]).array() * T.array();
	temp1 = 0.75 * Q1 + (temp1 + temp01) * 0.25;
	temp2 = 0.75 * Q2 + (temp2 + temp02) * 0.25;
	temp3 = 0.75 * Q3 + (temp3 + temp03) * 0.25;
	temp4 = 0.75 * Q4 + (temp4 + temp04) * 0.25;
	r = temp1.array() / J.array();
	u = temp2.array() / temp1.array();
	v = temp3.array() / temp1.array();
	p = (temp4.array() / J.array() - 0.5 * r.array() * (u.array() * u.array() + v.array() * v.array())) * (gamma - 1);

	//uout << u;
	//uout.close();

	//vout << v;
	//vout.close();

	//pout << p;
	//pout.close();

	//rout << r;
	//rout.close();

	boundary();
	space(LQ);
	temp01 = (*LQ[0]).array() * T.array();
	temp02 = (*LQ[1]).array() * T.array();
	temp03 = (*LQ[2]).array() * T.array();
	temp04 = (*LQ[3]).array() * T.array();
	temp1 = 1.0 / 3.0 * Q1 + (temp1 + temp01) * 2 / 3.0;
	temp2 = 1.0 / 3.0 * Q2 + (temp2 + temp02) * 2 / 3.0;
	temp3 = 1.0 / 3.0 * Q3 + (temp3 + temp03) * 2 / 3.0;
	temp4 = 1.0 / 3.0 * Q4 + (temp4 + temp04) * 2 / 3.0;
	r = temp1.array() / J.array();
	u = temp2.array() / temp1.array();
	v = temp3.array() / temp1.array();
	p = (temp4.array() / J.array() - 0.5 * r.array() * (u.array() * u.array() + v.array() * v.array())) * (gamma - 1);
	boundary();


	//uout << u;
	//uout.close();

	//vout << v;
	//vout.close();
	//
	//pout << p;
	//pout.close();

	//rout << r;
	//rout.close();
}

void init_breakpoint(int index1) {
	char filename[30];
	sprintf_s(filename, "u%d.txt", index1);
	readPortfolio(u, filename);
	sprintf_s(filename, "v%d.txt", index1);
	readPortfolio(v, filename);
	sprintf_s(filename, "p%d.txt", index1);
	readPortfolio(p, filename);
	sprintf_s(filename, "r%d.txt", index1);
	readPortfolio(r, filename);

}

int main()
{
	readPortfolio(X, "X3.txt");
	readPortfolio(Y, "Y3.txt");

	Jacobi();
	deriviate();
	ofstream xout("X.txt");
	xout << X;
	xout.close();
	ofstream yout("Y.txt");
	yout << Y;
	yout.close();
	ofstream jout("Eta_y.txt");
	jout << Eta_y;
	jout.close();
	init();
	//boundary();
	//init_breakpoint(10000);
	boundary();
	//PSI.row(M) = PSI.row(0);
	ofstream uout("u.txt");
	uout << u;
	uout.close();
	ofstream vout("v.txt");
	vout << v;
	vout.close();
	ofstream pout("p.txt");
	pout << p;
	pout.close();
	ofstream rout("r.txt");
	rout << r;
	rout.close();
	char filename[30];
	err = 0.01;
	int aa = 0;
	int k = 1;

	MatrixXd r1(M + 1, N + 1); //rho
	MatrixXd u1(M + 1, N + 1); //u velocity
	MatrixXd v1(M + 1, N + 1); //v velocity
	MatrixXd p1(M + 1, N + 1); //pressure

	double error_u;
	double error_v;
	double error_r;
	double error_p;

	//for (int k = 1; k <= 25; k++)
	while (k <= 10000) {

		cout << "k" << k << endl;
		u1 = u;
		v1 = v;
		p1 = p;
		r1 = r;

		boundary();
		time_part();

		/*sprintf_s(filename, "T%d.txt", k);
		ofstream Tout(filename);
		Tout << T;
		Tout.close();
		sprintf_s(filename, "L_xi%d.txt", k);
		ofstream xxout(filename);
		xxout << L_xi;
		xxout.close();
		sprintf_s(filename, "L_eta%d.txt", k);
		ofstream eeout(filename);
		eeout << L_eta;
		eeout.close();*/
		
		error_u = (u - u1).norm();
		error_v = (v - v1).norm();
		error_r = (r - r1).norm();
		error_p = (p - p1).norm();
		cout << "error = " << error_u << " " << error_v << " " << error_r << " " << error_p << endl;

		k = k + 1;

		/*if (error_u < err&&error_v < err&&error_r < err&&error_p < err) {
			sprintf_s(filename, "p%d.txt", k);
			ofstream psiout(filename);
			psiout << p;
			psiout.close();
			sprintf_s(filename, "u%d.txt", k);
			ofstream uout(filename);
			uout << u;
			uout.close();
			sprintf_s(filename, "v%d.txt", k);
			ofstream vout(filename);
			vout << v;
			vout.close();
			sprintf_s(filename, "r%d.txt", k);
			ofstream rout(filename);
			rout << r;
			rout.close();

			aa = 1;
		}*/

		if (k % 20 == 0) {
			sprintf_s(filename, "p%d.txt", k);
			ofstream psiout(filename);
			psiout << p;
			psiout.close();
			sprintf_s(filename, "u%d.txt", k);
			ofstream uout(filename);
			uout << u;
			uout.close();
			sprintf_s(filename, "v%d.txt", k);
			ofstream vout(filename);
			vout << v;
			vout.close();
			sprintf_s(filename, "r%d.txt", k);
			ofstream rout(filename);
			rout << r;
			rout.close();
		}
	}

	std::cout << "Hello World!\n";
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件

/*ofstream x1out("x_xi.txt");
x1out << X_xi;
x1out.close();
ofstream x2out("x_eta.txt");
x2out << X_eta;
x2out.close();
ofstream x3out("x_xixi.txt");
x3out << X_xixi;
x3out.close();
ofstream x4out("x_etaeta.txt");
x4out << X_etaeta;
x4out.close();
ofstream x5out("x_xieta.txt");
x5out << X_xieta;
x5out.close();
ofstream y1out("y_xi.txt");
y1out << X_xi;
y1out.close();
ofstream y2out("y_eta.txt");
y2out << X_eta;
y2out.close();
ofstream y3out("y_xixi.txt");
y3out << X_xixi;
y3out.close();
ofstream y4out("y_etaeta.txt");
y4out << X_etaeta;
y4out.close();
ofstream y5out("y_xieta.txt");
y5out << X_xieta;
y5out.close();*/
/*ofstream X1out("Xi_x.txt");
X1out << Xi_x;
X1out.close();
ofstream X2out("Eta_x.txt");
X2out << Eta_x;
X2out.close();
ofstream X3out("Xi_xx.txt");
X3out << Xi_xx;
X3out.close();
ofstream X4out("Eta_xx.txt");
X4out << Eta_xx;
X4out.close();
ofstream X5out("Xi_xy.txt");
X5out << Xi_xy;
X5out.close();
ofstream Y1out("Xi_y.txt");
Y1out << Xi_y;
Y1out.close();
ofstream Y2out("Eta_y.txt");
Y2out << Eta_y;
Y2out.close();
ofstream Y3out("Xi_yy.txt");
Y3out << Xi_yy;
Y3out.close();
ofstream Y4out("Eta_yy.txt");
Y4out << Eta_yy;
Y4out.close();
ofstream Y5out("Eta_xy.txt");
Y5out << Eta_xy;
Y5out.close();*/
//init();
//int index = 0;