# Install script for directory: C:/CFD1/CFD/eigen-3.3.7/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/CFD1/CFD/eigen-3.3.7/out/install/x64-Debug")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE FILE FILES
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/Cholesky"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/CholmodSupport"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/Core"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/Dense"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/Eigen"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/Eigenvalues"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/Geometry"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/Householder"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/IterativeLinearSolvers"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/Jacobi"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/LU"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/MetisSupport"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/OrderingMethods"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/PaStiXSupport"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/PardisoSupport"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/QR"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/QtAlignedMalloc"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/SPQRSupport"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/SVD"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/Sparse"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/SparseCholesky"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/SparseCore"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/SparseLU"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/SparseQR"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/StdDeque"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/StdList"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/StdVector"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/SuperLUSupport"
    "C:/CFD1/CFD/eigen-3.3.7/Eigen/UmfPackSupport"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE DIRECTORY FILES "C:/CFD1/CFD/eigen-3.3.7/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

