clear all
X=load("X.txt");
Y=load("Y.txt");
U=load("U120.txt");
V=load("V120.txt");
PSI=load("PSI120.txt");
W=load("W120.txt");
M=101;
N=101;

figure(1)
quiver(X,Y,U,V,0.1);
axis equal
hold on
r=1; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')
r=20; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')

figure(2)
hold on
r=1; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')
r=20; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')
contour(X,Y,W,100);
axis equal

figure(3)
contour(X,Y,PSI,100);
axis equal;
hold on
r=1; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')
r=20; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')
%streamline(X,Y,U,V);
% for i=1:M
%     plot(X(i,:),Y(i,:),'-b');
% end
% 
% for j=2:N-1
%     plot(X(:,j),Y(:,j),'-b');
% end

%  figure(2)
%  quiver(X,Y,U,V)
% 
% startx = -20:0.5:20;
% starty = zeros(size(startx));
% streamline(X,Y,U,V,startx,starty)
% axis equal