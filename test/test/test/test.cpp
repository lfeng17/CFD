﻿// test.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;
#include <fstream>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;

#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>
#include <stdio.h>
#define M 140
#define N 80
MatrixXd X(M + 1, N + 1);
MatrixXd Y(M + 1, N + 1);

void readPortfolio(MatrixXd& matrix, string b)  //this will be a function that should hopefully return a matrix to use in other functions.
{
    ifstream data(b);
    string lineOfData;

    if (data.is_open())
    {
        int i = 0;
        while (data.good())
        {
            char linebuff[4096];
            getline(data, lineOfData);
            strncpy_s(linebuff, lineOfData.c_str(), sizeof(linebuff) - 1);

            //cout << lineOfData << endl; //just to check if the data was read.
            {
                int j = 0;
                double val;
                char* p_r = NULL, * p_val;
                p_val = strtok_s(linebuff, " ,;", &p_r);
                while (NULL != p_val) {
                    //?wrong??  sscanf_s("%lf", p_val, &val);
                    val = atof(p_val);
                    //cout << "  " << val;
                    matrix(i, j) = val;
                    j++;
                    p_val = strtok_s(NULL, " ,;", &p_r);
                }
                //cout << endl;
            }
            i++;
        }
    }
    else { std::cout << "Unable to open file"; }

}

int main()
{
    readPortfolio(X, "X3.txt");
    readPortfolio(Y, "Y3.txt");
    ofstream xout("X.txt");
    xout << X;
    xout.close();
    ofstream yout("Y.txt");
    yout << Y;
    yout.close();
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
