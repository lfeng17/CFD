#include <fstream>
#include <iostream>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

#define _USE_MATH_DEFINES
#include <math.h>

#define _USE_MATH_DEFINES

#define M_PI 3.1415926535897932384626

double d1 = 1; //diameter of inner circle
double d2 = 15; //diameter of outter circle
#define M  40 //number of mesh along x
#define N  40 //number of mesh along y

MatrixXd X(M + 1, N + 1); //x coordinate
MatrixXd Y(M + 1, N + 1); //y coordinate

double dXi = 0.1; //delta Xi
double dEta = 0.1; //delta Eta
double epslon = 0.001; //error for iteration

//initial P,Q
MatrixXd  P = MatrixXd::Zero(M + 1, N + 1);
MatrixXd  Q = MatrixXd::Zero(M + 1, N + 1);

void init_PQ(MatrixXd& p, MatrixXd& q) {
	for (int i = 0; i <= M; i++) {
		p(i, 5) = 10;
		q(i, 5) = 10;
		//p(i, 11) = 10;
		//  q(i, 11) = 0;
	}
}

void init_PQ_orth(MatrixXd& x, MatrixXd& y, MatrixXd& p, MatrixXd& q) {
	p = MatrixXd::Zero(M + 1, N + 1);
	q = MatrixXd::Zero(M + 1, N + 1);

	for (int i = 1; i < M; i++) {
		p(i, 0) = -((x(i + 1, 0) - x(i - 1, 0)) / (2 * dXi)*(x(i + 1, 0) - 2 * x(i, 0) + x(i - 1, 0)) / pow(dXi, 2) + (y(i + 1, 0) - y(i - 1, 0)) / (2 * dXi)*(y(i + 1, 0) - 2 * y(i, 0) + y(i - 1, 0)) / pow(dXi, 2)) / (pow((x(i + 1, 0) - x(i - 1, 0)) / (2 * dXi), 2) + pow((y(i + 1, 0) - y(i - 1, 0)) / (2 * dXi), 2));
		p(i, N) = -((x(i + 1, N) - x(i - 1, N)) / (2 * dXi)*(x(i + 1, N) - 2 * x(i, N) + x(i - 1, N)) / pow(dXi, 2) + (y(i + 1, N) - y(i - 1, N)) / (2 * dXi)*(y(i + 1, N) - 2 * y(i, N) + y(i - 1, N)) / pow(dXi, 2)) / (pow((x(i + 1, N) - x(i - 1, N)) / (2 * dXi), 2) + pow((y(i + 1, N) - y(i - 1, N)) / (2 * dXi), 2));
	}

	for (int j = 1; j < N; j++) {
		q(0, j) = -((x(0, j + 1) - x(0, j - 1)) / (2 * dEta)*(x(0, j + 1) - 2 * x(0, j) + x(0, j - 1)) / pow(dEta, 2) + (y(0, j + 1) - y(0, j - 1)) / (2 * dEta)*(y(0, j + 1) - 2 * y(0, j) + y(0, j - 1)) / pow(dEta, 2)) / (pow((x(0, j + 1) - x(0, j - 1)) / (2 * dEta), 2) + pow((y(0, j + 1) - y(0, j - 1)) / (2 * dEta), 2));
		q(M, j) = -((x(M, j + 1) - x(M, j - 1)) / (2 * dEta)*(x(M, j + 1) - 2 * x(M, j) + x(M, j - 1)) / pow(dEta, 2) + (y(M, j + 1) - y(M, j - 1)) / (2 * dEta)*(y(M, j + 1) - 2 * y(M, j) + y(M, j - 1)) / pow(dEta, 2)) / (pow((x(M, j + 1) - x(M, j - 1)) / (2 * dEta), 2) + pow((y(M, j + 1) - y(M, j - 1)) / (2 * dEta), 2));
	}

	for (int i = 1; i < M; i++) {
		for (int j = 1; j < N; j++) {
			p(i, j) = p(i, 0) + float(j) / float(N)*(p(i, N) - p(i, 0));
			q(i, j) = q(0, j) + float(i) / float(M)*(q(M, j) - q(0, j));
		}
	}

}

//initial boundary points
void init_circle(MatrixXd& x, MatrixXd& y) {
	x = MatrixXd::Random(M + 1, N + 1);
	y = MatrixXd::Random(M + 1, N + 1);
	for (int i = 0; i <= M; i++) {
		//for (int j = 0; j <= N; j++) {
		//x(i, j) = (float(j) / N * (d2 - d1) / 2 + d1 / 2) * cos(float(i) / float(M) * 2.0 * M_PI);
		//x(i, j) = d2 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
		//y(i, j) = (float(j) / N * (d2 - d1) / 2 + d1 / 2) * sin(float(i) / float(M) * 2.0 * M_PI);
		//y(i, N) = d2 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
		//}
		x(i, 0) = d1 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
		x(i, N) = d2 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
		y(i, 0) = d1 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
		y(i, N) = d2 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
	}
	
	for (int j = 1; j <= N - 1; j++) {
		x(M, j) = x(0, j);
		y(0, j) = 0;
		y(M, j) = 0;
	}

	for (int i = 1; i < M; i++) {
		for (int j = 1; j < N; j++) {
			double theta = float(i) / float(M) * 2.0 * M_PI;
			double r = float(j) / float(N) * (d2 - d1) / 2 + d1 / 2;
			x(i, j) = r * cos(theta);
			y(i, j) = r * sin(theta);
		}
	}
}

void init_airfoil(MatrixXd& x, MatrixXd& y) {
	x = MatrixXd::Random(M + 1, N + 1);
	y = MatrixXd::Random(M + 1, N + 1);
	for (int i = 0; i <= M; i++) {
		//double theta = (cos(float(i) / float(M) * 2.0 * M_PI)+1) * M_PI / 2 * (M / 2 - i > 0 ? 1 : -1);
		double theta = float(i) / float(M) * 2.0 * M_PI;
		double temp = 0.5 * cos(theta) + 0.5;
		x(i, 0) = temp - 0.5;
		y(i, 0) = 0.594689181 * (0.298222773 * sqrt(temp) - 0.127125232 * temp - 0.357907906 * pow(temp, 2) + 0.291984971 * pow(temp, 3) - 0.105174606 * pow(temp, 4))* (M / 2 - i>0 ? 1 : -1);
		x(i, N) = d2 / 2 * cos(theta);
		y(i, N) = d2 / 2 * sin(theta);
		//cout << y(i, 0) <<"  *****  "<< x(i,N)<< endl;
	}

	for (int j = 1; j <= N - 1; j++) {
		x(M, j) = x(0, j);
		y(0, j) = 0;
		y(M, j) = 0;
	}

	for (int i = 1; i < M; i++) {
		for (int j = 1; j < N; j++) {
			double theta = float(i) / float(M) * 2.0 * M_PI;
			double r = float(j) / float(N) * (d2 - d1) / 2 + d1 / 2;
			x(i, j) = r * cos(theta);
			y(i, j) = r * sin(theta);
		}
	}
}

void init_circle_sta(MatrixXd& x, MatrixXd& y) {
	x = MatrixXd::Zero(M + 1, N + 1);
	y = MatrixXd::Zero(M + 1, N + 1);
	for (int i = 0; i <= M; i++) {
		//for (int j = 0; j <= N; j++) {
		//x(i, j) = (float(j) / N * (d2 - d1) / 2 + d1 / 2) * cos(float(i) / float(M) * 2.0 * M_PI);
		//x(i, j) = d2 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
		//y(i, j) = (float(j) / N * (d2 - d1) / 2 + d1 / 2) * sin(float(i) / float(M) * 2.0 * M_PI);
		//y(i, N) = d2 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
		//}
		x(i, 0) = d1 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
		x(i, N) = d2 / 2 * cos(float(i) / float(M) * 2.0 * M_PI);
		y(i, 0) = d1 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
		y(i, N) = d2 / 2 * sin(float(i) / float(M) * 2.0 * M_PI);
	}

	for (int j = 1; j <= N - 1; j++) {
		double theta = float(j) / float(N) * M_PI;
		x(M, j) = d1 / 2 + (d2 - d1) / 2 * (0.5 - 0.5*cos(theta));
		x(0, j) = d1 / 2 + (d2 - d1) / 2 * (0.5 - 0.5*cos(theta));
		y(0, j) = 0;
		y(M, j) = 0;
	}

	for (int i = 1; i < M; i++) {
		for (int j = 1; j < N; j++) {
			double r = d1 / 2 + (d2 - d1)*float(j) / float(N);
			double theta = float(i) / float(M) * 2.0 * M_PI;
			x(i, j) = r * cos(theta);
			y(i, j) = r * sin(theta);
		}
	}
}

void init_airfoil_sta(MatrixXd& x, MatrixXd& y) {

	x = MatrixXd::Zero(M + 1, N + 1);
	y = MatrixXd::Zero(M + 1, N + 1);
	for (int i = 0; i <= M; i++) {
		//double theta = (cos(float(i) / float(M) * 2.0 * M_PI)+1) * M_PI / 2 * (M / 2 - i > 0 ? 1 : -1);
		double theta = float(i) / float(M) * 2.0 * M_PI;
		double temp = 0.5 * cos(theta) + 0.5;
		x(i, 0) = temp - 0.5;
		y(i, 0) = 0.594689181 * (0.298222773 * sqrt(temp) - 0.127125232 * temp - 0.357907906 * pow(temp, 2) + 0.291984971 * pow(temp, 3) - 0.105174606 * pow(temp, 4)) * (M / 2 - i > 0 ? 1 : -1);
		x(i, N) = d2 / 2 * cos(theta);
		y(i, N) = d2 / 2 * sin(theta);
		//cout << y(i, 0) <<"  *****  "<< x(i,N)<< endl;
	}

	for (int j = 1; j <= N - 1; j++) {
		x(M, j) = d1 / 2 + (d2 - d1) / 2 * pow(float(j) / float(N), 1.3);
		x(0, j) = d1 / 2 + (d2 - d1) / 2 * pow(float(j) / float(N), 1.3);
		y(0, j) = 0;
		y(M, j) = 0;
	}
	for (int i = 1; i < M; i++) {
		for (int j = 1; j < N; j++) {
			double theta = float(i) / float(M) * 2.0 * M_PI;
			double r = float(j) / float(N) * (d2 - d1) / 2 + d1 / 2;
			x(i, j) = r * cos(theta);
			y(i, j) = r * sin(theta);
		}
	}
}

void iteration_J(MatrixXd& x, MatrixXd& y, MatrixXd& p, MatrixXd& q) {
	MatrixXd xnew(M + 1, N + 1);
	MatrixXd ynew(M + 1, N + 1);
	double alpha = 0;
	double beta = 0;
	double gamma = 0;
	double b_w = 0, b_e = 0, b_s = 0, b_n = 0, b_p = 0;
	double c_px = 0, c_py = 0;
	xnew = x;
	ynew = y;
	double errorx = 10;
	double errory = 10;
	static int count = 0;
	ofstream aout("alpha.txt");
	while (true)
	{
		for (int i = 0; i < M; i++) {
			int index = i> 0 ? i - 1 : M - 1;
			for (int j = 1; j < N; j++) {
				alpha = pow((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta, 2) + pow((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta, 2);
				beta = ((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta) * ((x(i + 1, j) - x(index, j)) / 2.0 / dXi) + ((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta) * ((y(i + 1, j) - y(index, j)) / 2.0 / dXi);
				gamma = pow((x(i + 1, j) - x(index, j)) / 2.0 / dXi, 2) + pow((y(i + 1, j) - y(index, j)) / 2.0 / dXi, 2);
				b_w = alpha / pow(dXi, 2);
				//b_e = b_w;
				b_s = gamma / pow(dEta, 2);
				//b_n = b_s;
				//b_p = b_w + b_e + b_s + b_n;
				b_p = 2 * b_w + 2 * b_s - alpha * p(i, j) / dXi - beta * q(i, j) / dEta;;
				c_px = -beta * (x(i + 1, j + 1) - x(i + 1, j - 1) - x(index, j + 1) + x(index, j - 1)) / 2.0 / dXi / dEta;
				c_py = -beta * (y(i + 1, j + 1) - y(i + 1, j - 1) - y(index, j + 1) + y(index, j - 1)) / 2.0 / dXi / dEta;
				xnew(i, j) = ((b_w - alpha * p(i, j)) * x(index, j) + b_w * x(i + 1, j) + (b_s - gamma * q(i, j)) * x(i, j - 1) + b_s * x(i, j + 1) + c_px) / b_p;
				ynew(i, j) = ((b_w - alpha * p(i, j)) * y(index, j) + b_w * y(i + 1, j) + (b_s - gamma * q(i, j)) * y(i, j - 1) + b_s * y(i, j + 1) + c_py) / b_p;
				xnew(M, j) = xnew(0, j);
				ynew(0, j) = 0;
				aout << "i:" << i << endl << "j:" << j << endl << b_p << endl;
			}
		}
		double errorx = (x - xnew).norm();
		double errory = (y - ynew).norm();
		x = xnew;
		y = ynew;
		aout << "xnew:\n" << xnew << endl;
		aout << "ynew:\n" << ynew << endl;
		aout << "errorx:" << errorx << endl;
		aout << "errory:" << errory << endl;
		aout << "count: " << count << endl;
		cout << "errorx:" << errorx << endl;
		cout << "errory:" << errory << endl;
		count++;
		if ((errorx < epslon) && (errory < epslon)) {
			cout << "count: " << count << endl;
			aout.close();
			return;
		}
	}

}

void iteration_GS(MatrixXd& x, MatrixXd& y, MatrixXd p, MatrixXd q) {
	MatrixXd xnew(M + 1, N + 1);
	MatrixXd ynew(M + 1, N + 1);
	double alpha = 0;
	double beta = 0;
	double gamma = 0;
	double b_w = 0, b_e = 0, b_s = 0, b_n = 0, b_p = 0;
	double c_px = 0, c_py = 0;
	xnew = x;
	ynew = y;
	double errorx = 10;
	double errory = 10;
	static int count = 0;
	ofstream aout("alpha.txt");
	
	while (true)
	{
		for (int i = 0; i < M; i++) {
			int index = i > 0 ? i - 1 : M - 1;
			for (int j = 1; j < N; j++) {
				alpha = pow((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta, 2) + pow((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta, 2);
				beta = ((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta) * ((x(i + 1, j) - x(index, j)) / 2.0 / dXi) + ((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta) * ((y(i + 1, j) - y(index, j)) / 2.0 / dXi);
				gamma = pow((x(i + 1, j) - x(index, j)) / 2.0 / dXi, 2) + pow((y(i + 1, j) - y(index, j)) / 2.0 / dXi, 2);
				b_w = alpha / pow(dXi, 2);
				//b_e = b_w;
				b_s = gamma / pow(dEta, 2);
				//b_n = b_s;
				//b_p = b_w + b_e + b_s + b_n;
				b_p = 2 * b_w + 2 * b_s - alpha * p(i, j) / dXi - beta * q(i, j) / dEta;
				c_px = -beta * (x(i + 1, j + 1) - x(i + 1, j - 1) - x(index, j + 1) + x(index, j - 1)) / 2.0 / dXi / dEta;
				c_py = -beta * (y(i + 1, j + 1) - y(i + 1, j - 1) - y(index, j + 1) + y(index, j - 1)) / 2.0 / dXi / dEta;
				xnew(i, j) = ((b_w - alpha * p(i, j)) * xnew(index, j) + b_w * x(i + 1, j) + (b_s - gamma * q(i, j)) * xnew(i, j - 1) + b_s * x(i, j + 1) + c_px) / b_p;
				ynew(i, j) = ((b_w - alpha * p(i, j)) * ynew(index, j) + b_w * y(i + 1, j) + (b_s - gamma * q(i, j)) * ynew(i, j - 1) + b_s * y(i, j + 1) + c_py) / b_p;
				xnew(M, j) = xnew(0, j);
				ynew(0, j) = 0;
				aout << "i:" << i << endl << "j:" << j << endl << b_p << endl;
			}
		}
		double errorx = (x - xnew).norm();
		double errory = (y - ynew).norm();
		x = xnew;
		y = ynew;
		aout << "xnew:\n" << xnew << endl;
		aout << "ynew:\n" << ynew << endl;
		aout << "errorx:" << errorx << endl;
		aout << "errory:" << errory << endl;
		aout << "count: " << count << endl;
		cout << "errorx:" << errorx << endl;
		cout << "errory:" << errory << endl;
		count++;
		if ((errorx < epslon) && (errory < epslon)) {
			cout << "count: " << count << endl;
			aout.close();
			return;
		}
	}

}

void iteration_relaxation(MatrixXd& x, MatrixXd& y, double a, MatrixXd p, MatrixXd q) {
	MatrixXd xnew(M + 1, N + 1);
	MatrixXd ynew(M + 1, N + 1);
	double alpha = 0;
	double beta = 0;
	double gamma = 0;
	double b_w = 0, b_e = 0, b_s = 0, b_n = 0, b_p = 0;
	double c_px = 0, c_py = 0;
	xnew = x;
	ynew = y;
	double errorx = 10;
	double errory = 10;
	static int count = 0;
	ofstream aout("alpha.txt");
	
	while (true)
	{
		for (int i = 0; i < M; i++) {
			int index = (i > 0 ? i - 1 : M - 1);
			for (int j = 1; j < N; j++) {
				alpha = pow((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta, 2) + pow((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta, 2);
				beta = ((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta) * ((x(i + 1, j) - x(index, j)) / 2.0 / dXi) + ((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta) * ((y(i + 1, j) - y(index, j)) / 2.0 / dXi);
				gamma = pow((x(i + 1, j) - x(index, j)) / 2.0 / dXi, 2) + pow((y(i + 1, j) - y(index, j)) / 2.0 / dXi, 2);
				b_w = alpha / pow(dXi, 2);
				//b_e = b_w;
				b_s = gamma / pow(dEta, 2);
				//b_n = b_s;
				//b_p = b_w + b_e + b_s + b_n;
				b_p = 2 * b_w + 2 * b_s - alpha * p(i, j) / dXi - beta * q(i, j) / dEta;
				c_px = -beta * (x(i + 1, j + 1) - x(i + 1, j - 1) - x(index, j + 1) + x(index, j - 1)) / 2.0 / dXi / dEta;
				c_py = -beta * (y(i + 1, j + 1) - y(i + 1, j - 1) - y(index, j + 1) + y(index, j - 1)) / 2.0 / dXi / dEta;
				xnew(i, j) = ((b_w - alpha * p(i, j)) * xnew(index, j) + b_w * x(i + 1, j) + (b_s - gamma * q(i, j)) * xnew(i, j - 1) + b_s * x(i, j + 1) + c_px) / b_p;
				ynew(i, j) = ((b_w - alpha * p(i, j)) * ynew(index, j) + b_w * y(i + 1, j) + (b_s - gamma * q(i, j)) * ynew(i, j - 1) + b_s * y(i, j + 1) + c_py) / b_p;
				xnew(M, j) = xnew(0, j);
				ynew(0, j) = 0;
				aout << "i:" << i << endl << "j:" << j << endl << b_p << endl;
			}
		}
		double errorx = (x - xnew).norm();
		double errory = (y - ynew).norm();
		x = a * xnew + (1 - a) * x;
		y = a * ynew + (1 - a) * y;
		aout << "xnew:\n" << xnew << endl;
		aout << "ynew:\n" << ynew << endl;
		aout << "errorx:" << errorx << endl;
		aout << "errory:" << errory << endl;
		aout << "a:" << a << endl;
		aout << "count: " << count << endl;
		cout << "errorx:" << errorx << endl;
		cout << "errory:" << errory << endl;
		count++;
		if ((errorx < epslon) && (errory < epslon)) {
			cout << "a:" << a << endl;
			cout << "count: " << count << endl;
			aout.close();
			return;
		}
	}

}

void iteration_relaxation_sta(MatrixXd& x, MatrixXd& y, double a, MatrixXd& p, MatrixXd& q) {
	MatrixXd xnew(M + 1, N + 1);
	MatrixXd ynew(M + 1, N + 1);
	double alpha = 0;
	double beta = 0;
	double gamma = 0;
	double b_w = 0, b_e = 0, b_s = 0, b_n = 0, b_p = 0;
	double c_px = 0, c_py = 0;
	xnew = x;
	ynew = y;
	double errorx = 10;
	double errory = 10;
	static int count = 0;
	ofstream aout("alpha.txt");
	while (true)
	{
		for (int i = 1; i < M; i++) {
			for (int j = 1; j < N; j++) {
				alpha = pow((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta, 2) + pow((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta, 2);
				beta = ((x(i, j + 1) - x(i, j - 1)) / 2.0 / dEta) * ((x(i + 1, j) - x(i - 1, j)) / 2.0 / dXi) + ((y(i, j + 1) - y(i, j - 1)) / 2.0 / dEta) * ((y(i + 1, j) - y(i - 1, j)) / 2.0 / dXi);
				gamma = pow((x(i + 1, j) - x(i - 1, j)) / 2.0 / dXi, 2) + pow((y(i + 1, j) - y(i - 1, j)) / 2.0 / dXi, 2);
				b_w = alpha / pow(dXi, 2);
				//b_e = b_w;
				b_s = gamma / pow(dEta, 2);
				//b_n = b_s;
				//b_p = b_w + b_e + b_s + b_n;
				b_p = 2 * b_w + 2 * b_s;
				c_px = -beta * (x(i + 1, j + 1) - x(i + 1, j - 1) - x(i - 1, j + 1) + x(i - 1, j - 1)) / 2.0 / dXi / dEta + alpha * p(i, j)*(x(i + 1, j) - x(i - 1, j)) / 2 / dXi + gamma * q(i, j)*(x(i, j + 1) - x(i, j - 1)) / 2 / dEta;
				c_py = -beta * (y(i + 1, j + 1) - y(i + 1, j - 1) - y(i - 1, j + 1) + y(i - 1, j - 1)) / 2.0 / dXi / dEta + alpha * p(i, j)*(y(i + 1, j) - y(i - 1, j)) / 2 / dXi + gamma * q(i, j)*(y(i, j + 1) - y(i, j - 1)) / 2 / dEta;
				xnew(i, j) = (b_w * xnew(i - 1, j) + b_w * x(i + 1, j) + b_s * xnew(i, j - 1) + b_s * x(i, j + 1) + c_px) / b_p;
				ynew(i, j) = (b_w * ynew(i - 1, j) + b_w * y(i + 1, j) + b_s * ynew(i, j - 1) + b_s * y(i, j + 1) + c_py) / b_p;
				aout << "i:" << i << endl << "j:" << j << endl << b_p << endl;
			}
		}
		double errorx = (x - xnew).norm();
		double errory = (y - ynew).norm();
		x = a * xnew + (1 - a) * x;
		y = a * ynew + (1 - a) * y;
		aout << "xnew:\n" << xnew << endl;
		aout << "ynew:\n" << ynew << endl;
		aout << "errorx:" << errorx << endl;
		aout << "errory:" << errory << endl;
		aout << "a:" << a << endl;
		aout << "count: " << count << endl;
		cout << "errorx:" << errorx << endl;
		cout << "errory:" << errory << endl;
		count++;
		if ((errorx < epslon) && (errory < epslon)) {
			cout << "a:" << a << endl;
			cout << "count: " << count << endl;
			aout.close();
			return;
		}
	}

}

int main()
{
	init_airfoil_sta(X, Y);
	//init_circle(X, Y);
	//init_PQ(P, Q);
	//init_PQ_orth(X, Y, P, Q);
	iteration_relaxation_sta(X, Y, 1.5, P, Q);
	//iteration_J(X, Y, P, Q);
	ofstream Xout("X.txt");
	Xout << X;
	Xout.close();
	ofstream Yout("Y.txt");
	Yout << Y;
	Yout.close();
	std::cout << X.rows() << "\n";
	std::cout << X.cols() << "\n";
	cout << X << endl << Y << endl;

	return 0;
}

