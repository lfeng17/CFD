# CMake generated Testfile for 
# Source directory: C:/CFD1/CFD/eigen-3.3.7
# Build directory: C:/CFD1/CFD/eigen-3.3.7/out/build/x64-Debug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Eigen")
subdirs("doc")
subdirs("test")
subdirs("blas")
subdirs("lapack")
subdirs("unsupported")
subdirs("demos")
subdirs("scripts")
