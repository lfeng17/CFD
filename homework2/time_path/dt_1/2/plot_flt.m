clear all
X=load("X.txt");
Y=load("Y.txt");
U=load("u120.txt");
V=load("v120.txt");
P=load("p120.txt");
R=load("r120.txt");
% PSI=load("PSI950.txt");
% W=load("W950.txt");

UU=(U.^2+V.^2).^0.5;
a=(1.4*P./R).^0.5;
Ma=UU./a;


M=101;
N=101;
figure(1)
quiver(X,Y,U,V,1);
axis equal
hold on
theta=0:pi/100:2*pi;
temp = 0.5*cos(theta)+0.5;
y= 0.594689181 .* (0.298222773 .* temp.^0.5 - 0.127125232 .* temp - 0.357907906 .* temp.^2 + 0.291984971 .* temp.^3 - 0.105174606 .* temp.^4) .* sign(pi-theta);
x=0.5*cos(theta)-2; 
plot(x,y,'-r')

figure(2)
contour(X,Y,P,100);
axis equal
hold on
theta=0:pi/100:2*pi;
temp = 0.5*cos(theta)+0.5;
y= 0.594689181 .* (0.298222773 .* temp.^0.5 - 0.127125232 .* temp - 0.357907906 .* temp.^2 + 0.291984971 .* temp.^3 - 0.105174606 .* temp.^4) .* sign(pi-theta);
x=0.5*cos(theta)-2; 
plot(x,y,'-r')

figure(3)
contour(X,Y,R,100);
axis equal
hold on
theta=0:pi/100:2*pi;
temp = 0.5*cos(theta)+0.5;
y= 0.594689181 .* (0.298222773 .* temp.^0.5 - 0.127125232 .* temp - 0.357907906 .* temp.^2 + 0.291984971 .* temp.^3 - 0.105174606 .* temp.^4) .* sign(pi-theta);
x=0.5*cos(theta)-2; 
plot(x,y,'-r')

figure(4)
%contourf(X,Y,P,100);
surface(X,Y,P);
shading interp
axis equal

figure(5)
surface(X,Y,UU);
shading interp
axis equal

figure(6)
%contourf(X,Y,Ma,100);
surface(X,Y,Ma);
shading interp
axis equal



%streamline(X,Y,U,V);
% for i=1:M
%     plot(X(i,:),Y(i,:),'-b');
% end
% 
% for j=2:N-1
%     plot(X(:,j),Y(:,j),'-b');
% end

%  figure(2)
%  quiver(X,Y,U,V)
% 
% startx = -20:0.5:20;
% starty = zeros(size(startx));
% streamline(X,Y,U,V,startx,starty)
% axis equal