clear all
close all
num=5;
t=num*0.01;
X=load("X.txt");
Y=load("Y.txt");
U=load(['U',num2str(num),'.data']);
V=load(['V',num2str(num),'.data']);
PSI=load(['PSI',num2str(num),'.data']);
% PSI=load("PSI.data");
W=load(['W',num2str(num),'.data']);

figure(1)
axis equal
hold on
quiver(X,Y,U,V);
% velocity=sqrt(U.^2+V.^2);
% contour(X,Y,velocity,10);
axis([-3,0,-1,1]);
title(['速度矢量图','t=',num2str(t)]);
% hold on
figure(2)
hold on
axis equal
contour(X,Y,W,100);
axis([-3,0,-1,1]);
title(['涡量云图',' t=',num2str(t)]);
% 
figure(3)
hold on
axis equal
contour(X,Y,PSI,50);
axis([-3,0,-1,1]);
title(['流线图','t=',num2str(t)]);

%streamline(X,Y,U,V);
% for i=1:M
%     plot(X(i,:),Y(i,:),'-b');
% end
% 
% for j=2:N-1
%     plot(X(:,j),Y(:,j),'-b');
% end

%  figure(2)
%  quiver(X,Y,U,V)
% 
% startx = -20:0.5:20;
% starty = zeros(size(startx));
% streamline(X,Y,U,V,startx,starty)
% axis equal