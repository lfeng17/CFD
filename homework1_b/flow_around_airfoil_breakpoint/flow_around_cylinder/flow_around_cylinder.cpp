﻿// flow_around_cylinder.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
using namespace std;
#include <fstream>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;

#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>
#include <stdio.h>
#define random(x) (rand()%x)
double d1 = 2; //diameter of inner circle
double d2 = 40; //diameter of outter circle
#define M  180 //number of mesh along x
#define M1 50
#define M2 40
#define M3 30
#define N  120 //number of mesh along y
double Re = 200;
double dt = 0.001;
double err;
double u0 = 1;

MatrixXd X(M + 1, N + 1); //x coordinate
MatrixXd Y(M + 1, N + 1); //y coordinate
MatrixXd X_eta(M + 1, N + 1); //first order
MatrixXd X_xi(M + 1, N + 1); //first order
MatrixXd Y_eta(M + 1, N + 1); //first order
MatrixXd Y_xi(M + 1, N + 1); //first order
MatrixXd X_etaeta(M + 1, N + 1); //second order,eta xi same
MatrixXd X_xixi(M + 1, N + 1); //second order,eta xi same
MatrixXd X_xieta(M + 1, N + 1); //second order,eta xi same
MatrixXd Y_etaeta(M + 1, N + 1); //second order,eta xi same
MatrixXd Y_xixi(M + 1, N + 1); //second order,eta xi same
MatrixXd Y_xieta(M + 1, N + 1); //second order,eta xi same
MatrixXd J(M + 1, N + 1);
MatrixXd Eta_x(M + 1, N + 1); //first order
MatrixXd Xi_x(M + 1, N + 1); //first order
MatrixXd Eta_y(M + 1, N + 1); //first order
MatrixXd Xi_y(M + 1, N + 1); //first order
MatrixXd Eta_xx(M + 1, N + 1); //second order,eta xi same
MatrixXd Xi_xx(M + 1, N + 1); //second order,eta xi same
MatrixXd Eta_xy(M + 1, N + 1); //second order,eta xi same
MatrixXd Eta_yy(M + 1, N + 1); //second order,eta xi same
MatrixXd Xi_yy(M + 1, N + 1); //second order,eta xi same
MatrixXd Xi_xy(M + 1, N + 1); //second order,eta xi same

double d = 1.0;

MatrixXd U(M + 1, N + 1); //u velocity
MatrixXd V(M + 1, N + 1); //v velocity
MatrixXd W(M + 1, N + 1); //vorticity
MatrixXd PSI(M + 1, N + 1); //flow function

void readPortfolio(MatrixXd& matrix, string b)  //this will be a function that should hopefully return a matrix to use in other functions.
{
    ifstream data(b);
    string lineOfData;
   
    if (data.is_open())
    {
        int i = 0;
        while (data.good())
        {
            char linebuff[4096];
            getline(data, lineOfData);
            strncpy_s(linebuff, lineOfData.c_str(), sizeof(linebuff) - 1);

            //cout << lineOfData << endl; //just to check if the data was read.
            {
                int j = 0;
                double val;
                char* p_r = NULL, * p_val;
                p_val = strtok_s(linebuff, " ,;", &p_r);
                while (NULL != p_val) {
                    //?wrong??  sscanf_s("%lf", p_val, &val);
                    val = atof(p_val);
                    //cout << "  " << val;
                    matrix(i, j) = val;
                    j++;
                    p_val = strtok_s(NULL, " ,;", &p_r);
                }
                //cout << endl;
            }
            i++;
        }
    }
    else { std::cout << "Unable to open file"; }
   
}

void Jacobi() {
   
    int index = 0;
    X_xi = MatrixXd::Zero(M + 1, N + 1);
    Y_xi = MatrixXd::Zero(M + 1, N + 1);
    X_eta = MatrixXd::Zero(M + 1, N + 1);
    Y_eta = MatrixXd::Zero(M + 1, N + 1);
    X_xixi = MatrixXd::Zero(M + 1, N + 1);
    X_etaeta = MatrixXd::Zero(M + 1, N + 1);
    Y_xixi = MatrixXd::Zero(M + 1, N + 1);
    Y_etaeta = MatrixXd::Zero(M + 1, N + 1);
    J = MatrixXd::Zero(M + 1, N + 1);
    for (int i = 1; i < M; i++) {
        index = i - 1;
        for (int j = 1; j < N; j++) {
            X_xi(i, j) = (X(i + 1, j) - X(index, j)) / 2.0 / d;
            Y_xi(i, j) = (Y(i + 1, j) - Y(index, j)) / 2.0 / d;
            X_eta(i, j) = (X(i, j + 1) - X(i, j - 1)) / 2.0 / d;
            Y_eta(i, j) = (Y(i, j + 1) - Y(i, j - 1)) / 2.0 / d;
            X_xixi(i, j) = (X(i + 1, j) - 2 * X(i, j) + X(index, j)) / d / d;
            Y_xixi(i, j) = (Y(i + 1, j) - 2 * Y(i, j) + Y(index, j)) / d / d;
            X_etaeta(i, j) = (X(i, j + 1) - 2 * X(i, j) + X(i, j - 1)) / d / d;
            Y_etaeta(i, j) = (Y(i, j + 1) - 2 * Y(i, j) + Y(i, j - 1)) / d / d;
            X_xieta(i, j) = (X(i + 1, j + 1) - X(i + 1, j - 1) - X(index, j + 1) + X(index, j - 1)) / 4.0 / d / d;
            Y_xieta(i, j) = (Y(i + 1, j + 1) - Y(i + 1, j - 1) - Y(index, j + 1) + Y(index, j - 1)) / 4.0 / d / d;

        }
    }//inner


    for (int i = 1; i < M; i++) {
        index = i - 1;
        int j = N;
        X_xi(i, j) = (X(i + 1, j) - X(index, j)) / 2.0 / d;
        Y_xi(i, j) = (Y(i + 1, j) - Y(index, j)) / 2.0 / d;
        X_eta(i, j) = (X(i, j) - X(i, j - 1)) / d;
        Y_eta(i, j) = (Y(i, j) - Y(i, j - 1)) / d;
        X_xixi(i, j) = (X(i + 1, j) - 2 * X(i, j) + X(index, j)) / d / d;
        Y_xixi(i, j) = (Y(i + 1, j) - 2 * Y(i, j) + Y(index, j)) / d / d;
        X_etaeta(i, j) = X_etaeta(i, j - 1);
        Y_etaeta(i, j) = Y_etaeta(i, j - 1);
        X_xieta(i, j) = (X(i + 1, j) - X(i + 1, j - 1) - X(index, j) + X(index, j - 1)) / 2.0 / d / d;
        Y_xieta(i, j) = (Y(i + 1, j) - Y(i + 1, j - 1) - Y(index, j) + Y(index, j - 1)) / 2.0 / d / d;
    } // up without corner

    for (int i = 0; i < M + 1; i += M) {
        for (int j = 1; j <= N; j++) {
            Y_xi(i, j) = 0;
            X_eta(i, j) = 0;
            Y_eta(i, j) = (Y(i, j) - Y(i, j - 1)) / d;
            X_etaeta(i, j) = X_etaeta(i, j - 1);
            Y_etaeta(i, j) = Y_etaeta(i, j - 1);
           
        }
    }

    for (int j = 1; j <= N; j++) {
        X_xi(0, j) = (X(1, j) - X(0, j)) / d;
        X_xi(M, j) = (X(M, j) - X(M - 1, j)) / d;
        X_xixi(0, j) = X_xixi(1, j);
        Y_xixi(0, j) = Y_xixi(1, j);
        X_xixi(M, j) = X_xixi(M - 1, j);
        Y_xixi(M, j) = Y_xixi(M - 1, j);
        X_xieta(0, j) = (X(1, j) - X(1, j - 1) - X(0, j) + X(0, j - 1)) / d / d;
        Y_xieta(0, j) = (Y(1, j) - Y(1, j - 1) - Y(0, j) + Y(0, j - 1)) / d / d;
        X_xieta(M, j) = (X(M, j) - X(M, j - 1) - X(M - 1, j) + X(M - 1, j - 1)) / d / d;
        Y_xieta(M, j) = (Y(M, j) - Y(M, j - 1) - Y(M - 1, j) + Y(M - 1, j - 1)) / d / d;
    }
    X_xi(0, 0) = (X(1, 0) - X(0, 0)) / d;
    X_xi(M, 0) = (X(M, 0) - X(M - 1, 0)) / d;

    for (int i = 0; i < M + 1; i += M) {
        Y_xi(i, 0) = 0;
        X_eta(i, 0) = 0;
        Y_eta(i, 0) = (Y(i,1) - Y(i, 0)) / d;
        X_etaeta(i, 0) = X_etaeta(i, 1);
        Y_etaeta(i, 0) = Y_etaeta(i, 1);
      
    } 

    X_xixi(0, 0) = X_xixi(1, 0);
    Y_xixi(0, 0) = Y_xixi(1, 0);
    X_xixi(M, 0) = X_xixi(M - 1, 0);
    Y_xixi(M, 0) = Y_xixi(M - 1, 0);
    X_xieta(0, 0) = (X(0 + 1, 1) - X(0 + 1, 0) - X(0, 1) + X(0, 0)) / d / d;
    Y_xieta(0, 0) = (Y(0 + 1, 1) - Y(0 + 1, 0) - Y(0, 1) + Y(0, 0)) / d / d;
    X_xieta(M, 0) = (X(M, 1) - X(M, 0) - X(M - 1, 1) + X(M - 1, 0)) / d / d;
    Y_xieta(M, 0) = (Y(M, 1) - Y(M, 0) - Y(M - 1, 1) + Y(M - 1, 0)) / d / d;
    //side incluing corner




    for (int i = 1; i < M; i++) {
        index = i - 1;
        int j = 0;
        X_xi(i, j) = (X(i + 1, j) - X(index, j)) / 2.0 / d;
        Y_xi(i, j) = (Y(i + 1, j) - Y(index, j)) / 2.0 / d;
        X_eta(i, j) = (X(i, j + 1) - X(i, j)) / d;
        Y_eta(i, j) = (Y(i, j + 1) - Y(i, j)) / d;
        X_xixi(i, j) = (X(i + 1, j) - 2 * X(i, j) + X(index, j)) / d / d;
        Y_xixi(i, j) = (Y(i + 1, j) - 2 * Y(i, j) + Y(index, j)) / d / d;
        X_etaeta(i, j) = X_etaeta(i, j + 1);
        Y_etaeta(i, j) = Y_etaeta(i, j + 1);
        X_xieta(i, j) = (X(i + 1, j + 1) - X(i + 1, j) - X(index, j + 1) + X(index, j)) / 2.0 / d / d;
        Y_xieta(i, j) = (Y(i + 1, j + 1) - Y(i + 1, j) - Y(index, j + 1) + Y(index, j)) / 2.0 / d / d;
    }//down without corner


    for (int i = 0; i <= M; i++) {
        for (int j = 0; j <= N; j++) {
            double temp = X_xi(i, j) * Y_eta(i, j) - X_eta(i, j) * Y_xi(i, j);
            //cout << "temp" << temp << endl;
            J(i, j) = temp;
        }
    }
   
}

void deriviate() {
    Vector3d A, B, C, D;
    Matrix3d E;
    double temp;
    Xi_x = MatrixXd::Zero(M + 1, N + 1);
    Eta_x = MatrixXd::Zero(M + 1, N + 1);
    Xi_y = MatrixXd::Zero(M + 1, N + 1);
    Eta_y = MatrixXd::Zero(M + 1, N + 1);
    Xi_xx = MatrixXd::Zero(M + 1, N + 1);
    Xi_xy = MatrixXd::Zero(M + 1, N + 1);
    Xi_yy = MatrixXd::Zero(M + 1, N + 1);
    Eta_xx = MatrixXd::Zero(M + 1, N + 1);
    Eta_xy = MatrixXd::Zero(M + 1, N + 1);
    Eta_yy = MatrixXd::Zero(M + 1, N + 1);
    for (int i = 0; i <= M; i++) {
        for (int j = 0; j <= N; j++) {
            Xi_x(i, j) = Y_eta(i, j) / J(i, j);
            Eta_x(i, j) = -Y_xi(i, j) / J(i, j);
            Xi_y(i, j) = -X_eta(i, j) / J(i, j);
            Eta_y(i, j) = X_xi(i, j) / J(i, j);
            A << Xi_x(i, j) * X_xixi(i, j) + Xi_y(i, j) * Y_xixi(i, j), Xi_x(i, j)* X_xieta(i, j) + Xi_y(i, j) * Y_xieta(i, j), Xi_x(i, j)* X_etaeta(i, j) + Xi_y(i, j) * Y_etaeta(i, j);
            //cout << A << endl;
            E << pow(Y_eta(i, j), 2), -2 * Y_eta(i, j) * Y_xi(i, j), pow(Y_xi(i, j), 2),
                -X_eta(i, j) * Y_eta(i, j), (X_xi(i, j) * Y_eta(i, j) + X_eta(i, j) * Y_xi(i, j)), -X_xi(i, j) * Y_xi(i, j),
                pow(X_eta(i, j), 2), -2 * X_xi(i, j) * X_eta(i, j), pow(X_xi(i, j), 2);
            //temp = pow(X_xi(i, j) * Y_eta(i, j), 2) + pow(X_eta(i, j) * Y_xi(i, j), 2);
            temp = pow(X_xi(i, j) * Y_eta(i, j) - X_eta(i, j) * Y_xi(i, j), 2);
            B = -E * A / temp;
            Xi_xx(i, j) = B(0);
            Xi_xy(i, j) = B(1);
            Xi_yy(i, j) = B(2);
            C << Eta_x(i, j) * X_xixi(i, j) + Eta_y(i, j) * Y_xixi(i, j), Eta_x(i, j)* X_xieta(i, j) + Eta_y(i, j) * Y_xieta(i, j), Eta_x(i, j)* X_etaeta(i, j) + Eta_y(i, j) * Y_etaeta(i, j);
            D = -E * C / temp;
            Eta_xx(i, j) = D(0);
            Eta_xy(i, j) = D(1);
            Eta_yy(i, j) = D(2);
        }
    }
}

void init() {
    U = u0 * MatrixXd::Ones(M + 1, N + 1);
    //U.row(M) = MatrixXd::Zero(1, N + 1);
    for (int i = M3; i < M - M3; i++) {
        U(i, 0) = 0;
    }

    V = MatrixXd::Zero(M + 1, N + 1);
    PSI = MatrixXd::Zero(M + 1, N + 1);
    W = MatrixXd::Zero(M + 1, N + 1);
    for (int i = 0; i <= M; i++) {
        for (int j = 1; j <= N; j++) {
            PSI(i, j) = u0 * Y(i, j);
            //PSI(i, j) = 0;
            //cout << "psi "<<PSI(i,j) << endl;
        }
    }

    //for (int i = M1; i <= M1 + 2* M2; i++) {
    //            PSI(i, N) = u0 * Y(i, N);
    //            //PSI(i, j) = 0;
    //            //cout << "psi "<<PSI(i,j) << endl;
    //    }
    /*for (int i = M3; i < M - M3 + 1; i++) {
        for (int j = 0; j < 5; j++) {
            PSI(i, j) = 0;
        }
    }*/

    double sum = 0;
    for (int i = M3; i < M - M3 + 1; i++)
    {
        sum += PSI(i, 1);// PSI(i, 1);
    }
    double ave = sum / M;
    for (int i = M3; i < M - M3 + 1; i++)
    {
        PSI(i, 0) = ave;// PSI(i, 1);
    }
    //for (int i = 0; i < M; i++)
    //{
    //    PSI(i, 0) = 0;// PSI(i, 1);
    //}
    double a = 0;
    double b = 0;
    double r = 0;
    double o = 0;
    double e = 0;
    double temp = 0;
    int index = 0;
    for (int i = M3; i < M - M3; i++)
    {
        /*if (i > 0) {
            index = i - 1;
        }
        else
        {
            index = M - 1;
        }
        int j = 0;
        a = pow(Xi_x(i, j), 2) + pow(Xi_y(i, j), 2);
        b = Xi_x(i, j) * Eta_x(i, j) + Xi_y(i, j) * Eta_y(i, j);
        r = pow(Eta_x(i, j), 2) + pow(Eta_y(i, j), 2);
        o = Xi_xx(i, j) + Xi_yy(i, j);
        e = Eta_xx(i, j) + Eta_yy(i, j);
        W(i, 0) = (a * (PSI(i + 1, 0) - 2 * PSI(i, 0) + PSI(index, 0)) / d / d + b * (PSI(i + 1, 1) - PSI(i + 1, 0) - PSI(index, 1) + PSI(index, 0)) / d / d + r * (PSI(i, 2) - 2 * PSI(i, 1) + PSI(i, 0))) + o * (PSI(i + 1, 0) - PSI(index, 0)) / 2.0 + e * (PSI(i, 1) - PSI(i, 0));*/

        W(i, 0) = 2 * PSI(i, 1) * (pow(Eta_x(i, 0), 2) + pow(Eta_y(i, 0), 2));
    }
}
void push() {
    int index = 0;
    MatrixXd W_new(M + 1, N + 1);
    W_new = MatrixXd::Zero(M + 1, N + 1);
    double a = 0;
    double b = 0;
    double r = 0;
    double o = 0;
    double e = 0;
    double temp = 0;
    double b_w = 0;
    double b_e = 0;
    double b_s = 0;
    double b_n = 0;
    double b_p = 0;
    double c_p = 0;
    double b_w1 = 0;
    double b_e1 = 0;
    double b_s1 = 0;
    double b_n1 = 0;
    for (int i = 1; i < M; i++)
    {
        index = i - 1;
       
        for (int j = 1; j < N; j++)
        {
            a = pow(Xi_x(i, j), 2) + pow(Xi_y(i, j), 2);
            b = Xi_x(i, j) * Eta_x(i, j) + Xi_y(i, j) * Eta_y(i, j);
            r = pow(Eta_x(i, j), 2) + pow(Eta_y(i, j), 2);
            o = Xi_xx(i, j) + Xi_yy(i, j);
            e = Eta_xx(i, j) + Eta_yy(i, j);
            b_e = (a + o / 2.0) / Re;
            b_w = (a - o / 2.0) / Re;
            b_s = (r + e / 2.0) / Re;
            b_n = (r - e / 2.0) / Re;
            b_w1 = (U(i, j) * Xi_x(i, j) + V(i, j) * Xi_y(i, j)) / 2.0;
            b_e1 = -b_w1;
            b_n1 = (U(i, j) * Eta_x(i, j) + V(i, j) * Eta_y(i, j)) / 2.0;
            b_s1 = -b_n1;
            W_new(i, j) = W(i, j) + dt * (W(i + 1, j) * (b_e + b_e1) + W(index, j) * (b_w + b_w1) + W(i, j + 1) * (b_s + b_s1) + W(i, j - 1) * (b_n + b_n1) - 2.0 / Re * (a + r) * W(i, j) + 2.0 * b / Re * (W(i + 1, j + 1) - W(i + 1, j - 1) - W(index, j + 1) + W(index, j - 1)));
   
        }
    }
    int j = 0;
    for (int i = 1; i < M3; i++) {
        a = pow(Xi_x(i, j), 2) + pow(Xi_y(i, j), 2);
        b = Xi_x(i, j) * Eta_x(i, j) + Xi_y(i, j) * Eta_y(i, j);
        r = pow(Eta_x(i, j), 2) + pow(Eta_y(i, j), 2);
        o = Xi_xx(i, j) + Xi_yy(i, j);
        e = Eta_xx(i, j) + Eta_yy(i, j);
        b_e = (a + o / 2.0) / Re;
        b_w = (a - o / 2.0) / Re;
        b_s = (r + e / 2.0) / Re;
        b_n = (r - e / 2.0) / Re;
        b_w1 = (U(i, j) * Xi_x(i, j) + V(i, j) * Xi_y(i, j)) / 2.0;
        b_e1 = -b_w1;
        b_n1 = (U(i, j) * Eta_x(i, j) + V(i, j) * Eta_y(i, j)) / 2.0;
        b_s1 = -b_n1;
        double wi = W(M - i, 1), wii = W(M - i - 1, 1), wid = W(M - i + 1, 1);
        W_new(i, j) = W(i, j) + dt * (W(i + 1, j) * (b_e + b_e1) + W(i-1, j) * (b_w + b_w1) + W(i, j + 1) * (b_s + b_s1) + wi * (b_n + b_n1) - 2.0 / Re * (a + r) * W(i, j) + 2.0 * b / Re * (W(i + 1, j + 1) - wii - W(i-1, j + 1) + wid));
    }
    for (int i = M - M3 + 1; i < M; i++) {
        W_new(i, j) = W_new(M - i, j);
    }
    W = W_new;

}

void psi_iteration() {
    int index = 0;
    double b_w = 0;
    double b_e = 0;
    double b_s = 0;
    double b_n = 0;
    double b_p = 0;
    double c_p = 0;
    double a = 0;
    double b = 0;
    double r = 0;
    double o = 0;
    double e = 0;
    double temp = 0;
    MatrixXd psi_new(M + 1, N + 1);
    psi_new = PSI;
    //PSI.col(M) = PSI.col(0);
    int count = 0;

    while (1)
    {
        for (int i = 1; i < M; i++)
        {
            index = i - 1;
            for (int j = 1; j < N; j++)
            {
                //cout << "i:" << i << "j" << j << endl;
                a = pow(Xi_x(i, j), 2) + pow(Xi_y(i, j), 2);
                b = Xi_x(i, j) * Eta_x(i, j) + Xi_y(i, j) * Eta_y(i, j);
                r = pow(Eta_x(i, j), 2) + pow(Eta_y(i, j), 2);
                o = Xi_xx(i, j) + Xi_yy(i, j);
                e = Eta_xx(i, j) + Eta_yy(i, j);
                psi_new(i, j) = ((a - o / 2.0) * PSI(index, j) + (a + o / 2.0) * PSI(i + 1, j) + (r - e / 2.0) * PSI(i, j - 1) + (r + e / 2.0) * PSI(i, j + 1) + 2 * b * (PSI(i + 1, j + 1) - PSI(i + 1, j - 1) - PSI(index, j + 1) + PSI(index, j - 1)) - W(i, j)) / (2 * a + 2 * r);
            }
        }

        int j = 0;
        for (int i = 1; i < M3; i++) {
            a = pow(Xi_x(i, j), 2) + pow(Xi_y(i, j), 2);
            b = Xi_x(i, j) * Eta_x(i, j) + Xi_y(i, j) * Eta_y(i, j);
            r = pow(Eta_x(i, j), 2) + pow(Eta_y(i, j), 2);
            o = Xi_xx(i, j) + Xi_yy(i, j);
            e = Eta_xx(i, j) + Eta_yy(i, j);
            double pi = PSI(M - i, 1), pii = PSI(M - i - 1, 1), pid = PSI(M - i + 1, 1);
            psi_new(i, j) = ((a - o / 2.0) * PSI(i-1, j) + (a + o / 2.0) * PSI(i + 1, j) + (r - e / 2.0) * pi + (r + e / 2.0) * PSI(i, j + 1) + 2 * b * (PSI(i + 1, j + 1) - pii - PSI(i - 1, j + 1) + pid) - W(i, j)) / (2 * a + 2 * r);

        }

        for (int i = M - M3 + 1; i < M; i++) {
            psi_new(i, j) = psi_new(M - i, j);
        }


        double error = (PSI.block(1, 1, M - 1, N - 1) - psi_new.block(1, 1, M - 1, N - 1)).norm(); // psi_new.block(1, 1, M - 1, N - 1).norm();
        PSI = psi_new;

        std::cout << "psi error" << error << endl;
        count++;
        if (error < err) { return; }
    }

}
void velocity() {
    int index = 0;
    for (int i = 1; i < M; i++)
    {
            index = i - 1;

        for (int j = 1; j < N; j++)
        {
            U(i, j) = X_xi(i, j) / J(i, j) * (PSI(i, j + 1) - PSI(i, j - 1)) / 2.0 / d - X_eta(i, j) / J(i, j) * (PSI(i + 1, j) - PSI(index, j)) / 2.0 / d;
            V(i, j) = Y_xi(i, j) / J(i, j) * (PSI(i, j + 1) - PSI(i, j - 1)) / 2.0 / d - Y_eta(i, j) / J(i, j) * (PSI(i + 1, j) - PSI(index, j)) / 2.0 / d;
        }
    }
    int j = 0;
    for (int i = 1; i < M3; i++) {
        double pi = PSI(M - i, 1);
        U(i, j) = X_xi(i, j) / J(i, j) * (PSI(i, j + 1) - pi) / 2.0 / d - X_eta(i, j) / J(i, j) * (PSI(i + 1, j) - PSI(i-1, j)) / 2.0 / d;
        V(i, j) = Y_xi(i, j) / J(i, j) * (PSI(i, j + 1) - pi) / 2.0 / d - Y_eta(i, j) / J(i, j) * (PSI(i + 1, j) - PSI(i-1, j)) / 2.0 / d;
    }
    for (int i = M - M3 + 1; i < M; i++) {
        U(i, j) = U(M - i, j);
        V(i, j) = V(M - i, j);
    }
}
void boundary() {
    int index = 0;
  
    for (int i = 0; i < M + 1; i++)
    {
        int j = N;
        U(i, j) = u0;
        V(i, j) = 0;
        PSI(i, j) = u0 * Y(i, j);
        W(i, j) = 0;   
    }

    for (int j = 0; j < N; j++) {
        U(0, j) = u0;
        V(0, j) = 0;
        PSI(0, j) = u0 * Y(0, j);
        W(0, j) = 0;
        U(M, j) = u0;
        V(M, j) = 0;
        PSI(M, j) = u0 * Y(M, j);
        W(M, j) = 0;
    }

    //double sum = 0;
    //for (int i = M3; i < M - M3 + 1; i++)
    //{
    //    sum += PSI(i, 1);// PSI(i, 1);
    //}
    //double ave = sum / M;
    //for (int i = M3; i < M - M3 + 1; i++)
    //{
    //    PSI(i, 0) = ave;// PSI(i, 1);
    //}
    for (int i = 0; i < M; i++)
     {
        PSI(i, 0) = 0;// PSI(i, 1);
     }
    double a = 0;
    double b = 0;
    double r = 0;
    double o = 0;
    double e = 0;
    double temp = 0;

    for (int i = M3; i < M - M3 + 1; i++)
    {
        /*if (i > 0) {
            index = i - 1;
        }
        else
        {
            index = M - 1;
        }
        a = pow(Xi_x(i, j), 2) + pow(Xi_y(i, j), 2);
        b = Xi_x(i, j) * Eta_x(i, j) + Xi_y(i, j) * Eta_y(i, j);
        r = pow(Eta_x(i, j), 2) + pow(Eta_y(i, j), 2);
        o = Xi_xx(i, j) + Xi_yy(i, j);
        e = Eta_xx(i, j) + Eta_yy(i, j);
        W(i, 0) = (a * (PSI(i + 1, 0) - 2 * PSI(i, 0) + PSI(index, 0)) / d / d + b * (PSI(i + 1, 1) - PSI(i + 1, 0) - PSI(index, 1) + PSI(index, 0)) / d / d + r * (PSI(i, 2) - 2 * PSI(i, 1) + PSI(i, 0))) + o * (PSI(i + 1, 0) - PSI(index, 0)) / 2.0 + e * (PSI(i, 1) - PSI(i , 0));*/

        W(i, 0) = 2 * PSI(i, 1) * (pow(Eta_x(i, 0), 2) + pow(Eta_y(i, 0), 2));
    }

}



void init_breakpoint(int index1) {
    char filename[30];
    sprintf_s(filename, "./result/W%d.txt", index1);
    readPortfolio(W, filename);
    sprintf_s(filename, "./result/U%d.txt", index1);
    readPortfolio(U, filename);
    sprintf_s(filename, "./result/V%d.txt", index1);
    readPortfolio(V, filename);
    sprintf_s(filename, "./result/PSI%d.txt", index1);
    readPortfolio(PSI, filename);

}

int main()
{
    readPortfolio(X, "X.txt");
    readPortfolio(Y, "Y.txt");

    Jacobi();
    deriviate();
    ofstream xout("./result/X.data");
    xout << X;
    xout.close();
    ofstream yout("./result/Y.data");
    yout << Y;
    yout.close();
    ofstream jout("./result/J.data");
    jout << J;
    jout.close();
    init();
    boundary();
    init_breakpoint(99);
    //PSI.row(M) = PSI.row(0);
    ofstream Uout("./result/U.data");
    Uout << U;
    Uout.close();
    ofstream Vout("./result/V.data");
    Vout << V;
    Vout.close();
    ofstream wout("./result/W.data");
    wout << W;
    wout.close();
    ofstream pout("./result/PSI.data");
    pout << PSI;
    pout.close();
    char filename[30];
    err = 0.001;

    for (int k = 1; k <= 100; k++)
    {
        
        cout << "k" << k << endl;
        push();
        sprintf_s(filename, "./result/W%d.data", k);
        ofstream wout(filename);
        wout << W;
        wout.close();
        if (k == 1)
        {
            ofstream pout("./result/PSI.data");
            pout << PSI;
            pout.close();
        }
        psi_iteration();
        velocity();
        boundary();


        sprintf_s(filename, "./result/PSI%d.data", k);
        ofstream psiout(filename);
        psiout << PSI;
        psiout.close();
        sprintf_s(filename, "./result/U%d.data", k);
        ofstream Uout(filename);
        Uout << U;
        Uout.close();
        sprintf_s(filename, "./result/V%d.data", k);
        ofstream Vout(filename);
        Vout << V;
        Vout.close();

    }

    std::cout << "Hello World!\n";
    return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件

/*ofstream x1out("x_xi.txt");
    x1out << X_xi;
    x1out.close();
    ofstream x2out("x_eta.txt");
    x2out << X_eta;
    x2out.close();
    ofstream x3out("x_xixi.txt");
    x3out << X_xixi;
    x3out.close();
    ofstream x4out("x_etaeta.txt");
    x4out << X_etaeta;
    x4out.close();
    ofstream x5out("x_xieta.txt");
    x5out << X_xieta;
    x5out.close();
    ofstream y1out("y_xi.txt");
    y1out << X_xi;
    y1out.close();
    ofstream y2out("y_eta.txt");
    y2out << X_eta;
    y2out.close();
    ofstream y3out("y_xixi.txt");
    y3out << X_xixi;
    y3out.close();
    ofstream y4out("y_etaeta.txt");
    y4out << X_etaeta;
    y4out.close();
    ofstream y5out("y_xieta.txt");
    y5out << X_xieta;
    y5out.close();*/
    /*ofstream X1out("Xi_x.txt");
    X1out << Xi_x;
    X1out.close();
    ofstream X2out("Eta_x.txt");
    X2out << Eta_x;
    X2out.close();
    ofstream X3out("Xi_xx.txt");
    X3out << Xi_xx;
    X3out.close();
    ofstream X4out("Eta_xx.txt");
    X4out << Eta_xx;
    X4out.close();
    ofstream X5out("Xi_xy.txt");
    X5out << Xi_xy;
    X5out.close();
    ofstream Y1out("Xi_y.txt");
    Y1out << Xi_y;
    Y1out.close();
    ofstream Y2out("Eta_y.txt");
    Y2out << Eta_y;
    Y2out.close();
    ofstream Y3out("Xi_yy.txt");
    Y3out << Xi_yy;
    Y3out.close();
    ofstream Y4out("Eta_yy.txt");
    Y4out << Eta_yy;
    Y4out.close();
    ofstream Y5out("Eta_xy.txt");
    Y5out << Eta_xy;
    Y5out.close();*/
    //init();
    //int index = 0;