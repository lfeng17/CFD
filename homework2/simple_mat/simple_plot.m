close all
figure(1)
contourf(x,y,u(:,1:N+1)',50, 'edgecolor','none');colormap jet
colorbar;
axis([0 1 0 1]); 
title('U'); 
figure(2)
contourf(x,y,p(1:M+1,1:N+1)',50, 'edgecolor','none');colormap jet
colorbar;
title("pressure");
axis([0 1 0 1]); 
figure(3)
quiver(x,y,u(:,1:N+1)',v(1:M+1,:)',1.5);
axis([0 1 0 1]); 
axis equal
figure(4)
contourf(x,y,v(1:M+1,:)',50, 'edgecolor','none');colormap jet
colorbar;
axis([0 1 0 1]); 
title('V'); 
figure(5)
hold on
startx = 0:0.05:1;
starty = 0.5*ones(size(startx));
streamline(x,y,u(:,1:N+1)',v(1:M+1,:)',startx,starty);
startx = 0:0.02:0.2;
starty = 0.05*ones(size(startx));
streamline(x,y,u(:,1:N+1)',v(1:M+1,:)',startx,starty);
startx = 0.8:0.01:1;
starty = 0.02*ones(size(startx));
streamline(x,y,u(:,1:N+1)',v(1:M+1,:)',startx,starty);
quiver(x,y,u(:,1:N+1)',v(1:M+1,:)');
axis([0 1 0 1]); 
axis equal
title("streamline");
figure(6)
velocity=u(:,1:N+1)'.^2+v(1:M+1,:)'.^2;
contour(x,y,velocity,100);
axis([0 1 0 1]); 
axis equal