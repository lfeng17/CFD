hold on
axis equal
X=load("X.txt");
Y=load("Y.txt");

X0=load("X0.txt");
Y0=load("Y0.txt");
X5=load("X5.txt");
Y5=load("Y5.txt");
M=21;
N=21;


%��ɢ��ͼ
% X1=reshape(X,1,M*M);
% Y1=reshape(Y,1,N*N);
% scatter(X1,Y1);

%������ͼ
for i=1:M
    plot(X0(i,:),Y0(i,:),'-b');
    hold on
end

for j=2:N-1
    plot(X0(:,j),Y0(:,j),'-b');
    hold on
end

for i=1:M
    plot(X5(i,:),Y5(i,:),'-g');
    hold on
end

for j=2:N-1
    plot(X5(:,j),Y5(:,j),'-g');
    hold on
end
for i=1:M
    plot(X(i,:),Y(i,:),'-');
    hold on
end

for j=2:N-1
    plot(X(:,j),Y(:,j),'-');
    hold on
end
theta=0:pi/100:2*pi;
temp = 0.5*cos(theta)+0.5;
x= temp - 0.5;
y= 0.594689181 .* (0.298222773 .* temp.^0.5 - 0.127125232 .* temp - 0.357907906 .* temp.^2 + 0.291984971 .* temp.^3 - 0.105174606 .* temp.^4) .* sign(pi-theta); 
plot(x,y,'-r')

r=7.5; theta=0:pi/100:2*pi;
x=r*cos(theta); y=r*sin(theta);
plot(x,y,'-r')