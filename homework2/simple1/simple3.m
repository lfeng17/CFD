close all
u0 = 1.0;
Re = 10000;	
alphau = 0.3;
alphap = 1-alphau;
err = 1e-4;
M = 33;
N = M;
dx = 1/M; 
x = 0:dx:1; 
dy = 1/N; 
y = 0:dy:1;

u =	zeros(M+1,N+2);
ustar = zeros(M+1,N+2); 
uprime =zeros(M+1,N+2); 
v =   zeros(M+2,N+1); 
vstar = zeros(M+2,N+1); 
vprime =   zeros(M+2,N+1);
p =zeros(M+2,N+2); 
pstar = zeros(M+2,N+2); 
pprime =  zeros(M+2,N+2); 
dU =       zeros(M+1,N+2);
dV =	zeros(M+2,N+1); 
uLeft =	zeros(1,N+2);

% u(:,:) = 0.0;
% v(:,:) = 0.0;
% p(:,:) = 0.0;
uold = u; 
vold = v; 
pstar = p;

for n = 1:maxIter
ustar=uold;
for i = 2:M
for j = 2:N+1
uce = dy / 2.0 * (uold(i, j) + uold(i + 1, j));
ucw = dy / 2.0 * (uold(i, j) + uold(i - 1, j)); 
ucn = dx / 2.0 * (vold(i, j) + vold(i + 1, j));
ucs = dx / 2.0 * (vold(i, j - 1) + vold(i + 1, j - 1)); 
xce = dy / dx / Re;
ydn = dx / dy / Re;
uae = xce + max(0, -uce); 
uaw = xce + max(0, ucw); 
uan = ydn + max(0, -ucn); 
uas = ydn + max(0, ucs); 			
uap = (uae + uaw + uan + uas)/alphau;
pressureu = dy * (pstar(i + 1, j) - pstar(i, j));		
dU(i, j) = dy / uap;
ustar(i, j) = (uae * ustar(i + 1, j) + uaw * ustar(i - 1, j) + uan * ustar(i, j + 1) + uas * ustar(i, j - 1) - pressureu ) / uap + (1 - alphau) * uold(i, j);	
end
end

for i = 2:M+1
for j = 2:N
vce = dy / 2.0 * (uold(i, j) + uold(i, j + 1)); 
vcw = dy / 2.0 * (uold(i - 1, j) + uold(i - 1, j + 1)); 
vcn = dx / 2.0 * (vold(i, j) + vold(i, j + 1)); 
vcs = dx / 2.0 * (vold(i, j) + vold(i, j - 1)); 
xce = dy / dx / Re;
ydn = dx / dy / Re;
vae = xce + max(0, -vce); 
vaw = xce + max(0, vcw); 
van = ydn + max(0, -vcn); 
vas = ydn + max(0, vcs); 
vap = (vae + vaw + van + vas) / alphau;
pressurev = dy * (pstar(i, j + 1) - pstar(i, j));
dV(i, j) = dx / vap;
vstar(i, j) = (vae * vstar(i + 1, j) + vaw * vstar(i - 1, j) + van * vstar(i, j + 1) + vas * vstar(i, j - 1) - pressurev ) / vap + (1 - alphau) * vold(i, j);		
end
end

pprime(:,:)=0;
for i = 2:M+1
for j = 2:N+1
ape = dU(i, j) * dy;
apw = dU(i - 1, j) * dy;
apn = dV(i, j) * dx;
aps = dV(i, j - 1) * dx;
ap = ape + apw + apn + aps;
bp = (ustar(i - 1, j) - ustar(i, j)) * dy + (vstar(i, j - 1) - vstar(i, j)) * dx;
pprime(i, j) = (ape * pprime(i + 1, j) + apw * pprime(i - 1, j) + apn * pprime(i, j + 1) + aps * pprime(i, j - 1) + bp) / ap;			
end
end
%如果没有这一步，会出来个inf
if n == 1
i = 2;
j = 2;
 ape = 0;
 apw = 0;
 apn = 0;
 aps = 0;
 ap = 1;
 bp = 0;
pprime(i, j) = (ape * pprime(i + 1, j) + apw * pprime(i - 1, j) + apn * pprime(i, j + 1) + aps * pprime(i, j - 1) + bp) / ap;
end

for i = 2:M
for j = 2:N+1
uprime(i, j) = dU(i, j) * (pprime(i, j) - pprime(i + 1, j));
				u(i,j) = ustar(i, j) + uprime(i, j);
end
end

for i = 2:M+1
for j = 2:N
vprime(i, j) = dV(i, j) * (pprime(i, j) - pprime(i, j + 1));
				v(i, j) = vstar(i, j) + vprime(i, j);
end
end

for i = 2:M+1
for j = 2:N+1 
p(i,j)=pstar(i,j)+pprime(i,j)*alphap;
end
end

if n>2
error=norm(pprime);
if error < err
break;
end

end
disp(error)
uold = u;
vold = v; 
pstar = p;


uold(:,1) = 0.0;	
uold(2:M,N+2) = 2*u0-uold(2:M,N+1);	
vold(1,:) =	0.0;	
vold(M+2,:) = 0.0;	

end
figure(1)
contourf(x,y,u(:,1:N+1)',50, 'edgecolor','none');colormap jet
colorbar;
axis([0 1 0 1]); 
title('U'); 
figure(2)
contourf(x,y,p(1:M+1,1:N+1)',50, 'edgecolor','none');colormap jet
colorbar;
axis([0 1 0 1]); 
figure(3)
quiver(x,y,u(:,1:N+1)',v(1:M+1,:)');
axis([0 1 0 1]); 
axis equal
figure(4)
contourf(x,y,v(1:M+1,:)',50, 'edgecolor','none');colormap jet
colorbar;
axis([0 1 0 1]); 
title('V'); 
figure(5)
hold on
startx = 0:0.2:1;
starty = 0.5*ones(size(startx));
streamline(x,y,u(:,1:N+1)',v(1:M+1,:)',startx,starty);
startx = 0:0.02:0.2;
starty = 0.2*ones(size(startx));
streamline(x,y,u(:,1:N+1)',v(1:M+1,:)',startx,starty);
startx = 0.9:0.01:1;
starty = 0.95*ones(size(startx));
streamline(x,y,u(:,1:N+1)',v(1:M+1,:)',startx,starty);
axis([0 1 0 1]); 
axis equal


