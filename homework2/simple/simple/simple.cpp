﻿// simple.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <fstream>
using namespace std;
#include<Eigen/Dense>
using namespace Eigen;
#include <math.h>
#include <string.h>
#include <stdio.h>
#define M 30
#define N 30
MatrixXd X(M + 1, N + 1); //x coordinate
MatrixXd Y(M + 1, N + 1); //y coordinate
MatrixXd U(M + 1, N);//u(i-1/2,j)toU(i,j);u(i+1/2,j)toU(i+1,j)
MatrixXd V(M , N + 1);//v(i,j-1/2)toV(i,j);v(i,j+1/2)toV(i,j+1)
MatrixXd P(M, N);
double u0 = 1;
double dx = 1.0 / float(M);
double dy = 1.0 / float(N);
double dt = 0.01;
double Re = 2000;

void mesh() {
	for (int i = 0; i <= M; i++)
	{
		for (int j = 0; j <= N; j++)
		{
			X(i, j) = float(i) / float(M);
			Y(i, j) = float(j) / float(N);
		}
	}
}

void init() {
	std::cout << "init begin" << endl;
	U = MatrixXd::Zero(M + 1, N);
	V = MatrixXd::Zero(M, N + 1);
	P = MatrixXd::Zero(M, N);
	std::cout << "zero" << endl;
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < N; j++)
		{
			//P(i, j) = pow(10, 3) * 9.8 * float(j) / float(N);
			P(i, j) = 0;
		}
	}
	for (int i = 1; i <= M; i++)
	{
		for (int j = 0; j < N; j++)
		{
			//U(i, j) = j / float(N) * u0;
			//U(M + 1, N)
			U(i, j) = 0;
		}
	}
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < N; j++)
		{
			V(i, j) =0;
		}
	}
}

void estimationuv(MatrixXd& u, MatrixXd& v, MatrixXd& p) {
	MatrixXd unew(M + 1, N);
	MatrixXd vnew(M, N + 1);
	double a1 = 0.7;
	unew = u;
	vnew = v;
	double err_x = 1;
	double err_y = 1;
	int flag = 1;
	while (flag) {
		//u M+1,N 最多考虑到i+2,i只能迭代到M-2(对应下标为M）
		//u迭代范围 j方向1~N-2,差0、N-1（上下边界）
		//i方向 1~M-1，差0、M（左右边界）,直接在边界条件boundary()赋值为0即可
		for (int i = 0; i < M-1; i++)
		{
			int j = 0;
			//a(i+1/2,j)
			double a1 = dx * dy / dt + dy * ((u(i + 2, j) + u(i + 1, j)) / 4.0 - (u(i + 1, j) + u(i, j)) / 4.0 + 2.0 / Re / dx) + dx * ((v(i, j + 1) + v(i + 1, j + 1)) / 4.0 - (v(i, j) + v(i + 1, j)) / 4.0 + 2.0 / Re / dy);
			//a(i-1/2,j)
			double a2 = dy * ((u(i + 1, j) + u(i, j)) / 4.0 + 1.0 / Re / dx);
			//a(i+3/2,j)
			double a3 = dy * (-(u(i + 2, j) + u(i + 1, j)) / 4.0 + 1.0 / Re / dx);
			//a(i+1/2,j-1)
			double a4 = dx * ((v(i, j) + v(i + 1, j)) / 4.0 + 1.0 / Re / dy);
			//a(i+1/2,j+1)
			double a5 = dx * (-(v(i, j + 1) + v(i + 1, j + 1)) / 4.0 + 1.0 / Re / dy);
			unew(i + 1, j) = (a2 * unew(i, j) + a3 * unew(i + 2, j) - a4 * unew(i + 1, j ) + a5 * unew(i + 1, j + 1) - dy * (p(i + 1, j) - p(i, j)) + dx * dy / dt * u(i + 1, j)) / a1;

			for (int j = 1; j < N-1; j++)
			{
				//a(i+1/2,j)
				a1 = dx * dy / dt + dy * ((u(i + 2, j) + u(i + 1, j)) / 4.0 - (u(i + 1, j) + u(i, j)) / 4.0 + 2.0 / Re / dx) + dx * ((v(i, j + 1) + v(i + 1, j + 1)) / 4.0 - (v(i, j) + v(i + 1, j)) / 4.0 + 2.0 / Re / dy);
				//a(i-1/2,j)
				a2 = dy * ((u(i + 1, j) + u(i, j)) / 4.0 + 1.0 / Re / dx);
				//a(i+3/2,j)
				a3 = dy * (-(u(i + 2, j) + u(i + 1, j)) / 4.0 + 1.0 / Re / dx);
				//a(i+1/2,j-1)
				a4 = dx * ((v(i, j) + v(i + 1, j)) / 4.0 + 1.0 / Re / dy);
				//a(i+1/2,j+1)
				a5 = dx * (-(v(i, j + 1) + v(i + 1, j + 1)) / 4.0 + 1.0 / Re / dy);
				unew(i + 1, j) = (a2 * unew(i, j) + a3 * unew(i + 2, j) + a4 * unew(i + 1, j - 1) + a5 * unew(i + 1, j + 1) - dy * (p(i + 1, j) - p(i, j)) + dx * dy / dt * u(i + 1, j)) / a1;
			}
			j = N - 1;
			a1 = dx * dy / dt + dy * ((u(i + 2, j) + u(i + 1, j)) / 4.0 - (u(i + 1, j) + u(i, j)) / 4.0 + 2.0 / Re / dx) + dx * ((v(i, j + 1) + v(i + 1, j + 1)) / 4.0 - (v(i, j) + v(i + 1, j)) / 4.0 + 2.0 / Re / dy);
			//a(i-1/2,j)
			a2 = dy * ((u(i + 1, j) + u(i, j)) / 4.0 + 1.0 / Re / dx);
			//a(i+3/2,j)
			a3 = dy * (-(u(i + 2, j) + u(i + 1, j)) / 4.0 + 1.0 / Re / dx);
			//a(i+1/2,j-1)
			a4 = dx * ((v(i, j) + v(i + 1, j)) / 4.0 + 1.0 / Re / dy);
			//a(i+1/2,j+1)
			a5 = dx * (-(v(i, j + 1) + v(i + 1, j + 1)) / 4.0 + 1.0 / Re / dy);
			unew(i + 1, j) = (a2 * unew(i, j) + a3 * unew(i + 2, j) + a4 * unew(i + 1, j - 1) + a5 * (2 * u0 - unew(i + 1, j)) - dy * (p(i + 1, j) - p(i, j)) + dx * dy / dt * u(i + 1, j)) / a1;


		}
		//v M,N+1 最多考虑到j+2,j只能迭代到N-2
		//v迭代范围 i方向1~M-2,差0、M-1（左右边界）
		//j方向 1~N-1，差0、N（上下边界）
		for (int j = 0; j < N - 1; j++)
		{
			int i = 0;
			//b(i,j+1/2)
			double b1 = dx * dy / dt + dx * ((v(i, j + 2) + v(i, j + 1)) / 4.0 - (v(i, j + 1) + v(i, j)) / 4.0 + 2.0 / Re / dy) + dy * ((u(i + 1, j) + u(i + 1, j + 1)) / 4.0 - (u(i, j) + u(i, j + 1)) / 4.0 + 2.0 / Re / dx);
			//b(i-1,j+1/2)
			double b2 = dy * ((u(i, j) + u(i, j + 1)) / 4.0 + 1.0 / Re / dx);
			//b(i+1,j+1/2)
			double b3 = dy * (-(u(i + 1, j) + u(i + 1, j + 1)) / 4.0 + 1.0 / Re / dx);
			//b(i,j-1/2)
			double b4 = dx * ((v(i, j + 1) + v(i, j)) / 4.0 + 1.0 / Re / dy);
			//b(i,j+3/2)
			double b5 = dx * (-(v(i, j + 2) + v(i, j + 1)) / 4.0 + 1.0 / Re / dy);
			vnew(i, j + 1) = ( b3 * vnew(i + 1, j + 1) + b4 * vnew(i, j) + b5 * vnew(i, j + 2) - dx * (p(i, j + 1) - p(i, j)) + dx * dy / dt * v(i, j + 1)) / (b1 + b2);

			for (int i = 1; i < M - 1; i++)
			{
				//b(i,j+1/2)
				 b1 = dx * dy / dt + dx * ((v(i, j + 2) + v(i, j + 1)) / 4.0 - (v(i, j + 1) + v(i, j)) / 4.0 + 2.0 / Re / dy) + dy * ((u(i + 1, j) + u(i + 1, j + 1)) / 4.0 - (u(i, j) + u(i, j + 1)) / 4.0 + 2.0 / Re / dx);
				//b(i-1,j+1/2)
				 b2 = dy * ((u(i, j) + u(i, j + 1)) / 4.0 + 1.0 / Re / dx);
				//b(i+1,j+1/2)
				 b3 = dy * (-(u(i + 1, j) + u(i + 1, j + 1)) / 4.0 + 1.0 / Re / dx);
				//b(i,j-1/2)
				 b4 = dx * ((v(i, j + 1) + v(i, j)) / 4.0 + 1.0 / Re / dy);
				//b(i,j+3/2)
				 b5 = dx * (-(v(i, j + 2) + v(i, j + 1)) / 4.0 + 1.0 / Re / dy);
				vnew(i, j + 1) = (b2 * vnew(i - 1, j + 1) + b3 * vnew(i + 1, j + 1) + b4 * vnew(i, j) + b5 * vnew(i, j + 2) - dx * (p(i, j + 1) - p(i, j)) + dx * dy / dt * v(i, j + 1)) / b1;
			}
			i = M - 1;
			 b1 = dx * dy / dt + dx * ((v(i, j + 2) + v(i, j + 1)) / 4.0 - (v(i, j + 1) + v(i, j)) / 4.0 + 2.0 / Re / dy) + dy * ((u(i + 1, j) + u(i + 1, j + 1)) / 4.0 - (u(i, j) + u(i, j + 1)) / 4.0 + 2.0 / Re / dx);
			//b(i-1,j+1/2)
			 b2 = dy * ((u(i, j) + u(i, j + 1)) / 4.0 + 1.0 / Re / dx);
			//b(i+1,j+1/2)
			 b3 = dy * (-(u(i + 1, j) + u(i + 1, j + 1)) / 4.0 + 1.0 / Re / dx);
			//b(i,j-1/2)
			 b4 = dx * ((v(i, j + 1) + v(i, j)) / 4.0 + 1.0 / Re / dy);
			//b(i,j+3/2)
			 b5 = dx * (-(v(i, j + 2) + v(i, j + 1)) / 4.0 + 1.0 / Re / dy);
			vnew(i, j + 1) = (b2 * vnew(i - 1, j + 1) + b4 * vnew(i, j) + b5 * vnew(i, j + 2) - dx * (p(i, j + 1) - p(i, j)) + dx * dy / dt * v(i, j + 1)) / (b1 + b3);

		}
		err_x = (unew - u).norm();
		err_y = (vnew - v).norm();
		std::cout << "err_u" << err_x << endl;
		std::cout << "err_v" << err_y << endl;
		u = a1 * unew + (1 - a1) * u;
		v = a1 * vnew + (1 - a1) * v;
		if ((err_x < 0.01) && (err_y < 0.01)) {
			flag = 0;
		}
	}
}

void fix(MatrixXd& u, MatrixXd& v, MatrixXd& p) {
	/*MatrixXd up(M + 1, N);
	MatrixXd vp(M, N + 1);*/
	double a0 = 0.2;
	MatrixXd pp(M, N);
	MatrixXd ppnew(M, N);
	ppnew= MatrixXd::Zero(M, N);
	pp= MatrixXd::Zero(M, N);
	double a = 0;
	double err = 1;
	while (err > 0.01) {
		std::cout << "内部修正" << endl;
		for (int i = 1; i < M - 1; i++)
		{
			for (int j = 1; j < N - 1; j++)
			{
				//用到了i+2 j+2 i-1 j-1,对于p差周围一圈边界
				//a(i+1/2,j)
				double a1 = dx * dy / dt + dy * ((u(i + 2, j) + u(i + 1, j)) / 4.0 - (u(i + 1, j) + u(i, j)) / 4.0 + 2.0 / Re / dx) + dx * ((v(i, j + 1) + v(i + 1, j + 1)) / 4.0 - (v(i, j) + v(i + 1, j)) / 4.0 + 2.0 / Re / dy);
				//a(i-1/2,j)
				double a2 = dy * ((u(i + 1, j) + u(i, j)) / 4.0 + 1.0 / Re / dx);
				//b(i,j+1/2)
				double b1 = dx * dy / dt + dx * ((v(i, j + 2) + v(i, j + 1)) / 4.0 - (v(i, j + 1) + v(i, j)) / 4.0 + 2.0 / Re / dy) + dy * ((u(i + 1, j) + u(i + 1, j + 1)) / 4.0 - (u(i, j) + u(i, j + 1)) / 4.0 + 2.0 / Re / dx);
				//b(i,j-1/2)
				double b4 = dx * ((v(i, j + 1) + v(i, j)) / 4.0 + 1.0 / Re / dy);
				a = (pow(dy, 2) / a1 + pow(dy, 2) / a2 + pow(dx, 2) / b1 + pow(dx, 2) / b4);
				ppnew(i, j) = (pow(dy, 2) / a1 * ppnew(i - 1, j) + pow(dy, 2) / a2 * ppnew(i + 1, j) + pow(dx, 2) / b1 * ppnew(i, j - 1) + pow(dx, 2) / b4 * pp(i, j + 1) - dy * (u(i + 1, j) - u(i, j)) - dx * (v(i, j + 1) - v(i, j))) / a;

			}
		}
		std::cout << "左侧边界" << endl;
		//左侧边界//v(M,N+1) u(M+1,N)
		int i = 0;
		for (int j = 1; j < N - 1; j++) {
			//a(i+1/2,j)
			double a1 = dx * dy / dt + dy * ((u(i + 2, j) + u(i + 1, j)) / 4.0 - (u(i + 1, j) + u(i, j)) / 4.0 + 2.0 / Re / dx) + dx * ((v(i, j + 1) + v(i + 1, j + 1)) / 4.0 - (v(i, j) + v(i + 1, j)) / 4.0 + 2.0 / Re / dy);
			//a(i-1/2,j)
			double a2 = dy * ((u(i + 1, j) + u(i, j)) / 4.0 + 1.0 / Re / dx);
			//b(i,j+1/2)
			double b1 = dx * dy / dt + dx * ((v(i, j + 2) + v(i, j + 1)) / 4.0 - (v(i, j + 1) + v(i, j)) / 4.0 + 2.0 / Re / dy) + dy * ((u(i + 1, j) + u(i + 1, j + 1)) / 4.0 - (u(i, j) + u(i, j + 1)) / 4.0 + 2.0 / Re / dx);
			//b(i,j-1/2)
			double b4 = dx * ((v(i, j + 1) + v(i, j)) / 4.0 + 1.0 / Re / dy);
			a = (pow(dy, 2) / a1 + pow(dy, 2) / a2 + pow(dx, 2) / b1 + pow(dx, 2) / b4);
			ppnew(i, j) = (pow(dy, 2) / a2 * pp(i + 1, j) + pow(dx, 2) / b1 * pp(i, j - 1) + pow(dx, 2) / b4 * pp(i, j + 1) - dy * (u(i + 1, j) - u(i, j)) - dx * (v(i, j + 1) - v(i, j))) / (a- dy / a1);
		}
		std::cout << "右侧边界" << endl;
		//右侧边界
		i = M - 1;
		for (int j = 1; j < N - 1; j++) {
			//v(M,N+1) u(M+1,N)
			//a(i+1/2,j)
			double a1 = dx * dy / dt + dy * (/*(u(i + 2, j) + u(i + 1, j)) / 4.0 此处应用边界条件变为0*/0 - (u(i + 1, j) + u(i, j)) / 4.0 + 2.0 / Re / dx) + dx *( /*(v(i, j + 1) + v(i + 1, j + 1))*/0.0 / 4.0 - /*(v(i, j) + v(i + 1, j))*/0.0 / 4.0 + 2.0 / Re / dy);
			//a(i-1/2,j)
			double a2 = dy * ((u(i + 1, j) + u(i, j)) / 4.0 + 1.0 / Re / dx);
			//b(i,j+1/2)
			double b1 = dx * dy / dt + dx * ((v(i, j + 2) + v(i, j + 1)) / 4.0 - (v(i, j + 1) + v(i, j)) / 4.0 + 2.0 / Re / dy) + dy * ((u(i + 1, j) + u(i + 1, j + 1)) / 4.0 - (u(i, j) + u(i, j + 1)) / 4.0 + 2.0 / Re / dx);
			//b(i,j-1/2)
			double b4 = dx * ((v(i, j + 1) + v(i, j)) / 4.0 + 1.0 / Re / dy);
			a = (pow(dy, 2) / a1 + pow(dy, 2) / a2 + pow(dx, 2) / b1 + pow(dx, 2) / b4);
			ppnew(i, j) = (pow(dy, 2) / a1 * pp(i - 1, j) + pow(dx, 2) / b1 * pp(i, j - 1) + pow(dx, 2) / b4 * pp(i, j + 1) - dy * (u(i + 1, j) - u(i, j)) - dx * (v(i, j + 1) - v(i, j))) / (a- dy / a2);
		}
		std::cout << "下侧边界" << endl;
		//下侧边界
		int j = 0;
		for (int i = 1; i < M - 1; i++) {
			//a(i+1/2,j)
			double a1 = dx * dy / dt + dy * ((u(i + 2, j) + u(i + 1, j)) / 4.0 - (u(i + 1, j) + u(i, j)) / 4.0 + 2.0 / Re / dx) + dx * ((v(i, j + 1) + v(i + 1, j + 1)) / 4.0 - (v(i, j) + v(i + 1, j)) / 4.0 + 2.0 / Re / dy);
			//a(i-1/2,j)
			double a2 = dy * ((u(i + 1, j) + u(i, j)) / 4.0 + 1.0 / Re / dx);
			//b(i,j+1/2)
			double b1 = dx * dy / dt + dx * ((v(i, j + 2) + v(i, j + 1)) / 4.0 - (v(i, j + 1) + v(i, j)) / 4.0 + 2.0 / Re / dy) + dy * ((u(i + 1, j) + u(i + 1, j + 1)) / 4.0 - (u(i, j) + u(i, j + 1)) / 4.0 + 2.0 / Re / dx);
			//b(i,j-1/2)
			double b4 = dx * ((v(i, j + 1) + v(i, j)) / 4.0 + 1.0 / Re / dy);
			a = (pow(dy, 2) / a1 + pow(dy, 2) / a2 + pow(dx, 2) / b1 + pow(dx, 2) / b4);
			ppnew(i, j) = (pow(dy, 2) / a1 * pp(i - 1, j) + pow(dy, 2) / a2 * pp(i + 1, j) + pow(dx, 2) / b4 * pp(i, j + 1) - dy * (u(i + 1, j) - u(i, j)) - dx * (v(i, j + 1) - v(i, j))) / (a - dx / b1);
		}
		std::cout << "上侧边界" << endl;
		//上侧边界
		//v(M,N+1) u(M+1,N)
		 j = N - 1;
		for (int i = 1; i < M - 1; i++) {
			//a(i+1/2,j)
			double a1 = dx * dy / dt + dy * ((u(i + 2, j) + u(i + 1, j)) / 4.0 - (u(i + 1, j) + u(i, j)) / 4.0 + 2.0 / Re / dx) + dx * ((v(i, j + 1) + v(i + 1, j + 1)) / 4.0 - (v(i, j) + v(i + 1, j)) / 4.0 + 2.0 / Re / dy);
			//a(i-1/2,j)
			double a2 = dy * ((u(i + 1, j) + u(i, j)) / 4.0 + 1.0 / Re / dx);
			//b(i,j+1/2)
			double b1 = dx * dy / dt + dx * (/*(v(i, j + 2) + v(i, j + 1)) / 4.0*/0.0 - (v(i, j + 1) + v(i, j)) / 4.0 + 2.0 / Re / dy) + dy * (/*(u(i + 1, j) + u(i + 1, j + 1))*/2.0*u0 / 4.0 - /*(u(i, j) + u(i, j + 1))*/2.0*u0 / 4.0 + 2.0 / Re / dx);
			//b(i,j-1/2)
			double b4 = dx * ((v(i, j + 1) + v(i, j)) / 4.0 + 1.0 / Re / dy);
			a = (pow(dy,2) / a1 + pow(dy, 2) / a2 + pow(dx, 2) / b1 + pow(dx, 2) / b4);
			ppnew(i, j) = (pow(dy, 2) / a1 * pp(i - 1, j) + pow(dy, 2) / a2 * pp(i + 1, j) + pow(dx, 2) / b1 * pp(i, j - 1) - dy * (u(i + 1, j) - u(i, j)) - dx * (v(i, j + 1) - v(i, j))) / (a - dx / b4);

		}
		err = (pp - ppnew).norm();
		std::cout << "p fix error" << err << endl;
		
		//std::cout << pp << endl;
		pp = a0*ppnew+(1-a0)*pp;
	}
	MatrixXd pnew(M, N);
	MatrixXd unew(M + 1, N);//u(i-1/2,j)toU(i,j);u(i+1/2,j)toU(i+1,j)
	MatrixXd vnew(M, N + 1);//v(i,j-1/2)toV(i,j);v(i,j+1/2)toV(i,j+1)
	unew = u;
	vnew = v;
	//std::cout << pp << endl;
	pnew = p + 0.8* pp;
	p = pnew;
	for (int i = 0; i < M - 1; i++)
	{
		for (int j = 0; j <= N - 1; j++)
		{
			//v(M,N+1) u(M+1,N)
			//最多使用了i+2 j+1
			//i从0到M-2，对应u从1到M-1，差左右为速度0，boundary()给出
			//j从0到N-1，不差
			//a(i+1/2,j)
			double a1 = dx * dy / dt + dy * ((u(i + 2, j) + u(i + 1, j)) / 4.0 - (u(i + 1, j) + u(i, j)) / 4.0 + 2.0 / Re / dx) + dx * ((v(i, j + 1) + v(i + 1, j + 1)) / 4.0 - (v(i, j) + v(i + 1, j)) / 4.0 + 2.0 / Re / dy);
			unew(i + 1, j) = u(i + 1, j) - dy / a1 * (pp(i + 1, j) - pp(i, j));
		}
	}
	u = unew;
	for (int i = 0; i <= M - 1; i++)
	{
		for (int j = 0; j < N - 1; j++)
		{
			//v(M,N+1) u(M+1,N)
			//最多使用了j+2 i+1
			//i从0到M-1，对应v从0到M-1，不差
			//j从0到N-2，对应从1到N-1，差上下为速度0，boundary()给出
			//b(i,j+1/2)
			double b1 = dx * dy / dt + dx * ((v(i, j + 2) + v(i, j + 1)) / 4.0 - (v(i, j + 1) + v(i, j)) / 4.0 + 2.0 / Re / dy) + dy * ((u(i + 1, j) + u(i + 1, j + 1)) / 4.0 - (u(i, j) + u(i, j + 1)) / 4.0 + 2.0 / Re / dx);
			vnew(i, j + 1) = v(i, j + 1) - dx / b1 * (pp(i, j + 1) - pp(i, j));
		}
	}
	v = vnew;
}

void boundary() {
	for (int j = 0; j < N; j++)
	{
		U(0, j) = 0;
		U(M, j) = 0;
	}
	for (int i = 0; i < M; i++)
	{
		V(i, 0) = 0;
		V(i, N) = 0;
	}
	P(0, 0) = (P(0, 1) + P(1, 0)) / 2.0;
	P(0, N - 1) = (P(0, N - 2) + P(1, N - 1)) / 2.0;
	P(M - 1, 0) = (P(M - 2, 0) + P(M - 1, 1)) / 2.0;
	P(M - 1, N - 1) = (P(M - 2, N - 1) + P(M - 1, N - 2)) / 2.0;
	U.row(0) = -U.row(1);
	//右侧边界
	U.row(M) = -U.row(M - 1);
	U.col(0) = MatrixXd::Zero(M + 1, 1);
	U.col(N - 1) = u0 * MatrixXd::Ones(M + 1, 1);
	V.row(0) = MatrixXd::Zero(1, N + 1);
	V.row(N - 1) = MatrixXd::Zero(1, N + 1);
	V.col(0) = -V.col(1);
	V.col(M) = -V.col(M - 1);
}

int main()
{
    std::cout << "Hello World!\n";
	char filename[30];
	mesh();

	ofstream xout("./result/X.txt");
	xout << X;
	xout.close();
	ofstream yout("./result/Y.txt");
	yout << Y;
	yout.close();
	init();
	boundary();
	int k = 0;
	sprintf_s(filename, "./result/p%d.txt", k);
	ofstream psiout(filename);
	psiout << P;
	psiout.close();
	sprintf_s(filename, "./result/u%d.txt", k);
	ofstream uout(filename);
	uout << U;
	uout.close();
	sprintf_s(filename, "./result/v%d.txt", k);
	ofstream vout(filename);
	vout << V;
	vout.close();
	for (int k = 1; k < 500; k++)
	{
		estimationuv(U, V, P);
		fix(U, V, P);
		boundary();
		sprintf_s(filename, "./result/p%d.txt", k);
		ofstream psiout(filename);
		psiout << P;
		psiout.close();
		sprintf_s(filename, "./result/u%d.txt", k);
		ofstream uout(filename);
		uout << U;
		uout.close();
		sprintf_s(filename, "./result/v%d.txt", k);
		ofstream vout(filename);
		vout << V;
		vout.close();
	}
	

}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
